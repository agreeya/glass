package com.agreeyamobility.glasslauncher;

public interface Common {
    // TODO sample code it must remove before final release.
    public static final boolean SUPPORT_TEST_VOICE_COMMAND = false;
    public static final long CLICK_CHECK_INTERVAL_TIME = 0;

    public static final String EXTERNAL_STORAGE_PATH = "/storage/emulated/legacy";

    public static final String BROWSER_DEFAULT_URL = "http://www.baidu.com";

    public class Display {
        public static final int RESOLUTAION_WIDTH = 320;
        public static final int RESOLUTAION_HEIGHT = 240;

        public static float DEVICE_DPI = 0.0f;
        public static int DEVICE_RESOLUTION_WIDTH = RESOLUTAION_WIDTH;
        public static int DEVICE_RESOLUTION_HEIGHT = RESOLUTAION_HEIGHT;

        public static int PIXEL_WIDTH = 0;
        public static int PIXEL_HEIGHT = 0;
    }

    public class HistoryType {
        public static final int TYPE_IMAGE = 0x01;
        public static final int TYPE_VIDEO = 0x02;
    }

    public interface Directory {
        public static final String SAVE_DIRECTORY_NAME = "AgreeyaDemo";
        public static final String SAVE_VOICE_DIRECTORY_NAME = "VoiceCommand";
    }

    public interface Weather {
        public static final String WEATHER_URI_FIRST  = "http://api.openweathermap.org/data/2.5/weather?q=";
        public static final String WEATHER_URI_CITY_KR  = "http://api.openweathermap.org/data/2.5/weather?q=Seoul";
        public static final String WEATHER_URI_CITY_CN  = "http://api.openweathermap.org/data/2.5/weather?q=Beijing";
        public static final String WEATHER_URI_SECOND = "&type=accurate&mode=xml";
        public static final String WEATHER_CITY_BEIJING = "Beijing";

        // Convert value from K to C
        public static final float WEATER_KELVIN_CELSIUS_OFFSET = 273.15f;

        // City information tag
        public interface CityInfo {
            public static final String WEATHER_CITY_BASE         = "city";
            public static final String WEATHER_CITY_BASE_NAME    = "name";
            public static final String WEATHER_CITY_BASE_ID      = "id";

            public static final String WEATHER_CITY_COORD        = "coord";
            public static final String WEATHER_CITY_COORD_LAT    = "lat";
            public static final String WEATHER_CITY_COORD_LON   = "lon";

            public static final String WEATHER_CITY_COUNTRY      = "country";

            public static final String WEATHER_CITY_SUN          = "sun";
            public static final String WEATHER_CITY_SUN_SET      = "set";
            public static final String WEATHER_CITY_SUN_RISE     = "rise";
        }

        // Temperature information tag
        public interface TemperatureInfo {
            public static final String TEMPERATURE_BASE          = "temperature";
            public static final String TEMPERATURE_BASE_UNIT     = "unit";
            public static final String TEMPERATURE_BASE_MAX      = "max";
            public static final String TEMPERATURE_BASE_MIN      = "min";
            public static final String TEMPERATURE_BASE_VALUE    = "value";
        }

        // Humidity information tag
        public interface HumidityInfo {
            public static final String HUMIDITY_BASE             = "humidity";
            public static final String HUMIDITY_BASE_UNIT        = "unit";
            public static final String HUMIDITY_BASE_VALUE       = "value";
        }

        // Pressure information tag
        public interface PressureInfo {
            public static final String PRESSURE_BASE             = "pressure";
            public static final String PRESSURE_BASE_UNIT        = "unit";
            public static final String PRESSURE_BASE_VALUE       = "value";
        }

        // Wind information tag
        public interface WindInfo {
            public static final String WIND_BASE                 = "wind";
            public static final String WIND_BASE_SPEED           = "speed";
            public static final String WIND_BASE_SPEED_NAME      = "name";
            public static final String WIND_BASE_SPEED_VALUE     = "value";

            public static final String WIND_BASE_DIRECTION       = "direction";
            public static final String WIND_BASE_DIRECTION_NAME  = "name";
            public static final String WIND_BASE_DIRECTION_VALUE = "value";
        }

        // Clouds information tag
        public interface CloudsInfo {
            public static final String CLOUDS_BASE               = "clouds";
            public static final String CLOUDS_BASE_NAME          = "name";
            public static final String CLOUDS_BASE_VALUE         = "value";
        }

        // Precipitation information tag
        public interface PrecipitationInfo {
            public static final String PRECIPITATION_BASE        = "precipitation";
            public static final String PRECIPITATION_BASE_MODE   = "mode";
        }

        // Weather information tag
        public interface WeatherInfo {
            public static final String WEATHER_BASE              = "weather";
            public static final String WEATHER_BASE_VALUE        = "value";

            public static final String WEATHER_BASE_ICON         = "icon";
            public static final String WEATHER_BASE_NUMBER       = "number";
        }
    }

    public interface VoiceCommand {
        public interface ENGLISH {
            public static final String  COMMAND_OK_GLASS = "ok glass";
            public static final String  COMMAND_TAKE_PICTURE = "take a picture";
            public static final String  COMMAND_RECORD_VIDEO = "record a video";
        }

        public interface CHINESE {
            public static final String  COMMAND_OK_GLASS = "眼镜请注意";
            public static final String  COMMAND_TAKE_PICTURE = "请拍照";
            public static final String  COMMAND_RECORD_VIDEO = "请录像";
        }
    }

    public interface WiFiType {
        public static final String WIFI_TYPE = "WIFI";
        public static final String WIFI_SSID = "#SSID@";
        public static final String WIFI_PASS = "@P#";

        public static final String WEP = "WEA";
        public static final String WPA = "WPA";
        public static final String WPA2 = "WPA2";
        public static final String ESS = "ESS";
    }

    public interface CityType {
        public static final String CITY_TYPE = "CITY";
        public static final String CITY_NAME = "#NAME@";
    }

    public interface ScheduleType {
        public static final String SCHEDULE_TYPE = "SCHEDULE";
        public static final String SCHEDULE_TIME = "#TIME@";
        public static final String SCHEDULE_DATA = "@DATA#";
    }
}
