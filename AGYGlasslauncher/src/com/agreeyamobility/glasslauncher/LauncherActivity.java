package com.agreeyamobility.glasslauncher;


import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.agreeyamobility.glasslauncher.Common.Display;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;


public class LauncherActivity extends Activity {
    private LauncherManager mLauncherManager;
    private long mOldTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        float dpi = getResources().getDisplayMetrics().density;
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);

        Display.DEVICE_DPI = dpi;
        Display.DEVICE_RESOLUTION_WIDTH = displaySize.x;
        Display.DEVICE_RESOLUTION_HEIGHT = displaySize.y;
        Display.PIXEL_WIDTH = (int) ((float) displaySize.x / dpi);
        Display.PIXEL_HEIGHT = (int) ((float) displaySize.y / dpi);

        setContentView(R.layout.base_content);

        mLauncherManager = LauncherManager.getInstence(this);

        View contentView = mLauncherManager.getContentView();

        contentView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        LinearLayout baseFrame = (LinearLayout) findViewById(R.id.baseFrame);
        baseFrame.addView(contentView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLauncherManager.resume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void onBackPressed() {
    }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// adb shell keyevent 27 to trigger take picture
		case KeyEvent.KEYCODE_CAMERA:
			if (mOldTime == 0) {
				mOldTime = System.currentTimeMillis();
			}
			return true;
			// adb shell keyevent 188 to trigger record video
		case KeyEvent.KEYCODE_BUTTON_1:
			if (mLauncherManager != null) {
				mLauncherManager.featureRecordAVideo();
			}
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// adb shell keyevent 27 to trigger take picture
		case KeyEvent.KEYCODE_CAMERA:
			long systemTime = System.currentTimeMillis();
			if (Math.abs(mOldTime - systemTime) < 1000) {
				if (mLauncherManager != null) {
					mLauncherManager.featureTakeAPicture(true);
				}
			} else {
				if (mLauncherManager != null) {
					mLauncherManager.featureRecordAVideo();
				}
			}
			mOldTime = 0;
			return true;
		}

		return super.onKeyUp(keyCode, event);
	}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLauncherManager.destroy();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}

