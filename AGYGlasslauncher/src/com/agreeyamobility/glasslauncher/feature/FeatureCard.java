package com.agreeyamobility.glasslauncher.feature;

import java.io.File;

import com.agreeyamobility.glasslauncher.Common;
import com.agreeyamobility.glasslauncher.Common.Directory;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;
import com.agreeyamobility.glasslauncher.ui.CardScrollView;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;

public abstract class FeatureCard {
    protected Context mContext;
    protected LauncherManager mManager;
    protected ContentResolver mResolver;
    protected LayoutInflater mInflater;
    protected String mPath = null;

    public FeatureCard(Context context, LauncherManager manager) {
        mContext = context;
        mManager = manager;
        mResolver = mContext.getContentResolver();
        mInflater = LayoutInflater.from(mContext);
        checkPath();
    }

    private void checkPath() {
        // check path
        String status = Environment.getExternalStorageState();

        if (status.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
//            String path = Environment.getExternalStorageDirectory().getPath();
            String path = Common.EXTERNAL_STORAGE_PATH;
            File dir = new File(path + "/" + Directory.SAVE_DIRECTORY_NAME);
            if (dir.exists() == false) {
                if (dir.mkdir()) {
                    mPath = dir.getPath();
                }
            } else {
                if (dir.isDirectory() == false) {
                    if (dir.mkdir()) {
                        mPath = dir.getPath();
                    }
                } else {
                    mPath = dir.getPath();
                }
            }
        } else {
//            String path = Environment.getDataDirectory().getPath();
            String path = Common.EXTERNAL_STORAGE_PATH;
            File dir = new File(path + "/" + Directory.SAVE_DIRECTORY_NAME);
            if (dir.exists() == false) {
                if (dir.mkdir()) {
                    mPath = dir.getPath();
                }
            } else {
                if (dir.isDirectory() == false) {
                    if (dir.mkdir()) {
                        mPath = dir.getPath();
                    }
                } else {
                    mPath = dir.getPath();
                }
            }
        }

        createMenu();
    }

    public void prepareMenu(CardScrollView menuList) {
    }

    public void selectMenu(int id) {
    }

    public abstract void createMenu();
    public abstract void onFinishAnimation();
    public abstract void destroy();
}