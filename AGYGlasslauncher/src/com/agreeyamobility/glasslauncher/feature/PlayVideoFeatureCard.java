package com.agreeyamobility.glasslauncher.feature;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;
import com.agreeyamobility.glasslauncher.ui.CardScrollView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView.FrameViewListener;

public class PlayVideoFeatureCard extends FeatureCard implements
        SurfaceTextureListener, FrameViewListener {
	private static final String TAG = "PlayVideoFeatureCard";
    private static final int PLAYER_STATE_NONE = 0x01;
    private static final int PLAYER_STATE_PREPARED = 0x02;
    private static final int PLAYER_STATE_COMPLETE = 0x03;

    private MediaPlayer mPlayer;
    private PlayTimeCheckHandler mHandler;
    private DisplayTimeHandler mDisplayTimeHandler;
    private String mContentPath;
    private TextView mTimeView;
    private ProgressBar mProgressBar;

    private Animation mHideAnimation;

    private View mMenuPlay;

    private int mPlayerState = PLAYER_STATE_NONE;

    public PlayVideoFeatureCard(Context context, LauncherManager manager,
            String path) {
        super(context, manager);
        mContentPath = path;
        mHideAnimation = AnimationUtils
                .loadAnimation(mContext, R.anim.disapper);
        View view = LayoutInflater.from(context).inflate(
                R.layout.play_video_content, null, false);
        ((TextureView) view.findViewById(R.id.playPreview))
                .setSurfaceTextureListener(this);
        mTimeView = (TextView) view.findViewById(R.id.timeView);
        mProgressBar = (ProgressBar) view.findViewById(R.id.playProgress);
        mManager.addLiveView(view, this);
        mManager.publish(false);
    }

    public PlayVideoFeatureCard(Context context, LauncherManager manager) {
        super(context, manager);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
            int height) {
        mPlayer = new MediaPlayer();
        mHandler = new PlayTimeCheckHandler();
        mDisplayTimeHandler = new DisplayTimeHandler();
        mPlayer.setSurface(new Surface(surface));
        mPlayer.setOnPreparedListener(mPreparedListener);
        mPlayer.setOnCompletionListener(mCompletionListener);
        try {
            mPlayer.setDataSource(mContentPath);
            mPlayer.prepare();
        } catch (Exception e) {
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
            int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @Override
    public void createMenu() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                com.agreeyamobility.glasslauncher.Common.Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);

        mMenuPlay = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuPlay.setId(R.id.toggle_play);
        mMenuPlay.setLayoutParams(params);
        ((ImageView) mMenuPlay.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_pause);
        ((TextView) mMenuPlay.findViewById(R.id.menuTitle))
                .setText(R.string.menu_extend);
    }

    @Override
    public void prepareMenu(CardScrollView menuList) {
        int resId = -1;
        int textId = -1;
        if (mPlayer.isPlaying()) {
            resId = R.drawable.ic_pause;
            textId = R.string.menu_pause;
        } else {
            resId = R.drawable.ic_play;
            textId = R.string.menu_play;
        }
        ((ImageView) mMenuPlay.findViewById(R.id.menuIcon))
                .setImageResource(resId);
        ((TextView) mMenuPlay.findViewById(R.id.menuTitle)).setText(textId);

        menuList.addListItem(mMenuPlay, true);
    }

    @Override
    public void selectMenu(int id) {
        switch (id) {
        case R.id.toggle_play:
            if (mPlayer.isPlaying()) {
                stopPlay();
            } else {
                startPlay();
            }
            break;
        }
    }

    @Override
    public void onFinishAnimation() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void onMotion(int action) {
        switch (action) {
        case LiveCardFrameView.VIEW_ACTION_CLICK:
            mManager.showLiveMenu();
            mDisplayTimeHandler.sendEmptyMessage(DisplayTimeHandler.START);
            break;
        case LiveCardFrameView.VIEW_ACTION_DOWN:
            stopPlay();
            mManager.removeLiveView();
            break;
        }
    }

    public void stopPlay() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.stop();
        }
    }

    public void startPlay() {
        if (mPlayer != null && mPlayerState == PLAYER_STATE_PREPARED) {
            mPlayer.start();
        }
    }

    private MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            mHandler.sendEmptyMessage(PlayTimeCheckHandler.START);
            mDisplayTimeHandler.sendEmptyMessage(DisplayTimeHandler.START);
            mPlayerState = PLAYER_STATE_PREPARED;
            mPlayer.start();
        }
    };

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            mPlayerState = PLAYER_STATE_COMPLETE;
            mHandler.sendEmptyMessage(PlayTimeCheckHandler.STOP);
            mManager.removeLiveView();
        }
    };

    private class DisplayTimeHandler extends Handler {
        public static final int START = 0x01;
        public static final int CHECK = 0x02;
        private static final int CHECK_TIME = 3000;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case START:
                if (mTimeView.isShown() == false) {
                    mTimeView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                this.removeMessages(CHECK);
                this.sendEmptyMessageDelayed(CHECK, CHECK_TIME);
                break;
            case CHECK:
                if (mTimeView.isShown() == true) {
                    mTimeView.setVisibility(View.GONE);
                    mTimeView.startAnimation(mHideAnimation);
                    mProgressBar.setVisibility(View.GONE);
                    mProgressBar.startAnimation(mHideAnimation);
                }
                break;
            }
        }
    }

    private class PlayTimeCheckHandler extends Handler {
        public static final int START = 0x01;
        public static final int UPDATE = 0x02;
        public static final int STOP = 0x03;
        private static final int UPDATE_TIME = 100;

        private int mTotal;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case START:

                mTotal = mPlayer.getDuration() / 1000;
                if (mProgressBar != null) {
                    mProgressBar.setMax(mTotal);
                }
                this.sendEmptyMessage(UPDATE);
                break;
            case UPDATE:
                if (mTimeView != null && mPlayer != null) {
                    int time = mPlayer.getCurrentPosition() / 1000;
                    if (mProgressBar != null) {
                        mProgressBar.setProgress(time);
                    }

                    mTimeView.setText(String.format("%d/%d", time, mTotal));
                    this.sendEmptyMessageDelayed(UPDATE, UPDATE_TIME);
                }
                break;
            case STOP:
                this.removeMessages(UPDATE);
                break;
            }
        }
    }
}