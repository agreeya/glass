package com.agreeyamobility.glasslauncher.feature;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.TextureView;
import android.view.View;
import android.view.TextureView.SurfaceTextureListener;

import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView.FrameViewListener;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

public class QRCodeFeatrueCard extends FeatureCard implements
        SurfaceTextureListener, FrameViewListener {
//	private static final String TAG = "WiFiSettingFeatrueCard";

    public static final int MODE_WIFI_CONNECT = 0x01;
    public static final int MODE_SET_SCHEDULE = 0x02;
    public static final int MODE_SET_WEATHER  = 0x03;

    private static final int PREVIEW_SIZE_WIDTH = 640;
    private static final int PREVIEW_SIZE_HEIGHT = 480;

    private Camera mCamera;
    private int mCameraId;
    private QRDecorderThread mThread;

    private int mMode = 0;

    public QRCodeFeatrueCard(Context context, LauncherManager manager, int mode) {
        super(context, manager);

        mMode = mode;

        View view = LayoutInflater.from(context).inflate(
                R.layout.qr_code_content, null, false);
        ((TextureView) view.findViewById(R.id.qrPreview))
                .setSurfaceTextureListener(this);
        mManager.addLiveView(view, this);
        mManager.publish(true);
    }

    @Override
    public void createMenu() {
    }

    @Override
    public void selectMenu(int index) {
    }

    @Override
    public void onFinishAnimation() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
            int height) {
        int numberOfCameras = Camera.getNumberOfCameras();
        CameraInfo cameraInfo = new CameraInfo();

        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);

            // CAMERA_FACING_FRONT or CAMERA_FACING_BACK
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                mCameraId = i;
            }
        }

        mCamera = Camera.open(mCameraId);

        if (mCamera != null) {
            try {
                mCamera.setPreviewTexture(surface);
                mCamera.setPreviewCallback(mPreviewCallback);

                mThread = new QRDecorderThread(new QRHandler());

                Camera.Parameters parameters = mCamera.getParameters();

                List<String> supportedFocusModes = parameters
                        .getSupportedFocusModes();
                boolean hasAutoFocus = supportedFocusModes != null
                        && supportedFocusModes
                                .contains(Camera.Parameters.FOCUS_MODE_AUTO);
                if (hasAutoFocus == true) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                }

                int jpegRotation = getJpegRotation(mCameraId, 0);
                parameters.setRotation(jpegRotation);

                mCamera.setDisplayOrientation(getDisplayOrientation(0, mCameraId));

                List<Size> previewList = parameters.getSupportedPreviewSizes();
                for (Size size : previewList) {
                    if (size.height == PREVIEW_SIZE_HEIGHT
                            && size.width == PREVIEW_SIZE_WIDTH) {
                        parameters.setPreviewSize(PREVIEW_SIZE_WIDTH,
                                PREVIEW_SIZE_HEIGHT);
                        mCamera.setParameters(parameters);
                        break;
                    }
                }

                // if (hasAutoFocus == true) {
                // mCamera.autoFocus(mAutoFocusCallback);
                // }
                mThread.start();
                mCamera.startPreview();
            } catch (IOException e) {
            }
        }
    }

    private int getJpegRotation(int cameraId, int orientation) {
        // See android.hardware.Camera.Parameters.setRotation for
        // documentation.
        int rotation = 0;
        if (orientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
            CameraInfo info = new CameraInfo();
            android.hardware.Camera.getCameraInfo(cameraId, info);
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                rotation = (info.orientation - orientation + 360) % 360;
            } else { // back-facing camera
                rotation = (info.orientation + orientation) % 360;
            }
        }
        return rotation;
    }

    private int getDisplayOrientation(int degrees, int cameraId) {
        // See android.hardware.Camera.setDisplayOrientation for
        // documentation.
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
            int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mThread != null) {
            mThread.interrupt();
        }

        if (mCamera != null) {
            mCamera.release();
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @Override
    public void onMotion(int action) {
        switch (action) {
        case LiveCardFrameView.VIEW_ACTION_DOWN:
            mManager.removeLiveView();
            break;
        }
    }

    private Camera.PreviewCallback mPreviewCallback = new Camera.PreviewCallback() {

        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            Size size = camera.getParameters().getPreviewSize();

            PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(
                    data, size.width, size.height, 0, 0, size.width,
                    size.height, false);

            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            mThread.insert(bitmap);
        }
    };

    @SuppressWarnings("unused")
    private Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {

        @Override
        public void onAutoFocus(boolean success, Camera camera) {
        }
    };

    private class QRDecorderThread extends Thread {
        private static final int QUEUE_MAX = 20;
        private BlockingQueue<BinaryBitmap> mQueue;
        private Handler mHandler;
        private boolean mCallOnce;

        public QRDecorderThread(Handler handler) {
            mCallOnce = false;
            mHandler = handler;
            mQueue = new ArrayBlockingQueue<BinaryBitmap>(QUEUE_MAX);
        }

        @Override
        public void run() {
            while (true) {
                try {
                    BinaryBitmap bitmap = mQueue.take();
                    QRCodeReader reader = new QRCodeReader();

                    try {
                        Result result = reader.decode(bitmap);
                        String resultData = result.getText();
                        if (resultData != null && mCallOnce == false) {
                            mCallOnce = true;
                            mCamera.stopPreview();
                            mHandler.sendMessageDelayed(mHandler.obtainMessage(0, resultData), 200);
                        }
                    } catch (Exception e) {
                    }
                } catch (InterruptedException e) {
                    break;
                }
            }
        }

        public void insert(BinaryBitmap data) {
            try {
                mQueue.put(data);
            } catch (InterruptedException e) {
            }
        }
    }

    private class QRHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            mThread.interrupt();
            mManager.readQRCode((String) msg.obj, mMode);
        }
    }
}