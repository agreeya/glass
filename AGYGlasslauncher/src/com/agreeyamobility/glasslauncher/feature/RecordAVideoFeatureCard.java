package com.agreeyamobility.glasslauncher.feature;

import java.io.IOException;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.Common.HistoryType;
import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.HistoryColumns;
import com.agreeyamobility.glasslauncher.ui.CardScrollView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView.FrameViewListener;


public class RecordAVideoFeatureCard extends FeatureCard implements
        FrameViewListener, SurfaceHolder.Callback {
	private static final String TAG = "RecordAVideoFeatureCard";
	
    private static final int DEFAULT_PROGRESS_VALUE = 10;

    private static final int RECORD_STATE_NONE = 0x00;
    private static final int RECORD_STATE_READY = 0x01;
    private static final int RECORD_STATE_START = 0x02;
    private static final int RECORD_STATE_STOP = 0x03;

    private Camera mCamera;

    private int mRecordState = RECORD_STATE_NONE;

    private ProgressBar mProgressBar;
    private TextView mRecordTimeView;
    private MediaRecorder mRecorder;

    private TimeUpdateHandler mHandler;

    private View mMenuExtend;
    private View mMenuStop;

    public RecordAVideoFeatureCard(Context context, LauncherManager manager) {
        super(context, manager);
        mHandler = new TimeUpdateHandler();
        View view = LayoutInflater.from(context).inflate(
                R.layout.record_content, null, false);
        ((SurfaceView) view.findViewById(R.id.recordLayout))
        .getHolder().addCallback(this);

        mRecordTimeView = (TextView) view.findViewById(R.id.recordTime);
        mProgressBar = (ProgressBar) view.findViewById(R.id.recordProgress);
        mProgressBar.setMax(DEFAULT_PROGRESS_VALUE);
        mManager.addLiveView(view, this);
        mManager.publish(false);
    }

    @Override
    public void onFinishAnimation() {
    }

    @Override
    public void destroy() {

        if (mCamera != null) {
            mCamera.release();
        }

        if (mRecorder != null) {
            mRecorder.release();
        }
    }

    public int getDisplayOrientation(int degrees, int cameraId) {
        // See android.hardware.Camera.setDisplayOrientation for
        // documentation.
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }
    
    private boolean prepareVideoRecorder(Surface surface) {
        mRecorder = new MediaRecorder();
        mRecorder.setPreviewDisplay(surface);
        
        mCamera.setDisplayOrientation(getDisplayOrientation(0, 0));
        
        mCamera.unlock();

        mRecorder.setCamera(mCamera);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        mRecorder.setProfile(CamcorderProfile
                .get(CamcorderProfile.QUALITY_720P));

        // See android.hardware.Camera.Parameters.setRotation for
        // documentation.
        // Note that mOrientation here is the device orientation, which is the opposite of
        // what activity.getWindowManager().getDefaultDisplay().getRotation() would return,
        // which is the orientation the graphics need to rotate in order to render correctly.
        int rotation = 0;
		CameraInfo info = new CameraInfo();
		android.hardware.Camera.getCameraInfo(0, info);
		if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
			rotation = (info.orientation - 0 + 360) % 360;
		} else { // back-facing camera
			rotation = (info.orientation + 0) % 360;
		}
		Log.i(TAG, "rotation: " + rotation);
		mRecorder.setOrientationHint(rotation);
        
        mPath = mPath + "/" + getVideoFileName();
        mRecorder.setOutputFile(mPath);

        mRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
                stopBError();
                mManager.removeLiveView();
            }
        });

        mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
            }
        });

        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
        }

        try {
            mRecorder.prepare();
            mRecordState = RECORD_STATE_READY;
        } catch (IllegalStateException e) {
            mRecordState = RECORD_STATE_NONE;
            return false;
        } catch (IOException e) {
            mRecordState = RECORD_STATE_NONE;
            return false;
        }

        return true;
    }

    private void stopBError() {
        if (mRecorder != null && mRecordState == RECORD_STATE_START) {
            mHandler.sendEmptyMessage(TimeUpdateHandler.TIMELINE_MESSAGE_STOP);
            mRecorder.stop();
            mRecordState = RECORD_STATE_STOP;
            mManager.hideLiveMenu();
        }
    }

    private void stopRecord() {
        if (mRecorder != null && mRecordState == RECORD_STATE_START) {
            mHandler.sendEmptyMessage(TimeUpdateHandler.TIMELINE_MESSAGE_STOP);
            mRecorder.stop();
            mRecordState = RECORD_STATE_STOP;
            mManager.hideLiveMenu();
            new RecordVideoSaveTask().executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, mPath);
        }
    }

    private String getVideoFileName() {
        Calendar calendar = Calendar.getInstance();
        return "Video_"
                + String.format("%02d%02d%02d_%02d%02d%02d",
                        calendar.get(Calendar.YEAR) % 100,
                        calendar.get(Calendar.MONTH) + 1,
                        calendar.get(Calendar.DAY_OF_MONTH),
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND)).concat(".mp4");
    }

    private class TimeUpdateHandler extends Handler {
        public static final int TIMELINE_MESSAGE_START = 0x01;
        public static final int TIMELINE_MESSAGE_UPDATE = 0x02;
        public static final int TIMELINE_MESSAGE_STOP = 0x03;

        private static final int TIME_UPDATE_DELAY = 1000;

        private long mTime;
        private int mProgress;

        @Override
        public void handleMessage(Message msg) {
            String timeString;

            switch (msg.what) {
            case TIMELINE_MESSAGE_START:
                if (mRecordTimeView != null) {
                    mRecordTimeView.setVisibility(View.VISIBLE);
                    mTime = 0;
                    timeString = String.format("%02d:%02d:%02d", 0, 0, 0);
                    mRecordTimeView.setText(timeString);
                    mProgress = 0;
                    this.sendEmptyMessageDelayed(TIMELINE_MESSAGE_UPDATE,
                            TIME_UPDATE_DELAY);
                }
                break;
            case TIMELINE_MESSAGE_UPDATE:
                mTime++;
                mProgress++;
                if (mRecordTimeView != null) {
                    int hour = (int) (mTime / 3600);
                    int min = (int) (mTime % 3600 / 60);
                    int sec = (int) (mTime % 60);
                    if (hour > 12) {
                        hour = hour - 12;
                    }

                    timeString = String
                            .format("%02d:%02d:%02d", hour, min, sec);

                    if (mProgress > mProgressBar.getMax()) {
                        stopRecord();
                        return;
                    }

                    mProgressBar.setProgress(mProgress);
                    mRecordTimeView.setText(timeString);
                    this.sendEmptyMessageDelayed(TIMELINE_MESSAGE_UPDATE,
                            TIME_UPDATE_DELAY);
                }
                break;
            case TIMELINE_MESSAGE_STOP:
                mRecordTimeView.setVisibility(View.GONE);
                this.removeMessages(TIMELINE_MESSAGE_UPDATE);
                break;
            }
        }
    }

    private class RecordVideoSaveTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            long time = System.currentTimeMillis();
            int type = HistoryType.TYPE_VIDEO;
            ContentValues values = new ContentValues();
            values.put(HistoryColumns.TYPE, type);
            values.put(HistoryColumns.TIME, time);
            values.put(HistoryColumns.TEXT, mPath);
            Uri uri = mResolver.insert(HistoryColumns.CONTENT_URI, values);
            int id = Integer.valueOf(uri.getLastPathSegment());
            mManager.addHistoryCard(type, time, mPath, id, null, null);
        }
    }

    @Override
    public void createMenu() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                com.agreeyamobility.glasslauncher.Common.Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);

        mMenuExtend = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuExtend.setId(R.id.extend_recode);
        mMenuExtend.setLayoutParams(params);
        ((ImageView)mMenuExtend.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_extend);
        ((TextView)mMenuExtend.findViewById(R.id.menuTitle)).setText(R.string.menu_extend);

        mMenuStop = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuStop.setId(R.id.stop_recode);
        mMenuStop.setLayoutParams(params);
        ((ImageView)mMenuStop.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_stop);
        ((TextView)mMenuStop.findViewById(R.id.menuTitle)).setText(R.string.menu_stop);
    }

    @Override
    public void prepareMenu(CardScrollView menuList) {
        menuList.addListItem(mMenuExtend, true);
        menuList.addListItem(mMenuStop, false);
    }

    @Override
    public void selectMenu(int id) {
        switch (id) {
        case R.id.extend_recode:
            if (mProgressBar != null) {
                int max = mProgressBar.getMax();
                mProgressBar.setMax(max + DEFAULT_PROGRESS_VALUE);
            }
            break;
        case R.id.stop_recode:
            stopRecord();
            break;
        }
    }

    @Override
    public void onMotion(int action) {
        switch (action) {
        case LiveCardFrameView.VIEW_ACTION_CLICK:
            mManager.showLiveMenu();
            break;
        case LiveCardFrameView.VIEW_ACTION_DOWN:
            mManager.hideLiveMenu();
            break;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        int numberOfCameras = Camera.getNumberOfCameras();
        CameraInfo cameraInfo = new CameraInfo();

        int cameraId = -1;

        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);

            // CAMERA_FACING_FRONT or CAMERA_FACING_BACK
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
            }
        }

        if (cameraId != -1) {

            mCamera = Camera.open(cameraId);
        } else {
            mCamera = Camera.open();
        }

        if (prepareVideoRecorder(holder.getSurface())) {
            mRecorder.start();
            mRecordState = RECORD_STATE_START;
            mHandler.sendEmptyMessage(TimeUpdateHandler.TIMELINE_MESSAGE_START);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
        
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mRecorder != null) {
            mRecorder.release();
        }

        if (mCamera != null) {
            mCamera.release();
        }
    }
}