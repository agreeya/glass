package com.agreeyamobility.glasslauncher.feature;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;

import com.agreeyamobility.glasslauncher.Common.HistoryType;
import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.HistoryColumns;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView.FrameViewListener;


public class TakeAPictureFeatureCard extends FeatureCard implements
		FrameViewListener, SurfaceTextureListener {
    private static final String TAG = "TakeAPictureFeatureCard";
    private static final boolean FULL_SIZE_MODE = true;
    
    private static final int PICTURE_SIZE_WIDTH = 640;
    private static final int PICTURE_SIZE_HEIGHT = 480;

    private Camera mCamera = null;
    private boolean mCanTake = false;
    private boolean mAutoTake = false;

    public TakeAPictureFeatureCard(Context context, LauncherManager manager, boolean autoTake) {
        super(context, manager);
        mAutoTake = autoTake;
        View view = LayoutInflater.from(context).inflate(
                R.layout.take_picture_content, null, false);
        ((TextureView) view.findViewById(R.id.takePicturePreview))
                .setSurfaceTextureListener(this);
//        view.setOnClickListener(mTakePictrueClickListener);
        mManager.addLiveView(view, this);
        mManager.publish(mAutoTake);
    }

//    private View.OnClickListener mTakePictrueClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            if (mCamera != null) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                       onTakePicture();
//                    }
//                }, 50);
//            }
//        }
//    };
    
	public int getJpegRotation(int cameraId, int orientation) {
		// See android.hardware.Camera.Parameters.setRotation for
		// documentation.
		int rotation = 0;
		if (orientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
			CameraInfo info = new CameraInfo();
			android.hardware.Camera.getCameraInfo(cameraId, info);
			if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
				rotation = (info.orientation - orientation + 360) % 360;
			} else { // back-facing camera
				rotation = (info.orientation + orientation) % 360;
			}
		}
		return rotation;
	}
    
    public int getDisplayOrientation(int degrees, int cameraId) {
        // See android.hardware.Camera.setDisplayOrientation for
        // documentation.
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }
	
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
            int height) {

        int cameraId = -1;
        boolean errorFound = false;
        boolean hasFeatCamera = mContext.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA);

        if (hasFeatCamera) {
            try {
                cameraId = CameraInfo.CAMERA_FACING_BACK;
                Camera.open(cameraId).release();
            } catch (Exception e) {
                Camera.open(0).release();
            }
        } else if (CameraInfo.CAMERA_FACING_FRONT > -1) {
            try {
                cameraId = CameraInfo.CAMERA_FACING_FRONT;
                Camera.open(cameraId).release();
            } catch (Exception e) {
                errorFound = true;
            }

            if (errorFound) {
                try {
                    Camera.open(0).release(); // Put in for Nexus 7
                    cameraId = 0;
                } catch (Exception e) {
                    cameraId = -1;
                }
            }
        }

        if (cameraId < 0) {
            new Handler().postDelayed(new Runnable() {
                
                @Override
                public void run() {
                    mManager.removeLiveView();
                }
            }, 100);
            return;
        }

        mCamera = Camera.open(cameraId);

        if (mCamera != null) {
            try {
                mCamera.setPreviewTexture(surface);

                Camera.Parameters parameters = mCamera.getParameters();

                // auto focus
                parameters.setFocusMode(Parameters.FOCUS_MODE_AUTO);

                // Preview fps
                List<int[]> rangeList = parameters.getSupportedPreviewFpsRange();
                Point rangeSize = new Point();
                rangeSize.x = 0;
                rangeSize.y = 0;

                for (int[] range : rangeList) {
                    if (range.length > 1) {
                        int value1 = range[0];
                        int value2 = range[1];
                        if (value1 > value2) {
                            rangeSize.y = value1;
                            rangeSize.x = value2;
                        } else {
                            rangeSize.y = value2;
                            rangeSize.x = value1;
                        }
                    }
                }

                parameters.setPreviewFpsRange(rangeSize.x, rangeSize.y);

                List<Size> previewList = parameters.getSupportedPreviewSizes();
                List<Size> pictureList = parameters.getSupportedPictureSizes();

                int pWidth = PICTURE_SIZE_WIDTH;
                int pHeight = PICTURE_SIZE_HEIGHT;

                if (FULL_SIZE_MODE) {
                    parameters.setPreviewSize(pWidth, pHeight);

                    // NOTICE. picture maximum size is occurred memory leak
                    for (Size picture : pictureList) {
                    	if (pWidth <= picture.width && pHeight <= picture.height) {
                    		pWidth = picture.width;
                    		pHeight = picture.height;
                            Log.i(TAG, "PictureSize WxH:" + pWidth + "x" + pHeight);
                    	}
                    }
                } else {
                    for (Size previewSize : previewList) {
                        for (Size pictureSize : pictureList) {
                            if (previewSize.width == pictureSize.width
                                    && previewSize.height == pictureSize.height
                                    && previewSize.width > pWidth
                                    && previewSize.height > pHeight) {
                                pWidth = previewSize.width;
                                pHeight = previewSize.height;
                                break;
                            }
                        }
                    }
                    parameters.setPreviewSize(pWidth, pHeight);
                }

				int jpegRotation = getJpegRotation(cameraId, 0);
				parameters.setRotation(jpegRotation);
				
				mCamera.setDisplayOrientation(getDisplayOrientation(0, cameraId));
                
                parameters.setPictureSize(pWidth, pHeight);

                mCamera.setParameters(parameters);
                mCanTake = true;
                mCamera.startPreview();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
            int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mCamera != null) {
            mCamera.release();
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    private void onTakePicture() {
        if (mCanTake == true) {
            mCanTake = false;
            mCamera.takePicture(null, null, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    if (mPath != null) {
                        new TakePictureSaveAsyncTask(data)
                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            });
        }
    }

    private class TakePictureSaveAsyncTask extends
            AsyncTask<Void, Void, Boolean> {
        private byte[] mData;
        private String mFilePath;

        public TakePictureSaveAsyncTask(byte[] data) {
            mData = data;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            mFilePath = mPath + "/IMG_" + getFileName();

            File file = new File(mFilePath);
            try {
                this.publishProgress();

                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                // 100 means no compression, the lower you go, the stronger the
                // compression
                fOut.write(mData);
                fOut.flush();
                fOut.close();
                return true;
            } catch (IOException e) {
            }
            return false;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

            // update history
            long time = System.currentTimeMillis();
            int type = HistoryType.TYPE_IMAGE;
            ContentValues value = new ContentValues();
            value.put(HistoryColumns.TYPE, type);
            value.put(HistoryColumns.TIME, time);
            value.put(HistoryColumns.TEXT, mFilePath);
            Uri uri = mResolver.insert(
                    HistoryColumns.CONTENT_URI, value);
            int id = Integer.valueOf(uri.getLastPathSegment());
            mManager.addHistoryCard(type, time,
                    mFilePath, id, mData, mCamera.getParameters());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result == true) {
                MediaScannerConnection.scanFile(mContext,
                        new String[] { mFilePath }, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {
                            }
                        });
            }
        }

        private String getFileName() {
            Calendar calendar = Calendar.getInstance();
            return String.format("%02d%02d%02d_%02d%02d%02d",
                    calendar.get(Calendar.YEAR) % 100,
                    calendar.get(Calendar.MONTH) + 1,
                    calendar.get(Calendar.DAY_OF_MONTH),
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    calendar.get(Calendar.SECOND)).concat(".jpg");
        }
    }

    @Override
    public void onFinishAnimation() {
        if (mCamera != null && mAutoTake == true) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onTakePicture();
                }
            }, 50);
        }
    }

    @Override
    public void destroy() {
        if (mCamera != null) {
            mCamera.release();
        }
    }

    @Override
    public void createMenu() {
    }

    @Override
    public void selectMenu(int index) {
    }
    
    @Override
    public void onMotion(int action) {
        if (mAutoTake == true) {
            return;
        }

        switch (action) {
        case LiveCardFrameView.VIEW_ACTION_CLICK:
        	Log.i(TAG, "onMotion Click");
            if (mCamera != null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                       onTakePicture();
                    }
                }, 50);
            }
            break;
        case LiveCardFrameView.VIEW_ACTION_DOWN:
        	Log.i(TAG, "onMoven Down");
            if (mCamera != null) {
            	mCamera.stopPreview();
            	mCamera.release();
            	mManager.removeLiveView();
            }
            break;
        }
    }
}
