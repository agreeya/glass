package com.agreeyamobility.glasslauncher.manager;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.AudioManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.InputDevice;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnGenericMotionListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.agreeyamobility.glasslauncher.Common;
import com.agreeyamobility.glasslauncher.Common.CityType;
import com.agreeyamobility.glasslauncher.Common.ScheduleType;
import com.agreeyamobility.glasslauncher.Common.VoiceCommand.CHINESE;
import com.agreeyamobility.glasslauncher.Common.VoiceCommand.ENGLISH;
import com.agreeyamobility.glasslauncher.Common.WiFiType;
import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.feature.FeatureCard;
import com.agreeyamobility.glasslauncher.feature.PlayVideoFeatureCard;
import com.agreeyamobility.glasslauncher.feature.RecordAVideoFeatureCard;
import com.agreeyamobility.glasslauncher.feature.TakeAPictureFeatureCard;
import com.agreeyamobility.glasslauncher.feature.QRCodeFeatrueCard;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.HistoryColumns;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.ScheduleColumns;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.WeatherColumns;
import com.agreeyamobility.glasslauncher.ui.BrowserCard;
import com.agreeyamobility.glasslauncher.ui.CalendarCard;
import com.agreeyamobility.glasslauncher.ui.Card;
import com.agreeyamobility.glasslauncher.ui.CardScrollView;
import com.agreeyamobility.glasslauncher.ui.CardScrollView.onCardScrollViewListener;
import com.agreeyamobility.glasslauncher.ui.HistoryCard;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView;
import com.agreeyamobility.glasslauncher.ui.LiveCardFrameView.FrameViewListener;
import com.agreeyamobility.glasslauncher.ui.SettingCard;
import com.agreeyamobility.glasslauncher.ui.TimeLineCard;
import com.agreeyamobility.glasslauncher.ui.WeatherCard;
import com.agreeyamobility.glasslauncher.voice.VoiceManager;
import com.agreeyamobility.glasslauncher.voice.VoiceManager.VoiceManagerListener;


public class LauncherManager {
	private static final String TAG = "LauncherManager";

    private static final int VOICE_START_TIEM = 500;
    private static final int VOICE_DELAY_TIEM = 4000;

    private Context mContext;

    private View mBaseLayout;
    private CardScrollView mBaseScrollView;

    // Menu
    private CardScrollView mMenuScrollView;
    private CardScrollView mSubMenuScrollView;
    private Animation mShowMenuAnimation;
    private Animation mHideMenuAnimation;
    private Animation mShowSubMenuAnimation;
    private Animation mHideSubMenuAnimation;
    private Animation mShowLiveMenuAnimation;
    private Animation mHideLiveMenuAnimation;

    // Voice command
    private Animation mShowVoiceAnimation;
    private Animation mHideVoiceAnimation;
    private RelativeLayout mVoiceCMDView;
    private FrameLayout mVoiceIconLayout;
    private VoiceManager mVoiceManager;
    private Handler mCheckCommandHandler;
    private boolean mCheckRun;

    // Live card
    private WindowManager mWindowManager;
    private AudioManager mAudioManager;
    private WindowManager.LayoutParams mWindowParams;
    private Animation mShowLiveCardAnimation;
    private LiveCardFrameView mLiveFrame;
    private CardScrollView mLiveMenuList;
    private FeatureCard mFeatureCard;
    private View mLiveBaseLayout;
    private QRCodeHandler mQRCodeHandler;

    private static LauncherManager mInstance;

    public static LauncherManager getInstence(Context context) {
        if (mInstance == null) {
            mInstance = new LauncherManager(context);
        }

        return mInstance;
    }

    private LauncherManager(Context context) {
        mContext = context;
        initialize();
    }

    private void initialize() {
        mBaseLayout = LayoutInflater.from(mContext).inflate(
                R.layout.root_content, null, false);
        mBaseScrollView = (CardScrollView) mBaseLayout
                .findViewById(R.id.mainList);
        mBaseScrollView.setOnCardScrollViewListener(mMainScroll);
        mMenuScrollView = (CardScrollView) mBaseLayout
                .findViewById(R.id.meunList);
        mMenuScrollView.setOnCardScrollViewListener(mMenuScroll);

        mSubMenuScrollView = (CardScrollView) mBaseLayout
                .findViewById(R.id.subMeunList);
        mSubMenuScrollView.setOnCardScrollViewListener(mSubMenuScroll);

        mShowMenuAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.down_to_up);
        mShowMenuAnimation.setAnimationListener(mShowMenu);

        mShowSubMenuAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.down_to_up);
        mShowSubMenuAnimation.setAnimationListener(mShowSubMenu);

        mHideMenuAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.up_to_down);
        mHideMenuAnimation.setAnimationListener(mHideMenu);

        mShowLiveMenuAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.down_to_up);
        mShowLiveMenuAnimation.setAnimationListener(mShowLiveMenu);
        mHideLiveMenuAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.up_to_down);
        mHideLiveMenuAnimation.setAnimationListener(mHideLiveMenu);

        mHideSubMenuAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.up_to_down);
        mHideSubMenuAnimation.setAnimationListener(mHideSubMenu);

        mVoiceManager = new VoiceManager(mContext, mVoiceListener);
        mVoiceCMDView = (RelativeLayout) mBaseLayout
                .findViewById(R.id.voiceCommandLayout);
        mVoiceCMDView.setClickable(true);
        mVoiceCMDView.setFocusable(true);
        mVoiceCMDView.setFocusableInTouchMode(true);
        mVoiceCMDView.setOnGenericMotionListener(mVoiceCMDMotionListener);
        mVoiceCMDView.setOnTouchListener(mVoiceCMDListener);
        mShowVoiceAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.down_to_up);
        mShowVoiceAnimation.setAnimationListener(mShowVoice);
        mHideVoiceAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.up_to_down);
        mHideVoiceAnimation.setAnimationListener(mHideVoice);

        mVoiceIconLayout = (FrameLayout) mBaseLayout
                .findViewById(R.id.eventFrame);
        ImageView voiceIcon = new ImageView(mContext);
        voiceIcon.setLayoutParams(new FrameLayout.LayoutParams(30, 30));
        voiceIcon.setImageResource(R.drawable.ic_voice);
        mVoiceIconLayout.addView(voiceIcon);

        // Live card
        mWindowManager = (WindowManager) mContext
                .getSystemService(Context.WINDOW_SERVICE);
        mAudioManager = (AudioManager) mContext
                .getSystemService(Context.AUDIO_SERVICE);

        mWindowParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);

        mLiveBaseLayout = LayoutInflater.from(mContext).inflate(
                R.layout.live_card_content, null, false);
        mLiveBaseLayout.setClickable(true);
        mLiveBaseLayout.setOnClickListener(mLiveClickListener);
        mLiveFrame = (LiveCardFrameView) mLiveBaseLayout
                .findViewById(R.id.liveFrameLayout);
        mLiveMenuList = (CardScrollView) mLiveBaseLayout
                .findViewById(R.id.liveMenuList);
        mLiveMenuList.setOnCardScrollViewListener(mLiveMenuListener);

        mShowLiveCardAnimation = AnimationUtils.loadAnimation(mContext,
                R.anim.down_to_up_slow);
        mShowLiveCardAnimation.setAnimationListener(mShowLiveCard);

        mCheckCommandHandler = new Handler();

        mQRCodeHandler = new QRCodeHandler();

        loadDefaultCard();
        loadHistoryCard();
    }

    public void resume() {
        mBaseScrollView.updateTime(System.currentTimeMillis());
    }

    public void tpSound(int effectType) {
        if (mAudioManager != null) {
            try {
                Log.i(TAG, "tpSound");
                // Play the standard keypress sound at full volume. This should
                // be available on
                // every device. We cannot play a ringtone here because media
                // services aren't
                // available yet. A DTMF-style tone is too soft to be noticed,
                // and might not exist
                // on tablet devices. The idea is to alert the user that
                // something is needed: this
                // does not have to be pleasing.
                mAudioManager.playSoundEffect(effectType, 100);
            } catch (Exception e) {
				Log.w(TAG, "notifyUser: Exception while playing sound: " + e);
            }
        }
    }

    private void loadDefaultCard() {
        // Setting
        new SettingCard(mContext, mBaseScrollView).setLauncherManager(this);
        new WeatherCard(mContext, mBaseScrollView).setLauncherManager(this);
        new CalendarCard(mContext, mBaseScrollView).setLauncherManager(this);
        new TimeLineCard(mContext, mBaseScrollView).setLauncherManager(this);
    }

    private void loadHistoryCard() {
        Cursor cursor = mContext.getContentResolver().query(
                HistoryColumns.CONTENT_URI,
                new String[] { HistoryColumns.ID, HistoryColumns.TEXT,
                        HistoryColumns.TYPE, HistoryColumns.TIME }, null, null,
                HistoryColumns.TIME + " DESC");

        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(0);
                    String text = cursor.getString(1);
                    int type = cursor.getInt(2);
                    long time = cursor.getLong(3);
                    HistoryCard historyCard = new HistoryCard(mContext,
                            mBaseScrollView, -1, id);
                    historyCard.setData(type, text, time, true, true);
                    historyCard.setLauncherManager(this);
                }
            } finally {
                cursor.close();
            }
        }
    }

    public void startVoiceCommand() {
        if (mVoiceIconLayout.isShown() == false && mVoiceManager.isReady()) {
            mVoiceIconLayout.setVisibility(View.VISIBLE);
            mVoiceManager.onStartVoiceCommand();
        }
    }

    public void stopVoiceCommand() {
        if (mVoiceIconLayout.isShown() == true && mVoiceManager.isReady()) {
            mVoiceIconLayout.setVisibility(View.GONE);
            mVoiceManager.onStopVoiceCommand();
        }
    }

    private void showVoiceCMD() {
        if (mVoiceCMDView.isShown() == false) {
            mVoiceCMDView.setVisibility(View.VISIBLE);
            mVoiceCMDView.startAnimation(mShowVoiceAnimation);
            mCheckCommandHandler.postDelayed(mVoiceCommandHide,
                    VOICE_DELAY_TIEM);
            mCheckRun = true;
        }
    }

    private void hideVoiceCMD() {
        if (mVoiceCMDView.isShown() == true) {
            if (mCheckRun == true) {
                mCheckCommandHandler.removeCallbacks(mVoiceCommandHide);
                mCheckRun = false;
            }
            mVoiceCMDView.setVisibility(View.GONE);
            mVoiceCMDView.startAnimation(mHideVoiceAnimation);
        }
    }

    private void showMenu() {
        if (mMenuScrollView.isShown() == false
                && mMenuScrollView.getListItemCount() == 0) {
            Card currentCard = (Card) mBaseScrollView.getCurrentView();
            currentCard.onBeforeShowMenu();
            currentCard.prepareMenu(mMenuScrollView);
            if (mMenuScrollView.getListItemCount() > 0) {
                mMenuScrollView.setVisibility(View.VISIBLE);
                mMenuScrollView.startAnimation(mShowMenuAnimation);
            }
        }
    }

    private void hideMenu() {
        if (mMenuScrollView.isShown() == true) {
            Card currentCard = (Card) mBaseScrollView.getCurrentView();
            mMenuScrollView.setVisibility(View.GONE);
            mMenuScrollView.startAnimation(mHideMenuAnimation);
            currentCard.onAfterHideMenu();
        }
    }

    public void showLiveMenu() {
        if (mLiveMenuList.isShown() == false
                && mLiveMenuList.getListItemCount() == 0) {
            if (mLiveMenuList.getListItemCount() > 0) {
                mLiveMenuList.setVisibility(View.VISIBLE);
                mLiveMenuList.startAnimation(mShowLiveMenuAnimation);
            } else {
                mFeatureCard.prepareMenu(mLiveMenuList);
                if (mLiveMenuList.getListItemCount() > 0) {
                    mLiveMenuList.setVisibility(View.VISIBLE);
                    mLiveMenuList.startAnimation(mShowLiveMenuAnimation);
                }
            }
        }
    }

    public void hideLiveMenu() {
        if (mLiveMenuList.isShown() == true) {
            mLiveMenuList.setVisibility(View.GONE);
            mLiveMenuList.startAnimation(mHideLiveMenuAnimation);
        }
    }

    public void addLiveView(View view, FrameViewListener listener) {
        mWindowManager.addView(mLiveBaseLayout, mWindowParams);
        view.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        mLiveFrame.addView(view);
        mLiveFrame.setFrameViewListener(listener);
    }

    public void publish(boolean flag) {
        mLiveFrame.setVisibility(View.VISIBLE);
        mLiveFrame.updateTime(System.currentTimeMillis());
        if (flag == true) {
            mLiveFrame.startAnimation(mShowLiveCardAnimation);
        }
    }

    public void removeLiveView() {
        if (mLiveFrame.getChildCount() > 0) {
            mLiveFrame.updateTime(0);
            mLiveFrame.removeAllViews();
            mLiveFrame.setVisibility(View.GONE);
            mFeatureCard = null;
            mWindowManager.removeView(mLiveBaseLayout);
        }
    }

    public CardScrollView getLiveMenuList() {
        return mLiveMenuList;
    }

    public void addBrowserCard() {
        ((Card) mBaseScrollView.getCurrentView()).checkVisible();

        final BrowserCard browserCard = 
                new BrowserCard(mContext, mBaseScrollView, 0, -1);
        browserCard.setLauncherManager(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopVoiceCommand();
                mBaseScrollView.moveToPosition(browserCard.getPosition());
            }
        }, 50);
    }

    public void addHistoryCard(int type, long time, String text, int id,
            byte[] data, Camera.Parameters parameter) {
        hideMenu();
        hideSubMenu();

        ((Card) mBaseScrollView.getCurrentView()).checkVisible();

        final HistoryCard historyCard = new HistoryCard(mContext,
                mBaseScrollView, 0, id);
        if (parameter != null) {
            historyCard.setCameraParameter(parameter);
        }
        historyCard.setData(type, text, true, false, data);
        historyCard.setTime(time, true);
        historyCard.setLauncherManager(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopVoiceCommand();
                mBaseScrollView.moveToPosition(historyCard.getPosition());
            }
        }, 50);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                removeLiveView();
            }
        }, 200);
    }

    public View getContentView() {
        return mBaseLayout;
    }

    public void destroy() {
        if (mVoiceManager != null) {
            mVoiceManager.destroy();
        }

        if (mFeatureCard != null) {
            mFeatureCard.destroy();
        }
    }

    public void featureTakeAPicture(boolean autoTake) {
        if (mFeatureCard == null) {
            mFeatureCard = new TakeAPictureFeatureCard(mContext,
                    LauncherManager.this, autoTake);
        }
    }

    public void featureRecordAVideo() {
        if (mFeatureCard == null) {
            mVoiceManager.onStopVoiceCommand();
            hideVoiceCMD();
    
            new Handler().postDelayed(new Runnable() {
    
                @Override
                public void run() {
                    if (mFeatureCard == null) {
                        mFeatureCard = new RecordAVideoFeatureCard(mContext,
                                LauncherManager.this);
                    }
                }
            }, 100);
        }
    }

    public void featureConnectWiFi() {
        if (mFeatureCard == null) {
            mFeatureCard = new QRCodeFeatrueCard(mContext,
                    LauncherManager.this, QRCodeFeatrueCard.MODE_WIFI_CONNECT);
        }
    }

    public void featureAddSchedule() {
        if (mFeatureCard == null) {
            mFeatureCard = new QRCodeFeatrueCard(mContext,
                    LauncherManager.this, QRCodeFeatrueCard.MODE_SET_SCHEDULE);
        }
    }

    public void featureAddWeatherCity() {
        if (mFeatureCard == null) {
            mFeatureCard = new QRCodeFeatrueCard(mContext,
                    LauncherManager.this, QRCodeFeatrueCard.MODE_SET_WEATHER);
        }
    }

    public void featurePlayVideo(String path) {
        if (mFeatureCard == null) {
            mFeatureCard = new PlayVideoFeatureCard(mContext,
                    LauncherManager.this, path);
        }
    }

    public void readQRCode(String data, int mode) {
        removeLiveView();

        mQRCodeHandler.sendMessage(mQRCodeHandler.obtainMessage(mode,
                data));
    }

    public void showSubMenu(int parentId) {
        if (mSubMenuScrollView.isShown() == false &&
                mSubMenuScrollView.getListItemCount() == 0) {
            ((Card) mBaseScrollView.getCurrentView()).prepareSubMenu(parentId,
                    mSubMenuScrollView);
            if (mSubMenuScrollView.getListItemCount() > 0) {
                mSubMenuScrollView.setVisibility(View.VISIBLE);
                mSubMenuScrollView.startAnimation(mShowSubMenuAnimation);
            }
        }
    }

    public void hideSubMenu() {
        if (mSubMenuScrollView.isShown() == true) {
            mSubMenuScrollView.setVisibility(View.GONE);
            mSubMenuScrollView.startAnimation(mHideSubMenuAnimation);
        }
    }

    private onCardScrollViewListener mMainScroll = new onCardScrollViewListener() {
        @Override
        public void onCardScrollView(View view, int position, int action) {
            Card currentCard = (Card) mBaseScrollView.getCurrentView();
            switch (action) {
            case CardScrollView.VIEW_ACTION_DOWN:
                if (currentCard.canRemove()) {
                    int dbId = currentCard.getDataBaseId();
                    mContext.getContentResolver().delete(
                            HistoryColumns.CONTENT_URI,
                            HistoryColumns.ID + " = " + dbId, null);
                    mBaseScrollView.moveToDefault();
                    mBaseScrollView.removeListItem(currentCard);
                }
                break;
            case CardScrollView.VIEW_ACTION_UP:
                if (mBaseScrollView.isDefautlView()
                        && mVoiceIconLayout.isShown() == false) {
                    startVoiceCommand();
                }
                break;
            case CardScrollView.VIEW_ACTION_SINGLE_TAP:
                if (currentCard.isCanShowMenu() == true) {
                    showMenu();
                }
                break;
            case CardScrollView.VIEW_ACTION_SCROLL:
                stopVoiceCommand();
                break;
            case CardScrollView.VIEW_ACTION_IDLE:
                if (mVoiceCMDView.isShown() == false
                        && currentCard.isCanVoice() == true) {
                    startVoiceCommand();
                }
                break;
            }
        }
    };

    private onCardScrollViewListener mMenuScroll = new onCardScrollViewListener() {
        @Override
        public void onCardScrollView(View view, int position, int action) {
            switch (action) {
            case CardScrollView.VIEW_ACTION_DOWN:
                ((Card) mBaseScrollView.getCurrentView()).cancelMenu();
                hideMenu();
                break;
            case CardScrollView.VIEW_ACTION_UP:
                break;
            case CardScrollView.VIEW_ACTION_SINGLE_TAP:
                Card current = (Card) mBaseScrollView.getCurrentView();
                if (!current.isSupportSubMenu()) {
                    hideMenu();
                }
                current.onSelectMenu(view, view.getId());
                break;
            }
        }
    };

    private onCardScrollViewListener mSubMenuScroll = new onCardScrollViewListener() {
        @Override
        public void onCardScrollView(View view, int position, int action) {
            Card current = (Card) mBaseScrollView.getCurrentView();
            switch (action) {
            case CardScrollView.VIEW_ACTION_DOWN:
                hideSubMenu();
                break;
            case CardScrollView.VIEW_ACTION_UP:
                break;
            case CardScrollView.VIEW_ACTION_SINGLE_TAP:
                hideSubMenu();
                current.onSelectSubMenu(view, view.getId(), action);
                break;
            case CardScrollView.VIEW_ACTION_TO_LEFT:
            case CardScrollView.VIEW_ACTION_TO_RIGHT:
                if (view.getId() == R.id.volum_control) {
                    current.onSelectSubMenu(view, view.getId(), action);
                }
                break;
            }
        }
    };

    private Animation.AnimationListener mShowMenu = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_KEYPRESS_STANDARD);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            Card currentCard = (Card) mBaseScrollView.getCurrentView();
            currentCard.displaydMenu();
            mMenuScrollView.updateTime(System.currentTimeMillis());
        }
    };

    private Animation.AnimationListener mShowSubMenu = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_KEYPRESS_STANDARD);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            Card currentCard = (Card) mBaseScrollView.getCurrentView();
            currentCard.displaydMenu();
            mSubMenuScrollView.updateTime(System.currentTimeMillis());
        }
    };

    private Animation.AnimationListener mShowLiveMenu = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_KEYPRESS_STANDARD);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mLiveMenuList.updateTime(System.currentTimeMillis());
        }
    };

    private Animation.AnimationListener mHideMenu = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_FOCUS_NAVIGATION_DOWN);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mMenuScrollView.removeListClear();
            Card currentCard = (Card) mBaseScrollView.getCurrentView();
            currentCard.closeMenu();
            mMenuScrollView.updateTime(0);
        }
    };

    private Animation.AnimationListener mHideSubMenu = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_FOCUS_NAVIGATION_DOWN);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mSubMenuScrollView.removeListClear();
            mSubMenuScrollView.updateTime(0);
            // Card currentCard = (Card) mBaseScrollView.getCurrentView();
            // currentCard.closeMenu();
        }
    };

    private Animation.AnimationListener mHideLiveMenu = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_FOCUS_NAVIGATION_DOWN);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mLiveMenuList.removeListClear();
            mLiveMenuList.updateTime(0);
        }
    };

    private Animation.AnimationListener mShowVoice = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            stopVoiceCommand();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    startVoiceCommand();
                }
            }, VOICE_START_TIEM);
        }
    };

    private Animation.AnimationListener mHideVoice = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            stopVoiceCommand();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
        }
    };

    private Animation.AnimationListener mShowLiveCard = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            tpSound(AudioManager.FX_KEY_CLICK);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (mFeatureCard != null) {
                mFeatureCard.onFinishAnimation();
            }
        }
    };

    private OnClickListener mLiveClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            tpSound(AudioManager.FX_KEYPRESS_STANDARD);
            if (mLiveMenuList.isShown() == false && mFeatureCard != null) {
                mFeatureCard.prepareMenu(mLiveMenuList);
                showLiveMenu();
            }
        }
    };

    private onCardScrollViewListener mLiveMenuListener = new onCardScrollViewListener() {

        @Override
        public void onCardScrollView(View view, int position, int action) {
            tpSound(AudioManager.FX_KEY_CLICK);
            switch (action) {
            case CardScrollView.VIEW_ACTION_SINGLE_TAP:
                if (mFeatureCard != null) {
                    mFeatureCard.selectMenu(view.getId());
                    hideLiveMenu();
                }
                break;
            case CardScrollView.VIEW_ACTION_DOWN:
                hideLiveMenu();
                break;
            }
        }
    };

    private OnGenericMotionListener mVoiceCMDMotionListener = new View.OnGenericMotionListener() {

        @Override
        public boolean onGenericMotion(View v, MotionEvent event) {
            int source = event.getSource();

            if (source == InputDevice.SOURCE_MOUSE) {
                int action = event.getActionMasked();

                switch (action) {
                case MotionEvent.ACTION_SCROLL:
                    if (event.getAxisValue(MotionEvent.AXIS_VSCROLL) > 0.0f) {
                        Log.i(TAG, "Drag up");
                    } else if (event.getAxisValue(MotionEvent.AXIS_VSCROLL) < 0.0f) {
                        Log.i(TAG, "Drag down");
                        hideVoiceCMD();
                    } else if (event.getAxisValue(MotionEvent.AXIS_HSCROLL) < 0.0f) {
                        Log.i(TAG, "Drag left");
                    } else if (event.getAxisValue(MotionEvent.AXIS_HSCROLL) > 0.0f) {
                        Log.i(TAG, "Drag right");
                    }
                    return true;
                case MotionEvent.ACTION_HOVER_MOVE:
                    return true;
                }
            }
            return false;
        }
    };

    private View.OnTouchListener mVoiceCMDListener = new View.OnTouchListener() {
        private static final float OFF_SET = 40.0f;

        private float mOldX;
        private float mOldY;
        private boolean mInSide;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction() & MotionEvent.ACTION_MASK;
            switch (action) {
            case MotionEvent.ACTION_DOWN:
                mOldX = event.getX();
                mOldY = event.getY();
                mInSide = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mInSide == true) {
                    if (Math.abs(event.getX() - mOldX) > OFF_SET
                            && Math.abs(event.getY() - mOldY) > OFF_SET) {
                        mInSide = false;
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                float deltaX = event.getX() - mOldX;
                float deltaY = event.getY() - mOldY;
                float absX = Math.abs(deltaX);
                float absY = Math.abs(deltaY);

                 if (absX < OFF_SET && absY > OFF_SET) {
                    // drag up / down
                    if (mInSide == true) {
                        if (deltaY > 0) { // down
                            if (mVoiceCMDView.isShown() == true) {
                                hideVoiceCMD();
                            }
                        }
                    }
                }
                break;
            }
            return false;
        }
    };

    private VoiceManagerListener mVoiceListener = new VoiceManagerListener() {

        @Override
        public void onVoiceCommand(String command) {
            Log.i(TAG, "Receive string :: " + command);
            mVoiceIconLayout.setVisibility(View.GONE);

            if (command == null) {
                if (mVoiceCMDView.isShown() == true) {
                    if (mCheckRun == true) {
                        mCheckCommandHandler.removeCallbacks(mVoiceCommandHide);
                    }
                }
                hideVoiceCMD();
                // finish voice detecting
                return;
            } else if (command.equals(ENGLISH.COMMAND_OK_GLASS)
                    || command.equals(CHINESE.COMMAND_OK_GLASS)) {
                // OK glass
                if (mVoiceCMDView.isShown() == false) {
                    Card currentCard = (Card) mBaseScrollView.getCurrentView();
                    if (currentCard.isCanVoice()) {
                        showVoiceCMD();
                    }
                }
            } else if (command.equals(ENGLISH.COMMAND_TAKE_PICTURE)
                    || command.equals(CHINESE.COMMAND_TAKE_PICTURE)) {
                // Take a picture
                if (mVoiceCMDView.isShown() == true) {
                    featureTakeAPicture(true);
                }
                hideVoiceCMD();
            } else if (command.equals(ENGLISH.COMMAND_RECORD_VIDEO)
                    || command.equals(CHINESE.COMMAND_RECORD_VIDEO)) {
                // Record a video
                if (mVoiceCMDView.isShown() == true) {
                    featureRecordAVideo();
                }

                hideVoiceCMD();
            } else {
                if (Common.SUPPORT_TEST_VOICE_COMMAND) {
                    if (mVoiceCMDView.isShown() == false) {
                        Card currentCard = (Card) mBaseScrollView
                                .getCurrentView();
                        if (currentCard.isCanVoice()) {
                            showVoiceCMD();
                        }
                    } else {
                        featureRecordAVideo();
                        hideVoiceCMD();
                    }
                } else {
                    if (mVoiceCMDView.isShown() == true) {
                        hideVoiceCMD();
                    }
                }
            }
        }

        @Override
        public void onVoiceStandy() {
            startVoiceCommand();
        }
    };

    private Runnable mVoiceCommandHide = new Runnable() {
        @Override
        public void run() {
            mCheckRun = false;
            hideVoiceCMD();
        }
    };

    private class QRCodeHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            String data = (String) msg.obj;
            switch (msg.what) {
            case QRCodeFeatrueCard.MODE_WIFI_CONNECT:
            if (data.startsWith(WiFiType.WIFI_TYPE)) {
                String wifiData = data.replace(WiFiType.WIFI_TYPE, "");
                String[] splite = wifiData.split(WiFiType.WIFI_PASS);
                String ssid = splite[0].replace(WiFiType.WIFI_SSID, "");
                String password = splite[1];

                WifiManager manager = (WifiManager) mContext
                        .getSystemService(Context.WIFI_SERVICE);
                WifiConfiguration wfc = new WifiConfiguration();
                wfc = new WifiConfiguration();
                wfc.SSID = "\"".concat(ssid).concat("\"");
                wfc.status = WifiConfiguration.Status.DISABLED;
                wfc.priority = 40;

                // WPA, WP2
                wfc.allowedAuthAlgorithms
                        .set(WifiConfiguration.AuthAlgorithm.OPEN);
                wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                wfc.allowedPairwiseCiphers
                        .set(WifiConfiguration.PairwiseCipher.CCMP);
                wfc.allowedPairwiseCiphers
                        .set(WifiConfiguration.PairwiseCipher.TKIP);
                wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                wfc.preSharedKey = "\"".concat(password).concat("\"");
                int networkId = manager.addNetwork(wfc);
                if (networkId != -1) {
                    manager.enableNetwork(networkId, true);
                }
            }
            break;
            case QRCodeFeatrueCard.MODE_SET_SCHEDULE:
                if (data.startsWith(ScheduleType.SCHEDULE_TYPE)) {
                    String scheduleData = data.replace(ScheduleType.SCHEDULE_TYPE, "");
                    String[] datas = scheduleData.split(ScheduleType.SCHEDULE_DATA);
                    String sTime = datas[0].replace(ScheduleType.SCHEDULE_TIME, "");
                    String sData = datas[1].replace(ScheduleType.SCHEDULE_DATA, "");

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    try {
                        Date date = format.parse(sTime);
                        ContentValues values = new ContentValues();
                        values.put(ScheduleColumns.TIME, date.getTime());
                        values.put(ScheduleColumns.DATA, sData);
                        mContext.getContentResolver().insert(ScheduleColumns.CONTENT_URI, values);
                    } catch (ParseException e) {
                    }
                }
                break;
            case QRCodeFeatrueCard.MODE_SET_WEATHER:
                if (data.startsWith(CityType.CITY_TYPE)) {
                    String cityName = data.replace(CityType.CITY_TYPE, "").replace(CityType.CITY_NAME, "");
                    ContentValues values = new ContentValues();
                    values.put(WeatherColumns.NAME, cityName);
                    values.put(WeatherColumns.DEFAULT, 1);
                    mContext.getContentResolver().insert(WeatherColumns.CONTENT_URI, values);
                }
                break;
            }
        }
    }
}