package com.agreeyamobility.glasslauncher.provider;

import java.io.File;

import com.agreeyamobility.glasslauncher.Common.HistoryType;
import com.agreeyamobility.glasslauncher.Common.Weather;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class LauncherProvider extends ContentProvider {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "launcher.db";

    private static final int URI_INDEX_HISTORY = 1;
    private static final int URI_INDEX_HISTORY_ID = 2;
    private static final int URI_INDEX_SCHEDULE = 3;
    private static final int URI_INDEX_SCHEDULE_ID = 4;
    private static final int URI_INDEX_WEATHER = 5;
    private static final int URI_INDEX_WEATHER_ID = 6;

    private static final String DATABASE_AUTHORITY = "com.agreeyamobility.glass";

    private SQLiteDatabase mDatabase;

    private static final UriMatcher mMatcher;

    static {
        mMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mMatcher.addURI(DATABASE_AUTHORITY, "history", URI_INDEX_HISTORY);
        mMatcher.addURI(DATABASE_AUTHORITY, "history/#", URI_INDEX_HISTORY_ID);
        mMatcher.addURI(DATABASE_AUTHORITY, "schedule", URI_INDEX_SCHEDULE);
        mMatcher.addURI(DATABASE_AUTHORITY, "schedule/#", URI_INDEX_SCHEDULE_ID);
        mMatcher.addURI(DATABASE_AUTHORITY, "weather", URI_INDEX_WEATHER);
        mMatcher.addURI(DATABASE_AUTHORITY, "weather/#", URI_INDEX_WEATHER_ID);
    }

    @Override
    public boolean onCreate() {
        mDatabase = new HistoryDatabase(getContext(), DATABASE_NAME, null,
                DATABASE_VERSION).getReadableDatabase();
        return (mDatabase == null) ? false : true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        String where = selection;
        String orderBy = sortOrder;

        String tableName = null;

        switch (mMatcher.match(uri)) {
        case URI_INDEX_HISTORY_ID:
            if (where == null) {
                where = HistoryColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_HISTORY:
            tableName = HistoryColumns.TABLE_NAME;
            if (orderBy == null) {
                orderBy = HistoryColumns.TIME + " DESC";
            }
            break;
        case URI_INDEX_SCHEDULE_ID:
            if (where == null) {
                where = ScheduleColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_SCHEDULE:
            tableName = ScheduleColumns.TABLE_NAME;
            if (orderBy == null) {
                orderBy = ScheduleColumns.ID + " asc";
            }
            break;
        case URI_INDEX_WEATHER_ID:
            if (where == null) {
                where = WeatherColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_WEATHER:
            tableName = WeatherColumns.TABLE_NAME;
            if (orderBy == null) {
                orderBy = WeatherColumns.NAME + " asc";
            }
            break;
        }

        Cursor result = mDatabase.query(tableName, projection, where,
                selectionArgs, null, null, orderBy);
        return result;
    }

    @Override
    public String getType(Uri uri) {
        switch (mMatcher.match(uri)) {
        case URI_INDEX_HISTORY:
            return "vnd.com.agreeyamobility.glass.dir/history";
        case URI_INDEX_HISTORY_ID:
            return "vnd.com.agreeyamobility.glass.item/history";
        case URI_INDEX_SCHEDULE:
            return "vnd.com.agreeyamobility.glass.dir/schedule";
        case URI_INDEX_SCHEDULE_ID:
            return "vnd.com.agreeyamobility.glass.item/schedule";
        case URI_INDEX_WEATHER:
            return "vnd.com.agreeyamobility.glass.dir/weather";
        case URI_INDEX_WEATHER_ID:
            return "vnd.com.agreeyamobility.glass.item/weather";
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri resultUri = null;
        long id = 0;

        switch (mMatcher.match(uri)) {
        case URI_INDEX_HISTORY:
            id = mDatabase.insert(HistoryColumns.TABLE_NAME, null, values);
            resultUri = ContentUris.withAppendedId(HistoryColumns.CONTENT_URI,
                    id);
            break;
        case URI_INDEX_SCHEDULE:
            id = mDatabase.insert(ScheduleColumns.TABLE_NAME, null, values);
            resultUri = ContentUris.withAppendedId(ScheduleColumns.CONTENT_URI,
                    id);
            break;
        case URI_INDEX_WEATHER:
            Cursor cursor = mDatabase.query(WeatherColumns.TABLE_NAME,
                    new String[] { WeatherColumns.ID }, WeatherColumns.DEFAULT
                            + " = 1", null, null, null, WeatherColumns.ID
                            + " ASC");
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                int defaultId = cursor.getInt(0);
                ContentValues weatherValues = new ContentValues();
                weatherValues.put(WeatherColumns.DEFAULT, 0);
                mDatabase.update(WeatherColumns.TABLE_NAME, weatherValues,
                        WeatherColumns.ID + " = " + defaultId, null);
                cursor.close();
            }

            id = mDatabase.insert(WeatherColumns.TABLE_NAME, null, values);
            resultUri = ContentUris.withAppendedId(WeatherColumns.CONTENT_URI,
                    id);
            break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return resultUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String where = selection;
        String[] whereArgs = selectionArgs;

        String tableName = null;

        switch (mMatcher.match(uri)) {
        case URI_INDEX_HISTORY_ID:
            if (where == null) {
                where = HistoryColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_HISTORY:
            tableName = HistoryColumns.TABLE_NAME;

            Cursor cursor = mDatabase.query(tableName,
                    new String[] { HistoryColumns.ID, HistoryColumns.TYPE,
                            HistoryColumns.TEXT }, where, null, null, null,
                    HistoryColumns.ID + " ASC");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        int type = cursor.getInt(1);
                        if (type == HistoryType.TYPE_IMAGE
                                || type == HistoryType.TYPE_VIDEO) {
                            removeFile(cursor.getString(2));
                        }
                    }
                } finally {
                    cursor.close();
                }
            }
            break;
        case URI_INDEX_SCHEDULE_ID:
            if (where == null) {
                where = ScheduleColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_SCHEDULE:
            tableName = ScheduleColumns.TABLE_NAME;
            break;
        case URI_INDEX_WEATHER_ID:
            if (where == null) {
                where = WeatherColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_WEATHER:
            tableName = WeatherColumns.TABLE_NAME;
            break;
        }

        int result = mDatabase.delete(tableName, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        String where = selection;
        String[] whereArgs = selectionArgs;

        String tableName = null;

        switch (mMatcher.match(uri)) {
        case URI_INDEX_HISTORY_ID:
            if (where == null) {
                where = HistoryColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_HISTORY:
            tableName = HistoryColumns.TABLE_NAME;
            break;
        case URI_INDEX_SCHEDULE_ID:
            if (where == null) {
                where = ScheduleColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_SCHEDULE:
            tableName = ScheduleColumns.TABLE_NAME;
            break;
        case URI_INDEX_WEATHER_ID:
            if (where == null) {
                where = WeatherColumns.ID + " = " + uri.getLastPathSegment();
            }
        case URI_INDEX_WEATHER:
            tableName = WeatherColumns.TABLE_NAME;

            Cursor cursor = mDatabase.query(WeatherColumns.TABLE_NAME,
                    new String[] { WeatherColumns.ID }, WeatherColumns.DEFAULT
                            + " = 1", null, null, null, WeatherColumns.ID
                            + " ASC");
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                int defaultId = cursor.getInt(0);
                ContentValues weatherValues = new ContentValues();
                weatherValues.put(WeatherColumns.DEFAULT, 0);
                mDatabase.update(WeatherColumns.TABLE_NAME, weatherValues,
                        WeatherColumns.ID + " = " + defaultId, null);
                cursor.close();
            }
            break;
        }
        
        int result = mDatabase.update(tableName, values, where, whereArgs);

        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    private void removeFile(String path) {
        File file = new File(path);
        if (file.exists() == true && file.isDirectory() == false) {
            file.delete();
        }
    }

    public static final class WeatherColumns {
        public static final String TABLE_NAME = "weather";

        public static final Uri CONTENT_URI = Uri.parse("content://"
                + DATABASE_AUTHORITY + "/weather");
        public static final String ID = "_id";
        public static final String LON = "lon";
        public static final String LAT = "lat";
        public static final String NAME = "name";
        public static final String DEFAULT = "main";
    }

    public static final class ScheduleColumns {
        public static final String TABLE_NAME = "schedule";

        public static final Uri CONTENT_URI = Uri.parse("content://"
                + DATABASE_AUTHORITY + "/schedule");
        public static final String ID = "_id";
        public static final String TIME = "time";
        public static final String DATA = "data";
    }

    public static final class HistoryColumns {
        public static final String TABLE_NAME = "history";

        public static final Uri CONTENT_URI = Uri.parse("content://"
                + DATABASE_AUTHORITY + "/history");
        public static final String ID = "_id";
        public static final String TYPE = "type";
        public static final String TEXT = "text";
        public static final String TIME = "time";
    }

    private class HistoryDatabase extends SQLiteOpenHelper {

        public HistoryDatabase(Context context, String name,
                CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createHistoryTable(db);
            createScheduleTable(db);
            createWeatherTable(db);

            firstWeatherSetting(db);
        }

        private void firstWeatherSetting(SQLiteDatabase db) {
            ContentValues values = new ContentValues();
            values.put(WeatherColumns.NAME, Weather.WEATHER_CITY_BEIJING);
            values.put(WeatherColumns.DEFAULT, 1);
            db.insert(WeatherColumns.TABLE_NAME, null, values);
        }

        private void createHistoryTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + HistoryColumns.TABLE_NAME + " ("
                    + HistoryColumns.ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + HistoryColumns.TEXT + " TEXT, " + HistoryColumns.TYPE
                    + " INTEGER, " + HistoryColumns.TIME + " INTEGER);");
        }

        private void createScheduleTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + ScheduleColumns.TABLE_NAME + " ("
                    + ScheduleColumns.ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ScheduleColumns.TIME + " INTEGER, " + ScheduleColumns.DATA
                    + " TEXT);");
        }

        private void createWeatherTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + WeatherColumns.TABLE_NAME + " ("
                    + WeatherColumns.ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + WeatherColumns.LON + " TEXT, " + WeatherColumns.LAT
                    + " TEXT, " + WeatherColumns.DEFAULT + " INTEGER, "
                    + WeatherColumns.NAME + " TEXT);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE " + HistoryColumns.TABLE_NAME);
            db.execSQL("DROP TABLE " + ScheduleColumns.TABLE_NAME);
            db.execSQL("DROP TABLE " + WeatherColumns.TABLE_NAME);
            onCreate(db);
        }
    }
}