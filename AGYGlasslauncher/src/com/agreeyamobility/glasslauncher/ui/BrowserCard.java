package com.agreeyamobility.glasslauncher.ui;

import com.agreeyamobility.glasslauncher.Common;
import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.HistoryColumns;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BrowserCard extends Card {
    private WebView mWebView;
    private ProgressBar mProgressBar;
    // private WebSettings mSettings;

    private View mMenuDelete;

    public BrowserCard(Context context, CardScrollView base, int pos, int id) {
        super(context, base, pos, id);
    }

    @Override
    void createMenu() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                com.agreeyamobility.glasslauncher.Common.Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);

        mMenuDelete = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuDelete.setLayoutParams(params);
        mMenuDelete.setId(R.id.delete);
        ((ImageView) mMenuDelete.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_delete);
        ((TextView) mMenuDelete.findViewById(R.id.menuTitle))
                .setText(R.string.menu_delete);
    }

    @Override
    public void prepareMenu(CardScrollView menu) {
        menu.addListItem(mMenuDelete, true);
    }

    @Override
    public void onSelectMenu(View view, int id) {
        switch (id) {
        case R.id.delete:
            mBaseLayout.moveToDefault();
            mBaseLayout.removeListItem(this);
            break;
        }
    }

    @Override
    void onMotionEvent(int action, String event) {
    }

    @Override
    View createView(Context context, ViewGroup parent) {
        return mInflater.inflate(R.layout.card_content_browser, null, false);
    }

    @Override
    void initialize(View view) {
        mCanMenu = true;

        mProgressBar = (ProgressBar) view.findViewById(R.id.webViewProgress);
        mProgressBar.setMax(100);
        mWebView = (WebView) view.findViewById(R.id.webView);
        mWebView.setClickable(false);
        mWebView.setWebChromeClient(new WebStateChrome());
        mWebView.setWebViewClient(new WebStateClient());
        // mSettings = mWebView.getSettings();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mWebView.loadUrl(Common.BROWSER_DEFAULT_URL);
            }
        }, 500);
    }

    @Override
    boolean isDefault() {
        return false;
    }

    @Override
    void updateUi() {
    }

    private class WebStateChrome extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            mProgressBar.setProgress(newProgress);
        }
    }

    private class WebStateClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setProgress(0);
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            // mSettings.setJavaScriptEnabled(true);
            super.onPageFinished(view, url);
        }

    }
}