package com.agreeyamobility.glasslauncher.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.ScheduleColumns;

public class CalendarCard extends Card {
    private static final String DISPLAY_DATE_FORMAT = "yyyy-MM-dd HH:mm";
    private static final int MAX_DISPLAY_LENGHT = 15;

    private ContentObserver mContentObserver;
    private ContentResolver mResolver;
    private View mAddMenuView;
    private TextView mEventView;

    public CalendarCard(Context context, CardScrollView base) {
        super(context, base, -1, -1);
    }

    @Override
    View createView(Context context, ViewGroup parent) {
        return mInflater.inflate(R.layout.card_content_calendar, parent, false);
    }

    @Override
    void initialize(View view) {
        mContentObserver = new ContentObserver(new Handler()) {

            @Override
            public void onChange(boolean selfChange) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                long startTime = calendar.getTimeInMillis();

                calendar.set(Calendar.HOUR, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);

                long endTime = calendar.getTimeInMillis();

                String where = ScheduleColumns.TIME + " >= " + startTime  + " AND "
                        + ScheduleColumns.TIME + " <= " + endTime;

                Cursor cursor = mResolver.query(ScheduleColumns.CONTENT_URI,
                        new String[] {ScheduleColumns.ID, ScheduleColumns.DATA, ScheduleColumns.TIME},
                        where, null, ScheduleColumns.TIME + " ASC");
                if (cursor != null) {
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        String data = cursor.getString(1);
                        if (data.length() > MAX_DISPLAY_LENGHT) {
                            mEventView.setText(String.format("%s...", data.substring(0, MAX_DISPLAY_LENGHT)));
                        } else {
                            mEventView.setText(data);
                        }
                    }
                    cursor.close();
                }
            }
        };

        Calendar calendar = Calendar.getInstance();
        String week = getContext().getResources().getStringArray(
                R.array.week_string)[calendar.get(Calendar.DAY_OF_WEEK) - 1];
        ((TextView) view.findViewById(R.id.calendaWeek)).setText(week);
        ((TextView) view.findViewById(R.id.calendaDay)).setText(String
                .valueOf(calendar.get(Calendar.DATE)));
        mEventView = (TextView) view.findViewById(R.id.calendaEvent);
        mCanMenu = true;

        // check database
        mResolver = getContext().getContentResolver();
        mResolver.registerContentObserver(ScheduleColumns.CONTENT_URI, true, mContentObserver);

        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        long startTime = calendar.getTimeInMillis();

        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        long endTime = calendar.getTimeInMillis();

        String where = ScheduleColumns.TIME + " >= " + startTime  + " AND "
                + ScheduleColumns.TIME + " <= " + endTime;

        Cursor cursor = mResolver.query(ScheduleColumns.CONTENT_URI,
                new String[] {ScheduleColumns.ID, ScheduleColumns.DATA, ScheduleColumns.TIME},
                where, null, ScheduleColumns.TIME + " ASC");
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                String data = cursor.getString(1);
                if (data.length() > MAX_DISPLAY_LENGHT) {
                    mEventView.setText(String.format("%s...", data.substring(0, MAX_DISPLAY_LENGHT)));
                } else {
                    mEventView.setText(data);
                }
            }
            cursor.close();
        }
    }

    @Override
    public void createMenu() {
        mAddMenuView = mInflater.inflate(R.layout.menu_layout, null, false);
        mAddMenuView.setId(R.id.add_schedule);

        ((ImageView) mAddMenuView.findViewById(R.id.menuIcon))
                .setImageResource(android.R.drawable.ic_menu_add);
        ((TextView) mAddMenuView.findViewById(R.id.menuTitle))
                .setText(R.string.menu_add_schedule);
    }

    @Override
    public void prepareMenu(CardScrollView menu) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        long startTime = calendar.getTimeInMillis();

        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        long endTime = calendar.getTimeInMillis();

        String where = ScheduleColumns.TIME + " >= " + startTime  + " AND "
                + ScheduleColumns.TIME + " <= " + endTime;

        Cursor cursor = mResolver.query(ScheduleColumns.CONTENT_URI,
                new String[] {ScheduleColumns.ID, ScheduleColumns.DATA, ScheduleColumns.TIME},
                where, null, ScheduleColumns.TIME + " ASC");
        boolean isDefault = true;

        if (cursor != null) {
            SimpleDateFormat format = new SimpleDateFormat(DISPLAY_DATE_FORMAT);
            while (cursor.moveToNext()) {
                String data = cursor.getString(1);

                final View eventView = mInflater.inflate(
                        R.layout.schedule_layout_content, null, false);
                eventView.setTag(cursor.getInt(0));
                ((TextView) eventView
                        .findViewById(R.id.scheduleData)).setText(data);

                ((TextView) eventView
                        .findViewById(R.id.scheduleTime)).setText(
                                format.format(new Date(cursor.getLong(2))));
                menu.addListItem(eventView, isDefault);

                if (isDefault == true) {
                    isDefault = false;
                }
            }
            cursor.close();
        }

        menu.addListItem(mAddMenuView, isDefault);
    }

    @Override
    public void onSelectMenu(View view, int id) {
        switch (id) {
        case R.id.add_schedule:
            mLauncherManager.featureAddSchedule();
            break;
        }
    }

    @Override
    void onMotionEvent(int action, String event) {
    }

    @Override
    boolean isDefault() {
        return false;
    }

    @Override
    void updateUi() {

    }
}