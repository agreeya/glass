package com.agreeyamobility.glasslauncher.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.agreeyamobility.glasslauncher.Common.Display;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;


public abstract class Card extends FrameLayout {
    protected final LayoutInflater mInflater;
    protected final CardScrollView mBaseLayout;
    protected CardScrollView mMenuLayout;

    protected LinearLayout.LayoutParams mParams;

    protected boolean mSupportSubMenu;
    protected boolean mCanMenu;
    protected boolean mCanVoice;

    protected LauncherManager mLauncherManager;

    private int mPosition;
    private int mId;

    protected boolean mCanRemove;

    private UiUpdateHandler mUiUpdateHandler;

    public Card(Context context, CardScrollView base, int pos, int id) {
        super(context);

        mCanRemove = false;

        mId = id;
        mBaseLayout = base;
        mInflater = LayoutInflater.from(context);
        mSupportSubMenu = false;

        mParams = new LinearLayout.LayoutParams(
                Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);

        View view = createView(getContext(), this);
        this.addView(view);
        initialize(view);
        this.setLayoutParams(mParams);
        view.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        if (pos < 0) {
            mPosition = mBaseLayout.addListItem(this, isDefault());
        } else {
            mPosition = mBaseLayout.addListItem(this, isDefault(), pos);
        }

        mUiUpdateHandler = new UiUpdateHandler();
        mUiUpdateHandler.sendEmptyMessage(0);
        createMenu();
    }

    public void voiceCommandEvent(String event) {
        onMotionEvent(CardScrollView.VIEW_ACTION_VOICE, event);
    }

    public boolean isCanShowMenu() {
        return mCanMenu;
    }

    public boolean isSupportSubMenu() {
        return mSupportSubMenu;
    }

    public boolean isCanVoice() {
        return mCanVoice;
    }

    public int getPosition() {
        return mPosition;
    }

    public void updatePosition(int position) {
        mPosition = position;
    }

    public boolean canRemove() {
        return mCanRemove;
    }

    public int getDataBaseId() {
        return mId;
    }

    public void onBeforeShowMenu() {
    }

    public void onSelectMenu(View view, int id) {
    }

    public void onSelectSubMenu(View view, int id, int action) {
    }


    public void onAfterHideMenu() {
    }

    public void displaydMenu() {
    }

    public void cancelMenu() {
    }

    public void closeMenu() {
    }

    public void checkVisible() {
    }

    public void setLauncherManager(LauncherManager manager) {
        mLauncherManager = manager;
    }

    public void prepareMenu(CardScrollView menu) {
    }

    public void prepareSubMenu(int parentId, CardScrollView menu) {
    }

    abstract void createMenu();

    abstract void onMotionEvent(int action, String event);

    abstract View createView(Context context, ViewGroup parent);

    abstract void initialize(View view);

    abstract boolean isDefault();

    abstract void updateUi();

    private class UiUpdateHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 0 :
                this.sendEmptyMessage(1);
                break;
            case 1 :
                updateUi();
                this.sendEmptyMessageDelayed(1, 30000);
                break;
            case 2 :
                this.removeMessages(1);
                this.removeMessages(0);
                break;
            }
        }
    }
}