package com.agreeyamobility.glasslauncher.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.agreeyamobility.glasslauncher.Common;
import com.agreeyamobility.glasslauncher.Common.Display;

public class CardScrollView extends HorizontalScrollView implements
        OnTouchListener, OnGestureListener {
	private static final String TAG = "CardScrollView";
    private static final boolean SUPPORT_MOTION_EVENT = true;

    public static final int VIEW_ACTION_SINGLE_TAP = 0x01;
    public static final int VIEW_ACTION_UP = 0x02;
    public static final int VIEW_ACTION_DOWN = 0x03;
    public static final int VIEW_ACTION_IDLE = 0x04;
    public static final int VIEW_ACTION_SCROLL = 0x05;
    public static final int VIEW_ACTION_VOICE = 0x06;
    public static final int VIEW_ACTION_TO_LEFT = 0x07;
    public static final int VIEW_ACTION_TO_RIGHT = 0x08;

    private static final int SWIPE_MIN_DISTANCE = 100;
    private static final int SWIPE_THRESHOLD_VELOCITY = 300;
    private static final int SWIPE_DETECTING_VALUE = 6;

    private static final int LOADING_ANIMATION_HEIGHT = 10;

    private GestureDetector mGestureDetector;

    private int mScrollTo = 0;
    private int mItemCount = 0;
    private int mCurrentPosition = 0;
    private int mItemWidth = Display.DEVICE_RESOLUTION_WIDTH;
    private int mMinimunDelta = 0;

    private int mDefaultPosition = 0;

    private float mCurrentPointX = 0;
    private float mOldPointX = 0;
    private float mOldPointY = 0;
    private boolean mFlingDisable = false;

    private boolean mEndCall;

    private LinearLayout mLinearLayout;
    private onCardScrollViewListener mListener;

    private boolean mMoveScrollCalled = false;

    private float mLoadThumPosition = 0;
    private boolean mShowLoadingUI = false;
    private Paint mLoadingBackPaint = new Paint();
    private Paint mLoadingThumbPaint = new Paint();
    private LoadingUIHandler mLoadingUIHandler;

    private boolean mIsCallTouch = false;

    // Scroll animation
    private ScrollMoveHandler mMoveHandler;
    private boolean mScrollBlock;

    private long mOldTime = 0;

    public CardScrollView(Context context) {
        this(context, null, -1);
    }

    public CardScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public CardScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    private void initialize() {
        mLoadingUIHandler = new LoadingUIHandler();

        mMoveHandler = new ScrollMoveHandler();

        mLoadingBackPaint.setAntiAlias(true);
        mLoadingBackPaint.setColor(0xffffffff);
        mLoadingThumbPaint.setAntiAlias(true);
        mLoadingThumbPaint.setColor(0xff000000);

        mScrollBlock = false;
        mEndCall = false;

        mGestureDetector = new GestureDetector(getContext(), this);

        this.setOnTouchListener(this);
        this.setOverScrollMode(OVER_SCROLL_NEVER);
        this.setSmoothScrollingEnabled(true);

        if (SUPPORT_MOTION_EVENT) {
            this.setFocusable(true);
            this.setFocusableInTouchMode(true);
            this.setOnGenericMotionListener(mMotionListener);
        }

        mLinearLayout = new LinearLayout(getContext());
        mLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        mLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        this.addView(mLinearLayout);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int childCount = this.getChildCount();
        if (childCount > 0) {
            ViewGroup frame = (ViewGroup) this.getChildAt(0);
            mItemCount = frame.getChildCount();

            if (mItemCount > 0) {
                mItemWidth = frame.getChildAt(0).getWidth();
            }
        }

        int width = MeasureSpec.getSize(widthMeasureSpec);
        mMinimunDelta = width / SWIPE_DETECTING_VALUE;

        int moveDelta = mItemWidth * mCurrentPosition;
        this.scrollTo(moveDelta, 0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            mIsCallTouch = false;

        if (mShowLoadingUI == true) {
            return true;
        }
        if (mGestureDetector.onTouchEvent(event)) {
            return true;
        }

        Boolean returnValue = mGestureDetector.onTouchEvent(event);

        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            mOldPointX = event.getX();
            mOldPointY = event.getY();

            int childCount = this.getChildCount();
            if (childCount > 0) {
                ViewGroup frame = (ViewGroup) this.getChildAt(0);
                mItemCount = frame.getChildCount();

                if (mItemCount > 0) {
                    mItemWidth = frame.getChildAt(0).getWidth();
                }
            }

            break;
        case MotionEvent.ACTION_MOVE:
            break;
        case MotionEvent.ACTION_UP:
            mCurrentPointX = event.getX();
            int delta = (int) (mOldPointX - mCurrentPointX);
            int idelta = delta / mItemWidth;
            if (Math.abs(delta) > mMinimunDelta) {
                if (delta > 0) {
                    if (idelta == 0) {
                        mCurrentPosition = mCurrentPosition + 1;
                    } else {
                        mCurrentPosition = mCurrentPosition + idelta + 1;
                    }
                    if (mCurrentPosition >= mItemCount)
                        mCurrentPosition = mItemCount - 1;
                } else {
                    if (idelta == 0) {
                        mCurrentPosition = mCurrentPosition - 1;
                    } else {
                        mCurrentPosition = mCurrentPosition + idelta - 1;
                    }
                    if (mCurrentPosition < 0)
                        mCurrentPosition = 0;
                }
            } else {
                float currentY = event.getY();
                float deltaY = currentY - mOldPointY;

                if (Math.abs(deltaY) > SWIPE_MIN_DISTANCE) {
                    if (deltaY > 0) {
                        if (mListener != null && mLinearLayout != null) {
                            mListener.onCardScrollView(
                                    mLinearLayout.getChildAt(mCurrentPosition),
                                    mCurrentPosition, VIEW_ACTION_DOWN);
                        }
                    } else {
                        if (mListener != null && mLinearLayout != null) {
                            mListener.onCardScrollView(
                                    mLinearLayout.getChildAt(mCurrentPosition),
                                    mCurrentPosition, VIEW_ACTION_UP);
                        }
                    }
                }
            }
            mScrollTo = mCurrentPosition * mItemWidth;
            this.smoothScrollTo(mScrollTo, 0);
            returnValue = true;
            break;
        }

        return returnValue;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
            float velocityY) {
        if (mFlingDisable)
            return false;
        boolean returnValue = false;
        float ptx1 = 0, ptx2 = 0;
        if (e1 == null || e2 == null)
            return false;
        ptx1 = e1.getX();
        ptx2 = e2.getX();

        float delta = ptx1 - ptx2;
        int idelta = (int) (delta / mItemWidth);
        if ((int) Math.abs(delta) > SWIPE_MIN_DISTANCE) {
            if (delta > 0 && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                if (idelta == 0) {
                    mCurrentPosition = mCurrentPosition + 1;
                } else {
                    mCurrentPosition = mCurrentPosition + idelta + 1;
                }
                if (mCurrentPosition >= mItemCount)
                    mCurrentPosition = mItemCount - 1;
                returnValue = true;
            } else if (delta < 0
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                if (idelta == 0) {
                    mCurrentPosition = mCurrentPosition - 1;
                } else {
                    mCurrentPosition = mCurrentPosition + idelta - 1;
                }
                if (mCurrentPosition < 0)
                    mCurrentPosition = 0;
                returnValue = true;
            }
        }
        mScrollTo = mCurrentPosition * mItemWidth;
        this.smoothScrollTo(mScrollTo, 0);
        return returnValue;
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (mListener != null) {
            if (getScrollX() == mScrollTo) {
                if (mEndCall == false) {
                    if (mLinearLayout != null) {
                        mMoveScrollCalled = false;
                        mListener.onCardScrollView(
                                mLinearLayout.getChildAt(mCurrentPosition),
                                mCurrentPosition, VIEW_ACTION_IDLE);
                    }
                    mEndCall = true;
                }
            } else {
                mEndCall = false;
                if (mMoveScrollCalled == false) {
                    mMoveScrollCalled = true;
                    if (mLinearLayout != null) {
                        mListener.onCardScrollView(
                                mLinearLayout.getChildAt(mCurrentPosition),
                                mCurrentPosition, VIEW_ACTION_SCROLL);
                    }
                }
            }
        }
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
            float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if (mLinearLayout != null && mListener != null && mIsCallTouch == false) {
            mListener.onCardScrollView(
                    mLinearLayout.getChildAt(mCurrentPosition),
                    mCurrentPosition, VIEW_ACTION_SINGLE_TAP);
            mIsCallTouch = true;
        }
        return false;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        int source = event.getSource();

        if (source == InputDevice.SOURCE_MOUSE) {
            int action = event.getActionMasked();

            switch (action) {
            case MotionEvent.ACTION_SCROLL:
                if (event.getAxisValue(MotionEvent.AXIS_VSCROLL) > 0.0f) {
                    Log.i(TAG, "Drag up");
                    if (mLinearLayout != null && mListener != null) {
                        mListener.onCardScrollView(
                                mLinearLayout.getChildAt(mCurrentPosition),
                                mCurrentPosition, VIEW_ACTION_UP);
                    }
                } else if (event.getAxisValue(MotionEvent.AXIS_VSCROLL) < 0.0f) {
                    Log.i(TAG, "Drag down");
                    if (mLinearLayout != null && mListener != null) {
                        mListener.onCardScrollView(
                                mLinearLayout.getChildAt(mCurrentPosition),
                                mCurrentPosition, VIEW_ACTION_DOWN);
                    }
                } else if (event.getAxisValue(MotionEvent.AXIS_HSCROLL) < 0.0f) {
                    Log.i(TAG, "Drag left");
                    if (mLinearLayout != null) {
                        mListener.onCardScrollView(
                                mLinearLayout.getChildAt(mCurrentPosition),
                                mCurrentPosition, VIEW_ACTION_TO_LEFT);
                    }
                    this.goLeft();
                } else if (event.getAxisValue(MotionEvent.AXIS_HSCROLL) > 0.0f) {
                    Log.i(TAG, "Drag right");
                    if (mLinearLayout != null) {
                        mListener.onCardScrollView(
                                mLinearLayout.getChildAt(mCurrentPosition),
                                mCurrentPosition, VIEW_ACTION_TO_RIGHT);
                    }
                    this.goRight();
                }
                return true;
            case MotionEvent.ACTION_HOVER_MOVE:
//                boolean canNext = false;
//                if (mOldTime == 0) {
//                    return false;
//                } else {
//                    long systemTime = System.currentTimeMillis();
//                    if (Math.abs(mOldTime - systemTime) > Common.CLICK_CHECK_INTERVAL_TIME) {
//                        canNext = true;
//                        mOldTime = systemTime;
//                    }
//                }

                if (mLinearLayout != null && mListener != null) {
                    mListener.onCardScrollView(
                            mLinearLayout.getChildAt(mCurrentPosition),
                            mCurrentPosition, VIEW_ACTION_SINGLE_TAP);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (mShowLoadingUI) {
            float left = mCurrentPosition * mItemWidth;
            float right = left + getWidth();
            canvas.drawRect(left, getBottom() - LOADING_ANIMATION_HEIGHT,
                    right, getBottom(), mLoadingBackPaint);
            float thumbLeft = left + mLoadThumPosition;
            float thumbRight = thumbLeft + LOADING_ANIMATION_HEIGHT;
            canvas.drawRect(thumbLeft, getBottom() - LOADING_ANIMATION_HEIGHT,
                    thumbRight, getBottom(), mLoadingThumbPaint);
        }
    }

    public void updateTime(long time) {
        mOldTime = time;
    }

    private void updateInfo() {
        int childCount = this.getChildCount();
        if (childCount > 0) {
            ViewGroup frame = (ViewGroup) this.getChildAt(0);
            mItemCount = frame.getChildCount();

            if (mItemCount > 0) {
                mItemWidth = frame.getChildAt(0).getWidth();
            }
        }
    }

    public View getCurrentView() {
        return mLinearLayout.getChildAt(mCurrentPosition);
    }

    public int getListItemCount() {
        return mLinearLayout.getChildCount();
    }

    public boolean isDefautlView() {
        return mCurrentPosition == mDefaultPosition;
    }

    public boolean canShowMenu() {
        return mCurrentPosition > mDefaultPosition;
    }

    public int addListItem(View view) {
        return this.addListItem(view, false);
    }

    public int addListItem(View view, boolean setDefault) {
        return this.addListItem(view, setDefault, -1);
    }

    public int addListItem(View view, boolean setDefault, int index) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (params == null) {
            view.setLayoutParams(new LinearLayout.LayoutParams(
                    Display.DEVICE_RESOLUTION_WIDTH,
                    Display.DEVICE_RESOLUTION_HEIGHT));
        }

        if (mLinearLayout != null) {
            int result = 0;
            if (index < 0) {
                mLinearLayout.addView(view);
                result = mLinearLayout.getChildCount() - 1;
            } else if (index == 0) {
                int sIndex = mDefaultPosition + 1;
                int tIndex = mLinearLayout.getChildCount();
                if (index < tIndex) {
                    for (int i = sIndex; i < tIndex; i++) {
                        Card card = (Card) mLinearLayout.getChildAt(i);
                        card.updatePosition(i + 1);
                    }
                }
                mLinearLayout.addView(view, mDefaultPosition + 1);
                result = mDefaultPosition + 1;
            } else {
                int sIndex = index;
                int tIndex = mLinearLayout.getChildCount();
                if (index < tIndex) {
                    for (int i = sIndex; i < tIndex; i++) {
                        Card card = (Card) mLinearLayout.getChildAt(i);
                        card.updatePosition(i + 1);
                    }
                }
                mLinearLayout.addView(view, index);
                result = index;
            }

            if (setDefault == true) {
                mDefaultPosition = mLinearLayout.getChildCount() - 1;
                mCurrentPosition = mDefaultPosition;
            }
            mItemCount = mLinearLayout.getChildCount();
            return result;
        }
        return -1;
    }

    public void removeListItem(int index) {
        if (mLinearLayout != null) {
            mLinearLayout.removeViewAt(index);
        }
    }

    public void removeListItem(View view) {
        if (mLinearLayout != null) {
            mLinearLayout.removeView(view);
        }
    }

    public void removeListClear() {
        if (mLinearLayout != null) {
            mLinearLayout.removeAllViews();
        }
    }

    public void goLeft() {
        updateInfo();
        if (mCurrentPosition > 0 && mScrollBlock == false) {
            // mCurrentPosition = mCurrentPosition - 1;
            // mScrollTo = mCurrentPosition * mItemWidth;
            // this.smoothScrollTo(mScrollTo, 0);
            mScrollBlock = true;
            mMoveHandler
                    .sendMessage(mMoveHandler.obtainMessage(
                            ScrollMoveHandler.MESSAGE_START,
                            ScrollMoveHandler.GO_LEFT));
        }
    }

    public void goRight() {
        updateInfo();
        if (mCurrentPosition < mItemCount - 1 && mScrollBlock == false) {
            // mCurrentPosition = mCurrentPosition + 1;
            // mScrollTo = mCurrentPosition * mItemWidth;
            // this.smoothScrollTo(mScrollTo, 0);
            mScrollBlock = true;
            mMoveHandler.sendMessage(mMoveHandler
                    .obtainMessage(ScrollMoveHandler.MESSAGE_START,
                            ScrollMoveHandler.GO_RIGHT));
        }
    }

    public void moveToPosition(int position) {
        if (position > 0 && position <= mItemCount) {
            mCurrentPosition = position;
            mScrollTo = mCurrentPosition * mItemWidth;
            this.smoothScrollTo(mScrollTo, 0);
        }
    }

    public void moveToDefault() {
        mCurrentPosition = mDefaultPosition;
        mScrollTo = mCurrentPosition * mItemWidth;
        this.smoothScrollTo(mScrollTo, 0);
    }

    public void showDefault() {
        if (mDefaultPosition != mCurrentPosition) {
            mCurrentPosition = mDefaultPosition;
            mScrollTo = mCurrentPosition * mItemWidth;
            this.scrollTo(mScrollTo, 0);
        }
    }

    public void showLoadingUI() {
        mShowLoadingUI = true;
        mLoadingUIHandler
                .sendEmptyMessage(LoadingUIHandler.LOADING_ACTION_START);
    }

    public void hideLoadingUI() {
        mShowLoadingUI = false;
        mLoadingUIHandler
                .sendEmptyMessage(LoadingUIHandler.LOADING_ACTION_STOP);
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public void setOnCardScrollViewListener(onCardScrollViewListener listener) {
        mListener = listener;
    }

    private void updateUI() {
        this.invalidate();
    }

    private OnGenericMotionListener mMotionListener = new View.OnGenericMotionListener() {

        @Override
        public boolean onGenericMotion(View v, MotionEvent event) {
            return onGenericMotionEvent(event);
        }
    };

    private class LoadingUIHandler extends Handler {
        public static final int LOADING_ACTION_START = 0x01;
        public static final int LOADING_ACTION_UPDATE = 0x02;
        public static final int LOADING_ACTION_STOP = 0x03;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case LOADING_ACTION_START:
                mLoadThumPosition = 0;
                this.sendMessage(this
                        .obtainMessage(LOADING_ACTION_UPDATE, 0, 0));
                break;
            case LOADING_ACTION_UPDATE:
                if (msg.arg1 > 0) {
                    float maxY = mItemWidth / 3.0f;
                    float maxX = mItemWidth;

                    float delta = mLoadThumPosition * (maxY / maxX) * -1 + maxY;
                    mLoadThumPosition = mLoadThumPosition + delta;

                    if (mLoadThumPosition >= mItemWidth - LOADING_ACTION_UPDATE) {
                        mLoadThumPosition = 0;
                    }
                }
                updateUI();
                this.sendMessageDelayed(
                        this.obtainMessage(LOADING_ACTION_UPDATE, 1, 0), 100);
                break;
            case LOADING_ACTION_STOP:
                this.removeMessages(LOADING_ACTION_UPDATE);
                updateUI();
                break;
            }
        }
    }

    private class ScrollMoveHandler extends Handler {
        public static final int MESSAGE_START = 0x01;
        public static final int MESSAGE_UPDATE = 0x02;
        public static final int MESSAGE_STOP = 0x03;

        public static final int GO_LEFT = -1;
        public static final int GO_RIGHT = 1;

        private static final int DISPLAY_DEALY_TIME = 15;

        private static final float DISTANCE = 5.0f;
        private float mDelta = 0.0f;
        private int mDistance;
        private int mDirection;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_START:
                mDirection = (Integer) msg.obj;
                int delta = mCurrentPosition + mDirection;
                mDistance = delta * mItemWidth;
                mDelta = 0.0f;

                this.sendEmptyMessage(MESSAGE_UPDATE);
                break;
            case MESSAGE_UPDATE:
                int currentX = getScrollX();

                if (mDirection == GO_LEFT) {
                    mDelta = mDelta - DISTANCE;
                } else {
                    mDelta = mDelta + DISTANCE;
                }

                int move = (int) (currentX + mDelta);

                if (mDirection == GO_LEFT) {
                    if (move < mDistance) {
                        mScrollTo = mDistance;
                        mCurrentPosition = mCurrentPosition - 1;
                        smoothScrollTo(mDistance, 0);
                        this.sendEmptyMessage(MESSAGE_STOP);
                    } else {
                        smoothScrollTo(move, 0);
                        this.sendEmptyMessageDelayed(MESSAGE_UPDATE,
                                DISPLAY_DEALY_TIME);
                    }
                } else {
                    if (move > mDistance) {
                        mScrollTo = mDistance;
                        mCurrentPosition = mCurrentPosition + 1;
                        smoothScrollTo(mDistance, 0);
                        this.sendEmptyMessage(MESSAGE_STOP);
                    } else {
                        smoothScrollTo(move, 0);
                        this.sendEmptyMessageDelayed(MESSAGE_UPDATE,
                                DISPLAY_DEALY_TIME);
                    }
                }
                break;
            case MESSAGE_STOP:
                mScrollBlock = false;
                break;
            }

        }
    }

    public interface onCardScrollViewListener {
        abstract void onCardScrollView(View view, int position, int action);
    }
}