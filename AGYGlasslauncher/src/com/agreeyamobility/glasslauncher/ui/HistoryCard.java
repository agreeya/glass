package com.agreeyamobility.glasslauncher.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.format.DateUtils;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.Common.HistoryType;
import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.manager.LauncherManager;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.HistoryColumns;

public class HistoryCard extends Card {
	private static final String TAG = "HistoryCard";
	
    private ImageView mBackground;
    private ImageView mTypeIcon;
    private TextView mTimeStemp;
    private TextView mVoiceCommand;

    private Point mDisplaySize;

    private Animation mTimeAnimation;

    private long mTime = 0;
    private int mType;
    private String mText;

    private LauncherManager mManager;

    private View mMenuPlay;
    private View mMenuShare;
    private View mMenuDelete;

    private Camera.Parameters mParameters;

    public HistoryCard(Context context, CardScrollView base, int pos, int id) {
        super(context, base, pos, id);
        mCanVoice = false;
    }

    public void setCameraParameter(Camera.Parameters parameters) {
        mParameters = parameters;
    }

    public void setLauncherManager(LauncherManager manager) {
        mManager = manager;
    }

    @Override
    public void createMenu() {
        mMenuPlay = mInflater.inflate(R.layout.menu_layout, null, false);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                com.agreeyamobility.glasslauncher.Common.Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mMenuPlay.setLayoutParams(params);
        mMenuPlay.setId(R.id.play);
        ((ImageView)mMenuPlay.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_play);
        ((TextView)mMenuPlay.findViewById(R.id.menuTitle)).setText(R.string.menu_play);

        mMenuShare = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuShare.setLayoutParams(params);
        mMenuShare.setId(R.id.share);
        ((ImageView)mMenuShare.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_share);
        ((TextView)mMenuShare.findViewById(R.id.menuTitle)).setText(R.string.menu_share);

        mMenuDelete = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuDelete.setLayoutParams(params);
        mMenuDelete.setId(R.id.delete);
        ((ImageView)mMenuDelete.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_delete);
        ((TextView)mMenuDelete.findViewById(R.id.menuTitle)).setText(R.string.menu_delete);
    }

    @Override
    public void prepareMenu(CardScrollView menu) {
        if (mType == HistoryType.TYPE_VIDEO) {
            menu.addListItem(mMenuPlay, true);
        }

        if (mType == HistoryType.TYPE_IMAGE) {
            menu.addListItem(mMenuShare, true);
        } else {
            menu.addListItem(mMenuShare, false);
        }
        menu.addListItem(mMenuDelete, false);
    }

    @Override
    void onMotionEvent(int action, String event) {
    }

    @Override
    View createView(Context context, ViewGroup parent) {
        return mInflater.inflate(R.layout.card_content_history, null, false);
    }

    @Override
    void initialize(View view) {
        mBackground = (ImageView) view.findViewById(R.id.historyImageView);
        mTypeIcon = (ImageView) view.findViewById(R.id.historyType);
        mTimeStemp = (TextView) view.findViewById(R.id.historyTimeStemp);
        mVoiceCommand = (TextView) view.findViewById(R.id.timeCommand);

        Display display = ((WindowManager) getContext().getSystemService(
                Context.WINDOW_SERVICE)).getDefaultDisplay();
        mDisplaySize = new Point();
        display.getSize(mDisplaySize);

        mTimeAnimation = AnimationUtils.loadAnimation(getContext(),
                R.anim.down_to_up_slow);
    }

    @Override
    public void onSelectMenu(View view, int id) {
        switch (id) {
        case R.id.play:
            mManager.featurePlayVideo(mText);
            break;
        case R.id.delete:
            int dbId = getDataBaseId();
            getContext().getContentResolver().delete(
                    HistoryColumns.CONTENT_URI,
                    HistoryColumns.ID + " = " + dbId, null);
            mBaseLayout.moveToDefault();
            mBaseLayout.removeListItem(this);
            break;
        }
    }

    public void setData(int type, String text, boolean canMenu, boolean canVoice, Bitmap image) {
        initializeData(type, text, -1, canMenu, canVoice, image, null);
    }

    public void setData(int type, String text, boolean canMenu, boolean canVoice, byte[] image) {
        initializeData(type, text, -1, canMenu, canVoice, null, image);
    }

    public void setData(int type, String text, long time, boolean canMenu,
            boolean canVoice) {
        initializeData(type, text, time, canMenu, canVoice, null, null);
    }

    public void setTime(final long time, boolean animation) {
        mTime = time;

        if (animation == true) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayTime(time);
                    mTimeStemp.setVisibility(View.VISIBLE);
                    mTimeStemp.startAnimation(mTimeAnimation);
                }
            }, 500);
        } else {
            displayTime(time);
            mTimeStemp.setVisibility(View.VISIBLE);
        }
    }

    private void initializeData(int type, String text, long time,
            boolean canMenu, boolean canVoice, Bitmap image, byte[] data) {
        mCanMenu = canMenu;
        mCanVoice = false;

        mText = text;
        mType = type;

        showCommand();

        if (time != -1) {
            mTime = time;
            displayTime(time);
            mTimeStemp.setVisibility(View.VISIBLE);
        }

        if (image == null && data == null) {
            switch (type) {
            case HistoryType.TYPE_IMAGE:
                new LoadImageTask().executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, text);
                break;
            case HistoryType.TYPE_VIDEO:
                mTypeIcon.setImageResource(R.drawable.ic_record);
                new LoadThumbNailTask().executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, text);
                break;
            }
        } else if (data != null) {
            updateImage(data, true);
        } else if (image != null){
            updateImage(image, true);
        }
    }

    private void updateImage(byte[] image, boolean async) {
        if (async == true) {
            new SetBitmapImageTask(image).execute();
        }
    }


    private void updateImage(Bitmap image, boolean async) {
        if (async == true) {
            new SetBitmapImageTask(null).execute(image);
        } else {
            mBackground.setVisibility(View.VISIBLE);
            mBackground.setImageBitmap(image);
            mBackground.invalidate();
        }
    }

    @Override
    boolean isDefault() {
        return false;
    }

    private void showCommand() {
        if (mCanVoice == true) {
            mVoiceCommand.setVisibility(View.VISIBLE);
        }
    }

    private void displayTime(long time) {
        mTimeStemp.setText(DateUtils.getRelativeTimeSpanString(time,
                System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS));
    }

    private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Config.RGB_565;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(params[0], options);

            float widthScale = options.outWidth / mDisplaySize.x;
            float heightScale = options.outHeight / mDisplaySize.y;
            float scale = widthScale > heightScale ? widthScale : heightScale;

            if (scale >= 8) {
                options.inSampleSize = 8;
            } else if (scale >= 6) {
                options.inSampleSize = 6;
            } else if (scale >= 4) {
                options.inSampleSize = 4;
            } else if (scale >= 2) {
                options.inSampleSize = 2;
            } else if (scale >= 1) {
                options.inSampleSize = 1;
            }
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(params[0], options);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null) {
                updateImage(result, false);
            }
        }
    }

    private class SetBitmapImageTask extends AsyncTask<Bitmap, Void, Bitmap> {
        private byte[] mData;

        public SetBitmapImageTask(byte[] data) {
            mData = data;
        }

        @Override
        protected Bitmap doInBackground(Bitmap... params) {
            Bitmap image = null;
            if (mData != null && mParameters != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Config.RGB_565;
                options.inJustDecodeBounds = true;
                options.outWidth = mParameters.getPictureSize().width;
                options.outHeight = mParameters.getPictureSize().height;

                float widthScale = options.outWidth / mDisplaySize.x;
                float heightScale = options.outHeight / mDisplaySize.y;
                float scale = widthScale > heightScale ? widthScale : heightScale;

                if (scale >= 8) {
                    options.inSampleSize = 8;
                } else if (scale >= 6) {
                    options.inSampleSize = 6;
                } else if (scale >= 4) {
                    options.inSampleSize = 4;
                } else if (scale >= 2) {
                    options.inSampleSize = 2;
                } else if (scale >= 1) {
                    options.inSampleSize = 1;
                }
                options.inJustDecodeBounds = false;
                android.util.Log.i("TEST", "options.scale :: " + options.inSampleSize);
                image = BitmapFactory.decodeByteArray(mData, 0, mData.length, options);
                mData = null;
            } else {
                image = Bitmap.createScaledBitmap(params[0], 320, 240, true);
            }
            return image;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                mBackground.setVisibility(View.VISIBLE);
                mBackground.setImageBitmap(result);
                mBackground.invalidate();
            }
        }
    }

    private class LoadThumbNailTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            return ThumbnailUtils.createVideoThumbnail(params[0],
                    MediaStore.Images.Thumbnails.MINI_KIND);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null) {
                mTypeIcon.setVisibility(View.VISIBLE);
                updateImage(result, false);
            }
        }
    }

    @Override
    void updateUi() {
        if (mTime != 0) {
            displayTime(mTime);
        }
    }
}