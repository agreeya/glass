package com.agreeyamobility.glasslauncher.ui;

import com.agreeyamobility.glasslauncher.Common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

public class LiveCardFrameView extends FrameLayout {
    private static final String TAG = "LiveCardFrameView";
    
    private static final boolean SUPPORT_MOTION_EVENT = true;

    public static final int VIEW_ACTION_CLICK = 0x01;
    public static final int VIEW_ACTION_UP    = 0x02;
    public static final int VIEW_ACTION_DOWN  = 0x03;
    public static final int VIEW_ACTION_LEFT  = 0x04;
    public static final int VIEW_ACTION_RIGHT = 0x05;

    private FrameViewListener mListener;
    private long mOldTime = 0;

    public LiveCardFrameView(Context context) {
        this(context, null);
    }

    public LiveCardFrameView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public LiveCardFrameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        initialize();
    }

    public void setFrameViewListener(FrameViewListener listener) {
        mListener = listener;
    }

    private void initialize() {
        this.setClickable(true);

        this.setOnTouchListener(mOnTouchListener);

        if (SUPPORT_MOTION_EVENT) {
            this.setFocusable(true);
            this.setFocusableInTouchMode(true);
            this.setOnGenericMotionListener(mMotionListener);
        }
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        int source = event.getSource();

        if (source == InputDevice.SOURCE_MOUSE) {
            int action = event.getActionMasked();

            switch (action) {
            case MotionEvent.ACTION_SCROLL:
                if (event.getAxisValue(MotionEvent.AXIS_VSCROLL) > 0.0f) {
                    Log.i(TAG, "Drag up");
                    if (mListener != null) {
                        mListener.onMotion(VIEW_ACTION_UP);
                    }
                } else if (event.getAxisValue(MotionEvent.AXIS_VSCROLL) < 0.0f) {
                    Log.i(TAG, "Drag down");
                    if (mListener != null) {
                        mListener.onMotion(VIEW_ACTION_DOWN);
                    }
                } else if (event.getAxisValue(MotionEvent.AXIS_HSCROLL) < 0.0f) {
                    Log.i(TAG, "Drag left");
                    if (mListener != null) {
                        mListener.onMotion(VIEW_ACTION_LEFT);
                    }
                } else if (event.getAxisValue(MotionEvent.AXIS_HSCROLL) > 0.0f) {
                    Log.i(TAG, "Drag right");
                    if (mListener != null) {
                        mListener.onMotion(VIEW_ACTION_RIGHT);
                    }
                }
                return true;
            case MotionEvent.ACTION_HOVER_MOVE:
//                boolean canNext = false;
//                if (mOldTime == 0) {
//                    return false;
//                } else {
//                    long systemTime = System.currentTimeMillis();
//                    if (Math.abs(mOldTime - systemTime) > Common.CLICK_CHECK_INTERVAL_TIME) {
//                        canNext = true;
//                        mOldTime = systemTime;
//                    }
//                }

                if (mListener != null) {
                    mListener.onMotion(VIEW_ACTION_CLICK);
                }
                return true;
            }
        }
        return false;
    }

    public void updateTime(long time) {
        mOldTime = time;
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        private static final float OFF_SET = 40.0f;

        private float mOldX;
        private float mOldY;
        private boolean mInSide;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction() & MotionEvent.ACTION_MASK;
            switch (action) {
            case MotionEvent.ACTION_DOWN:
                mOldX = event.getX();
                mOldY = event.getY();
                mInSide = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mInSide == true) {
                    if (Math.abs(event.getX() - mOldX) > OFF_SET
                            && Math.abs(event.getY() - mOldY) > OFF_SET) {
                        mInSide = false;
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                float deltaX = event.getX() - mOldX;
                float deltaY = event.getY() - mOldY;
                float absX = Math.abs(deltaX);
                float absY = Math.abs(deltaY);

                if (absX < OFF_SET && absY < OFF_SET) {
                    // click
                    if (mListener != null) {
                        mListener.onMotion(VIEW_ACTION_CLICK);
                    }
                } else if (absX < OFF_SET && absY > OFF_SET) {
                    // drag up / down
                    if (mInSide == true) {
                        if (deltaY < 0) { // up
                            if (mListener != null) {
                                mListener.onMotion(VIEW_ACTION_UP);
                            }        
                        } else if (deltaY > 0) { // down
                            if (mListener != null) {
                                mListener.onMotion(VIEW_ACTION_DOWN);
                            }
                        }
                    }
                } else if (absX > OFF_SET && absY < OFF_SET) {
                    // drag left / right
                    if (mInSide == true) {
                        if (deltaX < 0) { // left 
                            if (mListener != null) {
                                mListener.onMotion(VIEW_ACTION_LEFT);
                            }
                        } else if (deltaX > 0) { // right
                            if (mListener != null) {
                                mListener.onMotion(VIEW_ACTION_RIGHT);
                            }
                        }
                    }
                }
                break;
            }
            return false;
        }
    };

    private OnGenericMotionListener mMotionListener = new View.OnGenericMotionListener() {

        @Override
        public boolean onGenericMotion(View v, MotionEvent event) {
            return onGenericMotionEvent(event);
        }
    };

    public interface FrameViewListener {
        abstract void onMotion(int action);
    }
}