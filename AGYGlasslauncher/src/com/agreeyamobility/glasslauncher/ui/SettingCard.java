package com.agreeyamobility.glasslauncher.ui;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.BatteryManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.R;


public class SettingCard extends Card {
	private static final String TAG = "SettingCard";
    private static final int VOLUME_LEVEL = 1;
    private SettingInfoLoader mInfoLoader;

    private TextView mWifiDataView;
    private TextView mBatteryInfoView;
    private ImageView mBatteryIcon;
    private ImageView mWifiIcon;

    private View mMenuWifiView;
    private View mMenuBTView;
    private View mMenuDeviceView;
    private View mMenuVolumView;

    private View mSubMenuWiFiOnView;
    private View mSubMenuJoinView;
    private View mSubMenuSwitchView;
    private View mSubMenuForgotView;
    private View mSubMenuBTOnView;
    private View mSubMenuVolumeView;

    private LinearLayout.LayoutParams mMenuParams;

    private int[] mWiFiIconList = new int[] { R.drawable.ic_wifi_1,
            R.drawable.ic_wifi_2, R.drawable.ic_wifi_3, R.drawable.ic_wifi_4,
            R.drawable.ic_wifi_5 };

    private int[] mBatteryChargeList = new int[] {
            R.drawable.ic_charge_bettery_1, R.drawable.ic_charge_bettery_2,
            R.drawable.ic_charge_bettery_3, R.drawable.ic_charge_bettery_4,
            R.drawable.ic_charge_bettery_5 };

    private int[] mBatteryList = new int[] { R.drawable.ic_bettery_1,
            R.drawable.ic_bettery_2, R.drawable.ic_bettery_3,
            R.drawable.ic_bettery_4, R.drawable.ic_bettery_5 };

    private AudioManager mAudioManager;

    public SettingCard(Context context, CardScrollView base) {
        super(context, base, -1, -1);
    }

    @Override
    View createView(Context context, ViewGroup parent) {
        return mInflater.inflate(R.layout.card_content_setting, parent, false);
    }

    @Override
    void initialize(View view) {
        mBatteryInfoView = (TextView) view.findViewById(R.id.text1);
        mWifiDataView = (TextView) view.findViewById(R.id.text2);
        mBatteryIcon = (ImageView) view.findViewById(R.id.battery);
        mWifiIcon = (ImageView) view.findViewById(R.id.wifi);

        mAudioManager = (AudioManager) getContext().getSystemService(
                Context.AUDIO_SERVICE);

        mCanMenu = true;
        mInfoLoader = new SettingInfoLoader();
        mInfoLoader.registerBroadcastReceiver();
        mSupportSubMenu = true;
    }

    @Override
    public void onSelectMenu(View view, int id) {

        switch (id) {
        case R.id.wifi:
        case R.id.bluetooth:
        case R.id.volum:
            mLauncherManager.showSubMenu(id);
            break;
        }
    }

    @Override
    public void onSelectSubMenu(View view, int id, int action) {
        switch (id) {
        case R.id.wifi_join:
        case R.id.wifi_switch:
            mLauncherManager.featureConnectWiFi();
            break;
        case R.id.wifi_forgot:
            mInfoLoader.mWifiManager.disconnect();
            break;
        case R.id.volum_control:
            int max = mAudioManager
                    .getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
            int current = mAudioManager
                    .getStreamVolume(AudioManager.STREAM_SYSTEM);
            switch (action) {
            case CardScrollView.VIEW_ACTION_TO_LEFT:
                if (current > 0) {
                    current = current - VOLUME_LEVEL;
                }
                if (current < 0) {
                    current = 0;
                }
                break;
            case CardScrollView.VIEW_ACTION_TO_RIGHT:
                if (current < max) {
                    current = current + VOLUME_LEVEL;
                }
                if (current > max) {
                    current = max;
                }
                break;
            }

            ProgressBar progress = (ProgressBar) mSubMenuVolumeView
                    .findViewById(R.id.volum_progress);
            if (current == -1) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0,
                        AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                progress.setProgress(0);
            } else {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM,
                        current, AudioManager.FLAG_PLAY_SOUND);
                progress.setProgress(current);
            }

            int volumIcond = R.drawable.ic_volum_3;
            int vTextColorId = R.color.setting_color_green;

            String vText = null;

            int vValue = (int) ((float) current / (float) max * 100.0f);
            vText = String.format("%d", vValue) + " %";

            if (vValue == 0) {
                vTextColorId = R.color.setting_color_red;
                volumIcond = R.drawable.ic_volum_1;
                vText = getContext().getResources().getString(
                        R.string.setting_volum_mute);
            } else if (vValue > 0 && vValue <= 50) {
                vTextColorId = R.color.setting_color_yellogreen;
                volumIcond = R.drawable.ic_volum_2;
            }

            ((ImageView) mSubMenuVolumeView.findViewById(R.id.volum_icon))
                    .setImageResource(volumIcond);

            ((ImageView) mMenuVolumView.findViewById(R.id.icon))
                    .setImageResource(volumIcond);

            TextView vText1 = (TextView) mMenuVolumView
                    .findViewById(R.id.text1);
            vText1.setTextColor(getContext().getResources().getColor(
                    vTextColorId));
            vText1.setText(vText);
            break;
        case R.id.toggle_bt:
            boolean enable = mInfoLoader.mBluetoothAdapter.isEnabled();
            if (enable == true) {
                mInfoLoader.mBluetoothAdapter.disable();
            } else {
                mInfoLoader.mBluetoothAdapter.enable();
            }
            break;
        case R.id.toggle_wifi:
            if (mInfoLoader.mWifiManager.isWifiEnabled() == false) {
                mInfoLoader.mWifiManager.setWifiEnabled(true);
            }
            break;
        }
    }

    @Override
    boolean isDefault() {
        return false;
    }

    @Override
    public void createMenu() {
        mMenuParams = new LinearLayout.LayoutParams(
                com.agreeyamobility.glasslauncher.Common.Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);

        mMenuWifiView = mInflater.inflate(R.layout.menu_setting, null, false);
        mMenuWifiView.setLayoutParams(mMenuParams);
        mMenuWifiView.setId(R.id.wifi);
        ((TextView) mMenuWifiView.findViewById(R.id.title))
                .setText(R.string.menu_title_wifi);
        ((ImageView) mMenuWifiView.findViewById(R.id.icon))
                .setImageResource(R.drawable.ic_wifi_menu_enable);

        int textColor = R.color.setting_color_green;
        int textId = R.string.setting_wifi_data;

        if (mInfoLoader.mWifiManager.isWifiEnabled()) {
            WifiInfo info = mInfoLoader.mWifiManager.getConnectionInfo();
            if (info.getLinkSpeed() < 0) {
                textId = R.string.setting_wifi_disconnected;
                textColor = R.color.setting_color_red;
                ((ImageView) mMenuWifiView.findViewById(R.id.icon))
                        .setImageResource(R.drawable.ic_wifi_menu_disable);
            } else {
            	textId = R.string.setting_wifi_connected;
                ((TextView) mMenuWifiView.findViewById(R.id.title))
                .setText(info.getSSID().replace("\"", ""));
            }
            
            ((TextView) mMenuWifiView.findViewById(R.id.text3))
            .setText(R.string.setting_wifi_available);
        } else {
            textId = R.string.setting_wifi_off;
            textColor = R.color.setting_color_red;
        }

        TextView text1 = (TextView) mMenuWifiView.findViewById(R.id.text1);
        text1.setTextColor(getContext().getResources().getColor(textColor));
        text1.setText(getContext().getResources().getString(textId));

        mMenuBTView = mInflater.inflate(R.layout.menu_setting, null, false);
        mMenuBTView.setLayoutParams(mMenuParams);
        mMenuBTView.setId(R.id.bluetooth);
        ((TextView) mMenuBTView.findViewById(R.id.title))
                .setText(R.string.menu_bluetooth);
        ((ImageView) mMenuBTView.findViewById(R.id.icon))
                .setImageResource(R.drawable.ic_bluetooth_enable);

        if (mInfoLoader.mBluetoothAdapter.isEnabled() == false) {
            TextView btText1 = (TextView) mMenuBTView.findViewById(R.id.text1);
            btText1.setTextColor(getContext().getResources().getColor(
                    R.color.setting_color_red));
            btText1.setText(R.string.setting_bluetooth_off);
            ((ImageView) mMenuBTView.findViewById(R.id.icon))
                    .setImageResource(R.drawable.ic_bluetooth_disable);
        } else {
            TextView btText1 = (TextView) mMenuBTView.findViewById(R.id.text3);
            btText1.setText(R.string.setting_bluetooth_discorver);
        }

        mMenuDeviceView = mInflater.inflate(R.layout.menu_setting, null, false);
        mMenuDeviceView.setLayoutParams(mMenuParams);
        mMenuDeviceView.setId(R.id.device);
        ((TextView) mMenuDeviceView.findViewById(R.id.title))
                .setText(R.string.menu_device_info);

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
                .getPath());
        String unit = null;
        double bytesAvailable = (double) stat.getBlockSize()
                * (double) stat.getAvailableBlocks();
        double megAvailable = bytesAvailable / (1024 * 1024);
        unit = "MB";
        if (megAvailable > 1024) {
            megAvailable = megAvailable / (double) 1024;
            unit = "GB";
        }

        TextView deviceText1 = (TextView) mMenuDeviceView
                .findViewById(R.id.text1);
        deviceText1.setTextColor(getContext().getResources().getColor(
                R.color.setting_color_green));
        deviceText1.setText(String.format("%.1f " + unit + " free",
                megAvailable));

        ((TextView) mMenuDeviceView.findViewById(R.id.text3))
                .setText(getContext().getResources().getString(
                        R.string.setting_device_serial, "Demo version"));
        ((ImageView) mMenuDeviceView.findViewById(R.id.icon))
                .setImageResource(R.drawable.ic_device);

        mMenuVolumView = mInflater.inflate(R.layout.menu_setting, null, false);
        mMenuVolumView.setLayoutParams(mMenuParams);
        mMenuVolumView.setId(R.id.volum);
        ((TextView) mMenuVolumView.findViewById(R.id.title))
                .setText(R.string.menu_volum);

        mSubMenuJoinView = mInflater.inflate(R.layout.menu_layout, null, false);
        mSubMenuJoinView.setLayoutParams(mMenuParams);
        mSubMenuJoinView.setId(R.id.wifi_join);
        ((ImageView) mSubMenuJoinView.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_wifi_menu_enable);
        ((TextView) mSubMenuJoinView.findViewById(R.id.menuTitle))
                .setText(R.string.menu_join_network);

        mSubMenuSwitchView = mInflater.inflate(R.layout.menu_layout, null,
                false);
        mSubMenuSwitchView.setLayoutParams(mMenuParams);
        mSubMenuSwitchView.setId(R.id.wifi_switch);
        ((ImageView) mSubMenuSwitchView.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_wifi_menu_enable);
        ((TextView) mSubMenuSwitchView.findViewById(R.id.menuTitle))
                .setText(R.string.menu_switch_network);

        mSubMenuForgotView = mInflater.inflate(R.layout.menu_layout, null,
                false);
        mSubMenuForgotView.setLayoutParams(mMenuParams);
        mSubMenuForgotView.setId(R.id.wifi_forgot);
        ((ImageView) mSubMenuForgotView.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_delete);
        ((TextView) mSubMenuForgotView.findViewById(R.id.menuTitle))
                .setText(R.string.menu_forgot_network);

        mSubMenuVolumeView = mInflater.inflate(R.layout.sub_menu_volum, null,
                false);
        mSubMenuVolumeView.setLayoutParams(mMenuParams);
        mSubMenuVolumeView.setId(R.id.volum_control);

        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);

        int volumIcond = R.drawable.ic_volum_3;
        int vTextColorId = R.color.setting_color_green;

        String vText = null;

        int vValue = (int) ((float) current / (float) max * 100.0f);
        vText = String.format("%d", vValue) + " %";

        if (current == 0) {
            vTextColorId = R.color.setting_color_red;
            volumIcond = R.drawable.ic_volum_1;
            vText = getContext().getResources().getString(
                    R.string.setting_volum_mute);
        } else if (current > 0 && current <= max / 2) {
            volumIcond = R.drawable.ic_volum_2;
        }

        ((ImageView) mSubMenuVolumeView.findViewById(R.id.volum_icon))
                .setImageResource(volumIcond);

        ((ImageView) mMenuVolumView.findViewById(R.id.icon))
                .setImageResource(volumIcond);

        TextView vText1 = (TextView) mMenuVolumView.findViewById(R.id.text1);
        vText1.setTextColor(getContext().getResources().getColor(vTextColorId));
        vText1.setText(vText);

        ProgressBar progress = (ProgressBar) mSubMenuVolumeView
                .findViewById(R.id.volum_progress);
        progress.setMax(max);
        progress.setProgress(current);

        mSubMenuWiFiOnView = mInflater.inflate(R.layout.menu_layout, null,
                false);
        mSubMenuWiFiOnView.setId(R.id.toggle_wifi);
        ((ImageView) mSubMenuWiFiOnView.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_wifi_menu_enable);
        ((TextView) mSubMenuWiFiOnView.findViewById(R.id.menuTitle))
                .setText(R.string.setting_turn_on);

        mSubMenuBTOnView = mInflater.inflate(R.layout.menu_layout, null, false);
        mSubMenuBTOnView.setId(R.id.toggle_bt);
        ((ImageView) mSubMenuBTOnView.findViewById(R.id.menuIcon))
                .setImageResource(R.drawable.ic_bluetooth_disable);
        ((TextView) mSubMenuBTOnView.findViewById(R.id.menuTitle))
                .setText(mInfoLoader.mBluetoothAdapter.isEnabled() == true ? R.string.setting_turn_off
                        : R.string.setting_turn_on);
    }

    @Override
    public void prepareMenu(CardScrollView menu) {
        int textColor = R.color.setting_color_green;
        int textId = R.string.setting_wifi_data;

        ((TextView) mMenuWifiView.findViewById(R.id.title))
        .setText(R.string.menu_title_wifi);
        
        if (mInfoLoader.mWifiManager.isWifiEnabled()) {
            WifiInfo info = mInfoLoader.mWifiManager.getConnectionInfo();
            if (info.getLinkSpeed() < 0) {
                textId = R.string.setting_wifi_disconnected;
                textColor = R.color.setting_color_red;
                ((ImageView) mMenuWifiView.findViewById(R.id.icon))
                        .setImageResource(R.drawable.ic_wifi_menu_disable);
            } else {
            	textId = R.string.setting_wifi_connected;
                ((TextView) mMenuWifiView.findViewById(R.id.title))
                .setText(info.getSSID().replace("\"", ""));
            }
            ((TextView) mMenuWifiView.findViewById(R.id.text3))
            .setText(R.string.setting_wifi_available);
        } else {
            textId = R.string.setting_wifi_off;
            textColor = R.color.setting_color_red;
        }

        TextView text1 = (TextView) mMenuWifiView.findViewById(R.id.text1);
        text1.setTextColor(getContext().getResources().getColor(textColor));
        text1.setText(getContext().getResources().getString(textId));

        menu.addListItem(mMenuWifiView, true);
        menu.addListItem(mMenuBTView, false);
        menu.addListItem(mMenuDeviceView, false);
        menu.addListItem(mMenuVolumView, false);
    }

    @Override
    public void prepareSubMenu(int parentId, CardScrollView menu) {
        switch (parentId) {
        case R.id.wifi:
            if (mInfoLoader.mWifiManager.isWifiEnabled() == false) {
                menu.addListItem(mSubMenuWiFiOnView, true);
            } else {
                WifiInfo info = mInfoLoader.mWifiManager.getConnectionInfo();
                if (info.getLinkSpeed() <= 0) {
                    menu.addListItem(mSubMenuJoinView, true);
                } else {
                    menu.addListItem(mSubMenuSwitchView, true);
                    menu.addListItem(mSubMenuForgotView, false);
                }
            }
            break;
        case R.id.bluetooth:
            ((TextView) mSubMenuBTOnView.findViewById(R.id.menuTitle))
                    .setText(mInfoLoader.mBluetoothAdapter.isEnabled() == true ? R.string.setting_turn_off
                            : R.string.setting_turn_on);
            menu.addListItem(mSubMenuBTOnView, true);
            break;
        case R.id.volum:
            menu.addListItem(mSubMenuVolumeView, true);
            break;
        }
    }

    @Override
    void onMotionEvent(int action, String event) {
    }

    private void onSettingResult(String info) {

    }

    private void onSettingBatteryInfo(int info, boolean connect) {
        String batteryInfo = getContext().getResources().getString(
                R.string.setting_battery_charge);
        String displayInfo = String.format("%d", info) + batteryInfo;

        int textColor = getContext().getResources().getColor(
                R.color.setting_color_green);

        int index = 0;
        if (info == 100) {
            index = 4;
        } else if (info > 70) {
            index = 3;
        } else if (info > 40) {
            index = 2;
            textColor = getContext().getResources().getColor(
                    R.color.setting_color_yellogreen);
        } else if (info >= 0) {
            index = 1;
            textColor = getContext().getResources().getColor(
                    R.color.setting_color_red);
        }

        int[] iconList;

        if (connect == false) {
            displayInfo = String.format("%d", info) +
                    getContext().getResources().getString(
                    R.string.setting_battery_disconnect);
            if (info == 100) {
                displayInfo = getContext().getResources().getString(
                        R.string.setting_battery_full);
            }
            iconList = mBatteryList;
        } else {
            if (info == 100) {
                displayInfo = getContext().getResources().getString(
                        R.string.setting_battery_full);
            }
            iconList = mBatteryChargeList;
        }

        mBatteryIcon.setImageResource(iconList[index]);
        mBatteryInfoView.setTextColor(textColor);
        mBatteryInfoView.setText(displayInfo);
    }

    private void onSettingWifi(int speed, boolean status) {
        String wifiState = getContext().getResources().getString(
                R.string.setting_wifi_data);
        int textColor = getContext().getResources().getColor(
                R.color.setting_color_green);

        if (status == false) {
            wifiState = getContext().getResources().getString(
                    R.string.setting_wifi_off);
            textColor = getContext().getResources().getColor(
                    R.color.setting_color_red);
        }

        int index = 0;
        if (speed >= 50) {
            index = 4;
        } else if (speed >= 40) {
            index = 3;
        } else if (speed >= 20) {
            index = 2;
        } else if (speed > 0) {
            index = 1;
        } else if (speed <= 0) {
            index = 0;
            wifiState = getContext().getResources().getString(
                    R.string.setting_wifi_no_network);
            textColor = getContext().getResources().getColor(
                    R.color.setting_color_red);
        }

        mWifiIcon.setImageResource(mWiFiIconList[index]);
        mWifiDataView.setTextColor(textColor);
        mWifiDataView.setText(wifiState);
        wifiMenuUpdate();
    }

    private void wifiMenuUpdate() {
        int subTextColor = R.color.setting_color_green;
        int textId = R.string.setting_wifi_data;

        ((TextView) mMenuWifiView.findViewById(R.id.title))
        .setText(R.string.menu_title_wifi);
        
        ((TextView) mMenuWifiView.findViewById(R.id.text3)).setText("");
        ((ImageView) mMenuWifiView.findViewById(R.id.icon))
                .setImageResource(R.drawable.ic_wifi_menu_enable);

        if (mInfoLoader.mWifiManager.isWifiEnabled()) {
            WifiInfo info = mInfoLoader.mWifiManager.getConnectionInfo();
            if (info.getLinkSpeed() < 0) {
                textId = R.string.setting_wifi_disconnected;
                subTextColor = R.color.setting_color_red;
                ((ImageView) mMenuWifiView.findViewById(R.id.icon))
                        .setImageResource(R.drawable.ic_wifi_menu_disable);
            } else {
            	textId = R.string.setting_wifi_connected;
                ((TextView) mMenuWifiView.findViewById(R.id.title))
                .setText(info.getSSID().replace("\"", ""));
            }
            ((TextView) mMenuWifiView.findViewById(R.id.text3))
            .setText(R.string.setting_wifi_available);
        }

        TextView text1 = (TextView) mMenuWifiView.findViewById(R.id.text1);
        text1.setTextColor(getContext().getResources().getColor(subTextColor));
        text1.setText(getContext().getResources().getString(textId));
    }

    public class SettingInfoLoader {
        private static final int BATTERY_MIN = 0;
        private static final int BATTERY_MAX = 100;

        // Return the event to parent
        private final UpdateResultHandler mResultHandler;
        private final WifiManager mWifiManager;
        private final BluetoothAdapter mBluetoothAdapter;
        private final BroadcastReceiver mSettingsReceiver;
        private int mBatteryInfo;
        private boolean mChargeStatus;
        private boolean mWifiStatus;

        public SettingInfoLoader() {
            mResultHandler = new UpdateResultHandler();
            mSettingsReceiver = new SettingsReceiver();

            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            // check WiFi enable
            mWifiManager = (WifiManager) getContext().getSystemService(
                    Context.WIFI_SERVICE);
            initSettings();
        }

        private void initSettings() {
            mBatteryInfo = 0;
            mWifiStatus = checkWIFI();
        }

        private boolean checkWIFI() {
            if (mResultHandler != null) {
                mResultHandler.sendMessage(mResultHandler
                        .obtainMessage(UpdateResultHandler.MESSAGE_WIFI_STATE));
            }
            return mWifiManager.isWifiEnabled();
        }

        public void registerBroadcastReceiver() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
            intentFilter
                    .addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
            intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
            intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

            Intent batteryIntent = getContext().registerReceiver(
                    mSettingsReceiver, intentFilter);

            int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL,
                    BATTERY_MIN);
            int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE,
                    BATTERY_MAX);
            mBatteryInfo = (level * BATTERY_MAX) / scale;
            if (mResultHandler != null) {
                mResultHandler.sendMessage(mResultHandler
                        .obtainMessage(UpdateResultHandler.MESSAGE_COMPLETE));
            }

            int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS,
                    -1);
            boolean check = status == BatteryManager.BATTERY_STATUS_NOT_CHARGING
                    || status == BatteryManager.BATTERY_STATUS_DISCHARGING;
            mChargeStatus = !check;

            if (mResultHandler != null) {
                mResultHandler
                        .sendMessage(mResultHandler
                                .obtainMessage(UpdateResultHandler.MESSAGE_BATTERY_INFO));
            }
        }

        public void unRegisterReceiver() {
            getContext().unregisterReceiver(mSettingsReceiver);
        }

        private class UpdateResultHandler extends Handler {
            public static final int MESSAGE_COMPLETE = 0x01;
            public static final int MESSAGE_ERROR = 0x02;
            public static final int MESSAGE_BATTERY_INFO = 0x03;
            public static final int MESSAGE_WIFI_STATE = 0x05;

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                case MESSAGE_COMPLETE:
                    onSettingResult((String) msg.obj);
                    break;
                case MESSAGE_ERROR:
                    break;
                case MESSAGE_BATTERY_INFO:
                    onSettingBatteryInfo(mBatteryInfo, mChargeStatus);
                    break;
                case MESSAGE_WIFI_STATE:
                    WifiInfo info = mWifiManager.getConnectionInfo();
                    onSettingWifi(info.getLinkSpeed(), mWifiStatus);
                    break;
                }
            }
        }

        public class SettingsReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
                    int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,
                            -1);
                    int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE,
                            -1);
                    if (level >= 0 && scale >= 0) {
                        mBatteryInfo = (level * 100) / scale;
                        if (mResultHandler != null) {
                            mResultHandler
                                    .sendMessage(mResultHandler
                                            .obtainMessage(UpdateResultHandler.MESSAGE_BATTERY_INFO));
                        }
                    }
                } else if (Intent.ACTION_POWER_CONNECTED.equals(action)) {
                    mChargeStatus = true;
                    if (mResultHandler != null) {
                        mResultHandler
                                .sendMessage(mResultHandler
                                        .obtainMessage(UpdateResultHandler.MESSAGE_BATTERY_INFO));
                    }
                } else if (Intent.ACTION_POWER_DISCONNECTED.equals(action)) {
                    mChargeStatus = false;
                    if (mResultHandler != null) {
                        mResultHandler
                                .sendMessage(mResultHandler
                                        .obtainMessage(UpdateResultHandler.MESSAGE_BATTERY_INFO));
                    }
                }

                // WiFi check
                if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                    int state = intent.getIntExtra(
                            WifiP2pManager.EXTRA_WIFI_STATE, -1);
                    if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                        mWifiStatus = true;
                    } else {
                        mWifiStatus = false;
                    }

                    if (mResultHandler != null) {
                        mResultHandler
                                .sendMessage(mResultHandler
                                        .obtainMessage(UpdateResultHandler.MESSAGE_WIFI_STATE));
                    }
                } else if (ConnectivityManager.CONNECTIVITY_ACTION
                        .equals(action)) {
                    if (mResultHandler != null) {
                        mResultHandler
                                .sendMessage(mResultHandler
                                        .obtainMessage(UpdateResultHandler.MESSAGE_WIFI_STATE));
                    }
                }

                if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    if (mInfoLoader != null && mInfoLoader.mBluetoothAdapter != null) {
                        ((TextView) mSubMenuBTOnView.findViewById(R.id.menuTitle))
                                .setText(mInfoLoader.mBluetoothAdapter.isEnabled() == true ? R.string.setting_turn_off
                                        : R.string.setting_turn_on);

                        if (mInfoLoader.mBluetoothAdapter.isEnabled() == false) {
                            TextView btText1 = (TextView) mMenuBTView
                                    .findViewById(R.id.text1);
                            btText1.setTextColor(getContext().getResources().getColor(
                                    R.color.setting_color_red));
                            btText1.setText(R.string.setting_bluetooth_off);
                            TextView btText3 = (TextView) mMenuBTView
                                    .findViewById(R.id.text3);
                            btText3.setText("");
                            ((ImageView) mMenuBTView.findViewById(R.id.icon))
                                    .setImageResource(R.drawable.ic_bluetooth_disable);
                        } else {
                            TextView btText1 = (TextView) mMenuBTView
                                    .findViewById(R.id.text1);
                            btText1.setText("");
                            TextView btText3 = (TextView) mMenuBTView
                                    .findViewById(R.id.text3);
                            btText3.setText(R.string.setting_bluetooth_discorver);
                            ((ImageView) mMenuBTView.findViewById(R.id.icon))
                            .setImageResource(R.drawable.ic_bluetooth_enable);
                        }
                    }
                }
            }
        }
    }

    @Override
    void updateUi() {
    }
}