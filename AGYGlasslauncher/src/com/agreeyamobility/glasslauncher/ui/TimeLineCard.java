package com.agreeyamobility.glasslauncher.ui;

import java.util.Calendar;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.R;


public class TimeLineCard extends Card {
	private static final String TAG = "TimeLineCard";
	
    private View mTimeLineView;

    private boolean mSelectMenu;

    private View mMenuTakeAPicture;
    private View mMenuRecodAVideo;
    private View mMenuBrowser;

    public TimeLineCard(Context context, CardScrollView base) {
        super(context, base, -1, -1);
    }

    @Override
    View createView(Context context, ViewGroup parent) {
        return mInflater.inflate(R.layout.card_content_timeline, parent, false);
    }

    @Override
    void initialize(View view) {
        mTimeLineView = view;
        mCanMenu = true;
        mCanVoice = true;
    }

    @Override
    boolean isDefault() {
        return true;
    }

    @Override
    public void createMenu() {
        mMenuTakeAPicture = mInflater.inflate(R.layout.menu_layout, null, false);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                com.agreeyamobility.glasslauncher.Common.Display.DEVICE_RESOLUTION_WIDTH,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mMenuTakeAPicture.setLayoutParams(params);
        mMenuTakeAPicture.setId(R.id.takePicture);
        ((TextView)mMenuTakeAPicture.findViewById(R.id.menuTitle)).setText(R.string.take_picture);
        ((ImageView)mMenuTakeAPicture.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_camera);

        mMenuRecodAVideo = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuRecodAVideo.setLayoutParams(params);
        mMenuRecodAVideo.setId(R.id.recordVideo);
        ((TextView)mMenuRecodAVideo.findViewById(R.id.menuTitle)).setText(R.string.record_video);
        ((ImageView)mMenuRecodAVideo.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_record);

        mMenuBrowser = mInflater.inflate(R.layout.menu_layout, null, false);
        mMenuBrowser.setLayoutParams(params);
        mMenuBrowser.setId(R.id.browser);
        ((TextView)mMenuBrowser.findViewById(R.id.menuTitle)).setText(R.string.browser);
        ((ImageView)mMenuBrowser.findViewById(R.id.menuIcon)).setImageResource(R.drawable.ic_browser);
    }

    @Override
    public void prepareMenu(CardScrollView menu) {
        // menu item "take a picture"
        menu.addListItem(mMenuTakeAPicture, true);
        // menu item "record a video"
        menu.addListItem(mMenuRecodAVideo, false);

        WifiInfo info = ((WifiManager)getContext()
                .getSystemService(Context.WIFI_SERVICE)).getConnectionInfo();
        if (info.getLinkSpeed() > 0) {
            menu.addListItem(mMenuBrowser, false);
        }
        mSelectMenu = false;
    }

    @Override
    public void onBeforeShowMenu() {
        if (mLauncherManager != null) {
        	Log.i(TAG, "onBeforeShowMenu");
        	mLauncherManager.stopVoiceCommand();
        }
    }

    @Override
    public void onSelectMenu(View view, int id) {
        mSelectMenu = true;

        switch (id) {
        case R.id.takePicture:
            if (mLauncherManager != null) {
            	Log.i(TAG, "featureTakeAPicture");
            	mLauncherManager.featureTakeAPicture(false);
            }
            break;
        case R.id.recordVideo:
            if (mLauncherManager != null) {
                mLauncherManager.featureRecordAVideo();
            }
            break;
        case R.id.browser:
            if (mLauncherManager != null) {
                mLauncherManager.addBrowserCard();
            }
            break;
        }
    }

    @Override
    public void onAfterHideMenu() {
        if (mLauncherManager != null) {
        	Log.i(TAG, "onAfterHideMenu");
        	mLauncherManager.startVoiceCommand();
        }
    }

    @Override
    public void displaydMenu() {
        mTimeLineView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void cancelMenu() {
        mTimeLineView.setVisibility(View.VISIBLE);
    }

    @Override
    public void closeMenu() {
        if (mSelectMenu == true) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTimeLineView.setVisibility(View.VISIBLE);
                }
            }, 500);
        }
    }

    @Override
    public void checkVisible() {
        if (mTimeLineView.isShown() == false) {
            mTimeLineView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    void onMotionEvent(int action, String event) {
    }

    public void stop() {
    }

    @Override
    void updateUi() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour > 12) {
            hour = hour - 12;
        } else if (hour == 0) {
            hour = 12;
        }

        String timeString = String.format("%02d:%02d", hour,
                calendar.get(Calendar.MINUTE));
        ((TextView) mTimeLineView.findViewById(R.id.timeLineText))
                .setText(timeString);
    }
}