package com.agreeyamobility.glasslauncher.ui;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agreeyamobility.glasslauncher.Common.Weather;
import com.agreeyamobility.glasslauncher.Common.Weather.CityInfo;
import com.agreeyamobility.glasslauncher.Common.Weather.CloudsInfo;
import com.agreeyamobility.glasslauncher.Common.Weather.HumidityInfo;
import com.agreeyamobility.glasslauncher.Common.Weather.PrecipitationInfo;
import com.agreeyamobility.glasslauncher.Common.Weather.PressureInfo;
import com.agreeyamobility.glasslauncher.Common.Weather.TemperatureInfo;
import com.agreeyamobility.glasslauncher.Common.Weather.WindInfo;
import com.agreeyamobility.glasslauncher.R;
import com.agreeyamobility.glasslauncher.provider.LauncherProvider.WeatherColumns;

public class WeatherCard extends Card {
    // private static final String TAG = "WeatherCard";

    private TextView mCityView;
    private TextView mTempView;
    private TextView mHumidityView;
    private TextView mTempMaxView;

    private RelativeLayout mHumidityLayout;
    private RelativeLayout mTempMaxLayout;

    private View mDivider;

    private WeatherLoader mWeatherLoader;

    private ContentResolver mResolver;
    private View mAddMenuView;

    private ContentObserver mContentObserver;

    public WeatherCard(Context context, CardScrollView base) {
        super(context, base, -1, -1);
    }

    @Override
    View createView(Context context, ViewGroup parent) {
        return mInflater.inflate(R.layout.card_content_weather, parent, false);
    }

    @Override
    void initialize(View view) {
        mContentObserver = new ContentObserver(new Handler()) {

            @Override
            public void onChange(boolean selfChange) {
                mWeatherLoader.scanWeather();
            }
        };

        mCityView = (TextView) view.findViewById(R.id.weatherCity);
        mTempView = (TextView) view.findViewById(R.id.weatherTemp);
        mHumidityView = (TextView) view.findViewById(R.id.weatherHumidi);
        mTempMaxView = (TextView) view.findViewById(R.id.tempMax);
        mTempMaxLayout = (RelativeLayout) view.findViewById(R.id.tempLayout);
        mHumidityLayout = (RelativeLayout) view
                .findViewById(R.id.humidityLayout);
        mDivider = (View) view.findViewById(R.id.divider);

        // check database
        mResolver = getContext().getContentResolver();
        mResolver.registerContentObserver(WeatherColumns.CONTENT_URI, true, mContentObserver);

        mWeatherLoader = new WeatherLoader(getContext());
        mWeatherLoader.scanWeather();

        mCanMenu = true;
    }

    @Override
    boolean isDefault() {
        return false;
    }

    @Override
    public void createMenu() {
        mAddMenuView = mInflater.inflate(R.layout.menu_layout, null, false);
        mAddMenuView.setId(R.id.add_city);

        ((ImageView) mAddMenuView.findViewById(R.id.menuIcon))
                .setImageResource(android.R.drawable.ic_menu_add);
        ((TextView) mAddMenuView.findViewById(R.id.menuTitle))
                .setText(R.string.menu_add_city);
    }

    @Override
    public void prepareMenu(CardScrollView menu) {
        Cursor cursor = mResolver.query(WeatherColumns.CONTENT_URI,
                new String[] { WeatherColumns.ID, WeatherColumns.LAT,
                        WeatherColumns.LON, WeatherColumns.NAME,
                        WeatherColumns.DEFAULT }, null, null,
                WeatherColumns.NAME + " ASC");

        boolean isDefault = true;

        if (cursor != null) { 
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String cityName = cursor.getString(3);
                    if (cityName != null) {
                        int defaultValue = cursor.getInt(4);
                        final View cityView = mInflater.inflate(
                                R.layout.menu_layout, null, false);
                        cityView.setId(R.id.city);
                        cityView.setTag(cityName);
                        TextView title = (TextView) cityView
                                .findViewById(R.id.menuTitle);
                        title.setText(cityName);
                        title.setTag(cursor.getInt(0));
                        ImageView icon = (ImageView) cityView
                                .findViewById(R.id.menuIcon);
                        icon.setTag(defaultValue);
                        icon.setImageResource(defaultValue == 1 ?
                                R.drawable.ic_navigation_default
                                : R.drawable.ic_navigation);
                        menu.addListItem(cityView, isDefault);

                        if (isDefault == true) {
                            isDefault = false;
                        }
                    }
                }
            }
            cursor.close();
        }

        menu.addListItem(mAddMenuView, isDefault);
    }

    @Override
    public void onSelectMenu(View view, int id) {
        switch (id) {
        case R.id.add_city:
            mLauncherManager.featureAddWeatherCity();
            break;
        case R.id.city:
            int isDefault = (Integer) ((ImageView) view
                    .findViewById(R.id.menuIcon)).getTag();
            if (isDefault != 1) {
                int cityId = (Integer) ((TextView) view
                        .findViewById(R.id.menuTitle)).getTag();
                ContentValues values = new ContentValues();
                values.put(WeatherColumns.DEFAULT, 1);
                int result = mResolver.update(WeatherColumns.CONTENT_URI, values,
                        WeatherColumns.ID + " = " + cityId, null);
                if (result > 0) {
                    mWeatherLoader.scanWeather();
                }
            }
            break;
        }
    }

    @Override
    void onMotionEvent(int action, String event) {
        switch (action) {
        case CardScrollView.VIEW_ACTION_IDLE:
            // mWeatherLoader.scanWeather();
            break;
        }
    }

    private void updateWeatherInfo(WeatherInfo info) {
        if (info != null) {
            mCityView.setText(info.cityName);
            mTempView.setText(getContext().getString(R.string.weather_temp,
                    info.temperature));
            mHumidityView.setText(info.humidity);
            mTempMaxView.setText(getContext().getString(R.string.weather_temp,
                    info.tempMax));
        }
    }

    private class WeatherLoader {
        // Httpclient object
        private HttpClient mHttpclient;
        // Weather information request async task
        private LoadWeatherTask mWeatherTask;
        // Receive data parser
        private WeatherXMLParser mWeaterParser;
        // Return the event to parent
        private UpdateResultHandler mResultHandler;
        // Receive data event handler
        private WeatherInfo mResultWeather;

        private WifiManager mWifiManager;
        private ConnectivityManager mConnectivityManager;

        public WeatherLoader(Context context) {
            mResultHandler = new UpdateResultHandler();
            mWeaterParser = new WeatherXMLParser();

            // check wifi enable
            mWifiManager = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
        }

        public void scanWeather() {
            if (canUseWiFi() == true) {
                mResultWeather = null;
                mWeatherTask = new LoadWeatherTask();
                mWeatherTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                mTempMaxLayout.setVisibility(View.VISIBLE);
                mHumidityLayout.setVisibility(View.VISIBLE);
                mDivider.setVisibility(View.VISIBLE);
            } else {
                mCityView.setText("Can't network");
                mTempMaxLayout.setVisibility(View.GONE);
                mHumidityLayout.setVisibility(View.GONE);
                mDivider.setVisibility(View.GONE);
            }
        }

        private boolean canUseWiFi() {
            if (mWifiManager.isWifiEnabled()) {
                NetworkInfo wifi = mConnectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                return wifi.isConnected();
            } else {
                return false;
            }
        }

        private class LoadWeatherTask extends AsyncTask<Void, Void, Boolean> {
            private static final int CONNECTION_MAX_TIME = 10000;
            // Query URL
            private String mSearchUrl = null;

            public LoadWeatherTask() {
                makeURL();
            }

            /**
             * Using web site - http://openweathermap.org/API
             */
            private void makeURL() {
                Cursor cursor = mResolver.query(WeatherColumns.CONTENT_URI,
                        new String[] { WeatherColumns.ID, WeatherColumns.LAT,
                                WeatherColumns.LON, WeatherColumns.NAME },
                        WeatherColumns.DEFAULT + " = 1", null,
                        WeatherColumns.NAME + " ASC");
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    String cityName = cursor.getString(3);
                    StringBuilder strBuilder = new StringBuilder();
                    strBuilder.append(Weather.WEATHER_URI_FIRST);
                    strBuilder.append(cityName);
                    strBuilder.append(Weather.WEATHER_URI_SECOND);
                    mSearchUrl = strBuilder.toString();
                }
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                mHttpclient = new DefaultHttpClient();
                try {
                    HttpGet get = new HttpGet();
                    get.setURI(new URI(mSearchUrl));

                    HttpParams param = mHttpclient.getParams();
                    // Set protocol version
                    param.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
                            HttpVersion.HTTP_1_1);
                    // Set timeout
                    HttpConnectionParams.setConnectionTimeout(param,
                            CONNECTION_MAX_TIME);
                    HttpConnectionParams.setSoTimeout(param,
                            CONNECTION_MAX_TIME);
                    // Receive data
                    mHttpclient.execute(get, mResponseSearchHandler);
                } catch (ConnectTimeoutException e) {
                    mResultHandler.sendMessage(mResultHandler.obtainMessage(
                            UpdateResultHandler.MESSAGE_ERROR, e.toString()));
                } catch (Exception e) {
                    mResultHandler.sendMessage(mResultHandler.obtainMessage(
                            UpdateResultHandler.MESSAGE_ERROR, e.toString()));
                } finally {
                    mHttpclient.getConnectionManager().shutdown();
                }
                return null;
            }

            // Receive data handler
            private ResponseHandler<Void> mResponseSearchHandler = new ResponseHandler<Void>() {

                @Override
                public Void handleResponse(HttpResponse response)
                        throws ClientProtocolException, IOException {
                    try {
                        // Read stream
                        InputStreamReader isr = new InputStreamReader(response
                                .getEntity().getContent(), "UTF-8");

                        // Set data in XMLparser.
                        XmlPullParserFactory parserCreator = XmlPullParserFactory
                                .newInstance();
                        XmlPullParser parser = parserCreator.newPullParser();
                        parser.setInput(isr);

                        // Start parsing
                        mResultWeather = mWeaterParser.startParsing(parser);
                        if (mResultWeather != null && mResultHandler != null) {
                            // Return end parsing event to parent
                            mResultHandler
                                    .sendMessage(mResultHandler
                                            .obtainMessage(UpdateResultHandler.MESSAGE_COMPLETE));
                        }
                    } catch (Exception e) {
                        return null;
                    }
                    return null;
                }
            };
        }

        private class UpdateResultHandler extends Handler {
            public static final int MESSAGE_COMPLETE = 0x01;
            public static final int MESSAGE_ERROR = 0x02;

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                case MESSAGE_COMPLETE:
                    updateWeatherInfo(mResultWeather);
                    break;
                case MESSAGE_ERROR:
                    break;
                }
            }
        }

        private class WeatherXMLParser {
            public WeatherXMLParser() {
            }

            public WeatherInfo startParsing(XmlPullParser parser)
                    throws XmlPullParserException, IOException {
                WeatherInfo weather = new WeatherInfo();
                int parserEvent = parser.getEventType();
                String tag;
                boolean isCountry = false;
                while (parserEvent != XmlPullParser.END_DOCUMENT) {
                    switch (parserEvent) {
                    case XmlPullParser.TEXT:
                        if (isCountry) {
                            weather.countyName = parser.getText();
                            isCountry = false;
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    case XmlPullParser.START_TAG:
                        tag = parser.getName();
                        if (tag.equals(CityInfo.WEATHER_CITY_BASE)) {
                            weather.cityName = parser.getAttributeValue(null,
                                    CityInfo.WEATHER_CITY_BASE_NAME);
                        } else if (tag.equals(CityInfo.WEATHER_CITY_COUNTRY)) {
                            isCountry = true;
                        } else if (tag.equals(TemperatureInfo.TEMPERATURE_BASE)) {
                            String value = parser.getAttributeValue(null,
                                    TemperatureInfo.TEMPERATURE_BASE_VALUE);
                            float tem = (float) (Float.valueOf(value) - Weather.WEATER_KELVIN_CELSIUS_OFFSET);
                            weather.temperature = new StringBuilder().append(
                                    String.format("%.0f", tem)).toString();

                            String max = parser.getAttributeValue(null,
                                    TemperatureInfo.TEMPERATURE_BASE_MAX);
                            tem = (float) (Float.valueOf(max) - Weather.WEATER_KELVIN_CELSIUS_OFFSET);
                            weather.tempMax = new StringBuilder().append(
                                    String.format("%.0f", tem)).toString();
                        } else if (tag.equals(HumidityInfo.HUMIDITY_BASE)) {
                            weather.humidity = new StringBuilder()
                                    .append(parser.getAttributeValue(null,
                                            HumidityInfo.HUMIDITY_BASE_VALUE))
                                    .append(" ")
                                    .append(parser.getAttributeValue(null,
                                            HumidityInfo.HUMIDITY_BASE_UNIT))
                                    .toString();
                        } else if (tag.equals(PressureInfo.PRESSURE_BASE)) {
                            weather.pressure = new StringBuilder()
                                    .append(parser.getAttributeValue(null,
                                            PressureInfo.PRESSURE_BASE_VALUE))
                                    .append(" ")
                                    .append(parser.getAttributeValue(null,
                                            PressureInfo.PRESSURE_BASE_UNIT))
                                    .toString();
                        } else if (tag.equals(WindInfo.WIND_BASE_SPEED)) {
                        } else if (tag.equals(WindInfo.WIND_BASE_DIRECTION)) {
                        } else if (tag.equals(CloudsInfo.CLOUDS_BASE)) {
                            weather.clouds = parser.getAttributeValue(null,
                                    CloudsInfo.CLOUDS_BASE_NAME);
                        } else if (tag
                                .equals(PrecipitationInfo.PRECIPITATION_BASE)) {
                            String value = parser.getAttributeValue(null,
                                    PrecipitationInfo.PRECIPITATION_BASE_MODE);
                            if (!value.equals("no")) {
                            }
                        } else if (tag.equals(Weather.WeatherInfo.WEATHER_BASE)) {
                        }
                        break;
                    }
                    parserEvent = parser.next();
                }
                return weather;
            }
        }
    }

    public class WeatherInfo {
        public String countyName = null;
        public String cityName = null;
        public String temperature = null;
        public String humidity = null;
        public String pressure = null;
        public String clouds = null;
        public String precipitation = null;
        public String wind = null;
        public String tempMax = null;
    }

    @Override
    void updateUi() {
        if (mHumidityLayout.isShown() == false) {
            mWeatherLoader.scanWeather();
        }
    }
}