package com.agreeyamobility.glasslauncher.voice;

public class Data {
    // public int playPos;
    public int length;
    public short[] buffer;

    public Data(short[] data, int play_pos) {
        this.length = data.length;
        this.buffer = data;
        // this.playPos=play_pos;
    }

    public Data(short[] data) {
        this.length = data.length;
        this.buffer = data;
        // this.playPos=-1;
    }
}