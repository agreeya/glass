package com.agreeyamobility.glasslauncher.voice;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.baidu.speech.easr.easrNativeJni;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;

public class DecodeTask extends AsyncTask<Void, Integer, Void> {
    private ArrayList<Data> dataQueue = new ArrayList<Data>();
    private Handler handle;

    public int monitoring = 0;
    private String selectedlibdir = "";
    public boolean writeToFile = false;

    public DecodeTask(Handler param) {
        dataQueue.clear();
        handle = param;
    }

    public void setLibdir(String libdir) {
        selectedlibdir = libdir;
    }

    public void GetData(Data data) {
        dataQueue.add(data);
        VADDetect(data.buffer, data.length);
    }

    public int VADDetect(short[] buffer, int dataLen) {
        boolean end = false;
        if (monitoring == 2) {
            end = true;
        }
        int vadRet = easrNativeJni.VADDetect(0, buffer, dataLen, end);
        return vadRet;
    }

    public int Rec(short[] buffer, int dataLen) {
        if (dataLen <= 0)
            return -1;

        int expectNum = 1;// 5;//1;
        byte[][] sentenceT = new byte[expectNum][1000];
        boolean end = false;
        if (monitoring == 2 && dataQueue.isEmpty())
            end = true;

        if (this.isCancelled()) {
            return 0;
        }

        int n = easrNativeJni.Decode(0, buffer, dataLen, sentenceT, expectNum,
                end);

        String printInfo = "";
        if (n < 0) {
            String immeStr = easrNativeJni.GetImmeSentence(0);
            printInfo = "Imme: " + immeStr + "\n";
        }
        if (n == 0) {
            printInfo = "no result, show the Imme result:\n\n";
            String immeStr = easrNativeJni.GetImmeSentence(0);
            printInfo += "Imme: " + immeStr + "\n";
        }
        if (n > 0) {
            printInfo = "result: " + "\n\n";
            for (int i = 0; i < n; i++) {
                String sen = null;
                try {
                    sen = new String(sentenceT[i], "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                int seri = i + 1;
                String reject = "";
                if (i == 0 && sen.charAt(0) == '0')
                    reject = " (x)";
                int firstSilIndex = sen.indexOf("sil");
                String resultForDisplay = sen.substring(0, firstSilIndex);
                printInfo += seri + ": " + resultForDisplay + reject + "\n";

                if (resultForDisplay.length() > 7) {
                    handle.sendMessage(handle.obtainMessage(
                            VoiceManager.DECODE_STATUS_COMMAND,
                            resultForDisplay.substring(7,
                                    resultForDisplay.length() - 1)));
                }
            }
        }

        boolean bEd = false;
        if (n >= 0)
            bEd = true;

        handle.sendMessage(handle.obtainMessage(
                VoiceManager.DECODE_STATUS_RESULT, printInfo));

        if (bEd) {
            return n;
        } else {
            return -3;
        }

    }

    @SuppressLint("SdCardPath")
    @Override
    protected Void doInBackground(Void... arg0) {

        while (true) {
            if (this.isCancelled()) {
                break;
            }
            if (dataQueue.size() <= 0) {
                continue;
            }

            Data ori = dataQueue.get(0);
            if (ori != null) {
                synchronized(ori) {
                    dataQueue.remove(0);
                    int n = Rec(ori.buffer, ori.length);
                    if (n >= 0) {
                        break;
                    }
                }
            }
        }
        dataQueue.clear();
        monitoring = 0;
        String printInfo = "";
        handle.sendMessage(handle.obtainMessage(
                VoiceManager.DECODE_STATUS_FINISH, printInfo));
        return null;
    }
}