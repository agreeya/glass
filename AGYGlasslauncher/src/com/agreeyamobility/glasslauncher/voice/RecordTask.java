package com.agreeyamobility.glasslauncher.voice;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Handler;

public class RecordTask extends AsyncTask<Void, Integer, Void> {
    private Handler handle;

    private static final int frequence = 8000;
    private static final int channelConfig = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    private static final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;

    private int MAXLEN = frequence * 300;
    private int EACHLEN = 8000;
    private short[] data = new short[MAXLEN];
    private int stPos = 0;
    private int readPos = 0;

    public File audioFile = null;
    public boolean bRecording = false;

    public RecordTask(Handler param) {
        handle = param;
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        AudioRecord record = null;
        DataOutputStream dos = null;

        stPos = 0;
        readPos = 0;

        try {
            dos = new DataOutputStream(new BufferedOutputStream(
                    new FileOutputStream(audioFile)));

            int bufferSize = EACHLEN;
            short[] buffer = new short[bufferSize];

            record = new AudioRecord(MediaRecorder.AudioSource.MIC, frequence,
                    channelConfig, audioEncoding, bufferSize);
            record.startRecording();

            while (bRecording) {
                int bufferReadResult = record.read(buffer, 0, buffer.length);
                for (int i = 0; i < bufferReadResult; i++) {
                    dos.writeShort(buffer[i]);
                    synchronized (this) {
                        data[stPos + i] = buffer[i];
                    }
                }

                SendData(stPos, bufferReadResult);

                stPos += bufferReadResult;
                if (MAXLEN - stPos < bufferSize)
                    stPos = 0;
            }

            int bufferReadResult = record.read(buffer, 0, buffer.length);
            for (int i = 0; i < bufferReadResult; i++) {
                dos.writeShort(buffer[i]);
                synchronized (this) {
                    data[stPos + i] = buffer[i];
                }
            }
            SendData(stPos, bufferReadResult);
            stPos += bufferReadResult;
            if (MAXLEN - stPos < bufferSize)
                stPos = 0;

            record.stop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (record != null) {
                record.release();
            }
            try {
                if (dos != null) {
                    dos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    void SendData(int st, int ed) {
        int writePos = st + ed;

        int readLen = (writePos - readPos) - (writePos - readPos) % 24;
        if (readLen < 24)
            return;

        short[] tmp = new short[readLen];
        for (int i = 0; i < readLen; i++) {
            tmp[i] = data[readPos + i];
        }
        readPos += readLen;
        Data data = new Data(tmp);
        handle.sendMessage(handle.obtainMessage(
                VoiceManager.DECODE_STATUS_AUDIO_DATA, data));
    }
}