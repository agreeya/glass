package com.agreeyamobility.glasslauncher.voice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.baidu.speech.easr.easrNativeJni;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

public class VoiceManager {
    private static final String TAG = "VoiceManager";

    private static final String DIRECTORY = "easr";

    private RecordTask recordTask = null;
    private DecodeTask decodeTask = null;
    private Handler decodeStatusHandler;
    public static final int DECODE_STATUS_AUDIO_DATA = 1;
    public static final int DECODE_STATUS_RESULT = 2;
    public static final int DECODE_STATUS_FINISH = 3;
    public static final int DECODE_STATUS_FILE_READ_FINISH = 4;
    public static final int DECODE_STATUS_COMMAND = 5;

    private static String gramsyn;// f_1
    private static String hmmdef;// f_3
    private static String hmmMap;// f_4
    private static String dict;// f_5
    private static String userDict;// f_6, usually is null

    private static String pcmfile;
    private static String pcmfolder;

    private static String slotfile;// f_b
    private static String slotlinkfile;// f_c
    private static String lmfile;// f_a
    private static String res_slotfile;// c_b
    private static String res_slotlinkfile;// c_c
    private static String res_lmfile;// c_a

    private static String guide;
    private static String selectedlibdir = "ime";
    private Handler uiHandler;
    private static final int SHOW_PROGRESS_DIALOG = 0;
    private static final int DISMISS_PROGRESS_DIALOG = 1;
    private static final int UPDATE_GUIDE_TEXT_VIEW = 2;
    private static final int UPDATE_INFO_TEXT_VIEW = 3;
    private static final int CLEAR_INFO_TEXT_VIEW = 4;
    private static final int UI_RESET_BUTTON_STATUS = 7;
    private static final int VOICE_STANDBY = 8;

    private Context mContext;
    private VoiceManagerListener mListener;

    private boolean mReport;

    private String mPath;

    private boolean mIsReady;

    public VoiceManager(Context context, VoiceManagerListener listener) {
        mContext = context;
        mListener = listener;
        mIsReady = false;
        intialize();
    }

    public boolean isReady() {
        return mIsReady;
    }

    private void intialize() {
        uiHandler = new Handler(new Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                case SHOW_PROGRESS_DIALOG:
                    break;
                case VOICE_STANDBY :
                    // ready
                    mIsReady = true;
                    if (mListener != null) {
                        mListener.onVoiceStandy();
                    }
                    break;
                case DISMISS_PROGRESS_DIALOG:
                    break;
                case UPDATE_GUIDE_TEXT_VIEW:
                    // infoContacts.setText((CharSequence) msg.obj);
                    break;
                case UPDATE_INFO_TEXT_VIEW:
                    // infoShow.setText((CharSequence) msg.obj);
                    break;
                case CLEAR_INFO_TEXT_VIEW:
                    // infoShow.setText("");
                    break;
                case UI_RESET_BUTTON_STATUS:
                    // recordButton.setEnabled(true);
                    // recordButton.setText(R.string.start_speak);
                    showCMD();
                    break;
                default:
                    break;
                }
                return false;
            }
        });

        decodeStatusHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                case DECODE_STATUS_AUDIO_DATA:
                    Data data = (Data) msg.obj;
                    decodeTask.GetData(data);
                    break;
                case DECODE_STATUS_RESULT:
                    String result = (String) msg.obj;
                    if (result.startsWith("no")) {
                        mListener.onVoiceCommand(null);
                    }
                    break;
                case DECODE_STATUS_FINISH:
                    if (recordTask != null) {
                        recordTask.bRecording = false;
                        if (mReport == false) {
                            mListener.onVoiceCommand(null);
                        }
                    }
                    break;
                case DECODE_STATUS_FILE_READ_FINISH:
                    Log.i(TAG, "file read over");

                    if (decodeTask != null && decodeTask.monitoring != 0) {
                        decodeTask.monitoring = 2;
                        short[] tmp = new short[256];
                        for (int i = 0; i < 256; i++)
                            tmp[i] = 0;
                        Data data2 = new Data(tmp);
                        decodeTask.GetData(data2);
                    }
                    break;
                case DECODE_STATUS_COMMAND:
                    mReport = true;
                    mListener.onVoiceCommand((String) msg.obj);
                    break;
                }
            }
        };

        mPath = Environment.getExternalStorageDirectory().getPath() + "/"
                + DIRECTORY;
        if (mPath != null) {
            java.io.File dir = new java.io.File(mPath);
            if (!dir.exists()) {
                new ResourceUnpackTask().execute();
            } else {
                new ModelSwitchTask().execute();
            }
        }
    }

    public void onStartVoiceCommand() {
        if (mIsReady == true) {
            if (recordTask == null || !recordTask.bRecording) {
                mReport = false;
                easrNativeJni.ResetVAD(0);
                easrNativeJni.ResetDecoder(0);
                easrNativeJni.SetCurrNetTreeID(0, 0, 0);

                File mediafile = null;

                boolean sd = Environment.getExternalStorageState().equals(
                        android.os.Environment.MEDIA_MOUNTED);
                if (sd) {
                    pcmfile = pcmfolder + "tmp.pcm";
                    mediafile = new File(pcmfile);
                } else {
                    return;
                }

                decodeTask = new DecodeTask(decodeStatusHandler);
                decodeTask.monitoring = 1;
                decodeTask.setLibdir(selectedlibdir);
                decodeTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                recordTask = new RecordTask(decodeStatusHandler);
                recordTask.audioFile = mediafile;
                recordTask.bRecording = true;
                recordTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    public void onStopVoiceCommand() {
        if (mIsReady == true) {
            if (decodeTask != null && decodeTask.monitoring != 0) {
                decodeTask.monitoring = 2;
                short[] tmp = new short[256];
                for (int i = 0; i < 256; i++)
                    tmp[i] = 0;
                Data data2 = new Data(tmp);
                decodeTask.GetData(data2);
//                decodeTask.cancel(true);
            }
        }
    }

    private void showCMD() {
        guide = mPath + "/" + selectedlibdir + "/guide.txt";

        StringBuilder builder = new StringBuilder("");
        try {
            File file = new File(guide);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                builder.append(tempString);
                builder.append("\n");
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String version = selectedlibdir;
        String info = version + builder.toString();
        uiHandler.sendMessage(uiHandler.obtainMessage(UPDATE_GUIDE_TEXT_VIEW,
                info));
    }

    private class ModelSwitchTask extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {
            uiHandler.sendMessage(uiHandler.obtainMessage(SHOW_PROGRESS_DIALOG,
                    "ModelSwitchTask"));
            load();
            showCMD();
            uiHandler.sendEmptyMessage(VOICE_STANDBY);
            return null;
        }

    }

    private void load() {
        String path = mPath + "/" + selectedlibdir + "/";

        gramsyn = path + "s_1:f_1";
        hmmdef = path + "s_1:f_3";
        hmmMap = path + "s_1:f_4";
        dict = path + "s_1:f_5";
        userDict = path + "s_1:f_6";
        pcmfolder = path + "/audio-data/";

        lmfile = path + "s_1:f_a";
        slotfile = path + "s_1:f_b";
        slotlinkfile = path + "s_1:f_c";

        res_lmfile = path + "s_1:c_a";
        res_slotfile = path + "s_1:c_b";
        res_slotlinkfile = path + "s_1:c_c";
        java.io.File dir = new java.io.File(pcmfolder);
        if (!dir.exists())
            dir.mkdirs();

        easrNativeJni.Free();
        //easrNativeJni.SetSampleRateMode(2);

        easrNativeJni.SetLogLevel(5);

        String authorMethod = "test";// Test
        if (authorMethod.equals("server")) {
            byte[] license = new byte[2048];
            byte[] appID = new byte[32];

            String cuid = "cuid-123456";
            String stat = "android-stat";
            int len = easrNativeJni.GetLicense(mContext, cuid, stat, license);
            if (len > 0) {
                int author = easrNativeJni.VerifyLicense(mContext, cuid,
                        license, len, appID);
                if (author >= 0)
                    Log.i(TAG, "server author success");
                else
                    Log.i(TAG, "server author failure");
                String appStr = new String(appID);
                Log.i(TAG, "appID: " + appStr);
            } else {
                Log.i(TAG, "GetLicense failure,error code :" + len);
            }
        } else {
            int author = easrNativeJni.GetTestAuthorize();
            if (author >= 0)
                Log.i(TAG, "test author success");
            else
                Log.i(TAG, "test author failure");
        }

        if (easrNativeJni.Initial(dict, userDict, hmmdef, hmmMap, false) != 0) {
            return;
        }

        if (IsExist(slotfile))
            easrNativeJni.ReadSlot(slotfile);
        if (IsExist(slotlinkfile))
            easrNativeJni.ReadSlotLink(slotlinkfile);
        if (IsExist(res_slotfile) && IsExist(res_slotlinkfile)
                && IsExist(res_lmfile)) {
            easrNativeJni.LoadRes(res_lmfile, "$ngram_LM_LOOP_CORE",
                    res_slotfile, res_slotlinkfile);
        }
        if (easrNativeJni.BuildNet(-1, gramsyn) != 0) {
            return;
        }
        easrNativeJni.InitialDecoder(0, 0, 2000, 15000);
        easrNativeJni.InitialVAD(0, (float) 4.0, (float) 0.5);
    }

    boolean IsExist(String file) {
        String file2 = file;
        if (file2.contains(":")) {
            String[] arr = file2.split(":");
            file2 = arr[0];
        }
        java.io.File file_io = new java.io.File(file2);
        if (file_io.exists())
            return true;
        return false;
    }

    public void destroy() {
        if (decodeTask != null) {
            decodeTask.cancel(true);
        }
        easrNativeJni.Free();
    }

    private void showRedInfo(CharSequence printInfo) {
        Spannable colorfulMessage = new SpannableString(printInfo + "\n");
        colorfulMessage.setSpan(new ForegroundColorSpan(Color.RED), 0,
                printInfo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private class ResourceUnpackTask extends
            AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {
            uiHandler.sendMessage(uiHandler.obtainMessage(SHOW_PROGRESS_DIALOG,
                    "unpacking"));
            copyFileOrDir(DIRECTORY);
            uiHandler.sendEmptyMessage(DISMISS_PROGRESS_DIALOG);
            java.io.File dir = new java.io.File(mPath);
            if (!dir.exists()) {
                showRedInfo("Not found");
            } else {
                new ModelSwitchTask().execute();
            }
            return null;
        }

    }

    private void copyFileOrDir(String path) {
        AssetManager assetManager = mContext.getAssets();
        String assets[] = null;
        try {
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath = Environment.getExternalStorageDirectory()
                        + "/" + path;
                File dir = new File(fullPath);
                if (!dir.exists())
                    dir.mkdir();
                for (int i = 0; i < assets.length; ++i) {
                    copyFileOrDir(path + "/" + assets[i]);
                }
            }
        } catch (IOException ex) {
            Log.i(TAG, "copyFileOrDir :: ", ex);
        }
    }

    private void copyFile(String filename) {
        AssetManager assetManager = mContext.getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            String newFileName = Environment.getExternalStorageDirectory()
                    + "/" + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.i(TAG, "copyFile :: " + e.getMessage());
        }
    }

    public interface VoiceManagerListener {
        abstract void onVoiceCommand(String command);
        abstract void onVoiceStandy();
    }
}