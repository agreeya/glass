package com.baidu.speech.easr;

import android.content.Context;

public class easrNativeJni {
	public static native int SetLogLevel(int level);
	
	//只有置为2才是16k，其余都是8K，默认是8K
	public static native int SetSampleRateMode(int mode);//mode=2,16k
	
	//程序退出时调用，释放所有内存
	public static native int Free();

	public static native int Initial(String dictFile,String userFile,String mmfFile,String hmmMapFile,boolean fast_mode);
	public static native int BuildSlot(String slotBuffer,int len);
	public static native int ReadSlot(String slotFile);
	public static native int ReadSlotLink(String slotLinkFile);
    public static native int LoadRes(String lmFile,String lmSlotName,String slotFile,String slotLinkFile);
	public static native int BuildNet(int netTreeID,String gramsyn);
    
    public static native int InitialDecoder(int id,int vadID,int nBeam,double prunThres);
    public static native int ResetDecoder(int id);
    public static native int SetCurrNetTreeID(int id,int treeID,int mode);
    public static native int Decode(int id,short[] data, int len, byte[][] sentenceArr,int expectNum,boolean bEd);
	public static native String GetImmeSentence(int id);
    
	public static native int InitialVAD(int id,float maxSpeechSec,float maxSpeechPauseSec);
	public static native int ResetVAD(int id);
    public static native int VADDetect(int id,short[] data, int len, boolean bEd);
    public static native int GetVadSt(int id);
    public static native int GetVadEd(int id);
    
    public static native int GetLicense(Context context,String cuid,String stat,byte[] license);
    public static native int VerifyLicense(Context context,String cuid,byte[] license,int len,byte[] appID);
	public static native int GetTestAuthorize();
    
	static
	{
		System.loadLibrary("mmtScore");
		System.loadLibrary("bdEASRAndroid.baiduspeech.v3");
	}
}