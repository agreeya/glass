package com.agreeyamobility.agreeyaglass;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class AnimationActivity extends Activity implements AnimationListener {
    private static final int ANIMATION_DELAY_TIME = 2000; // 2sec

    private ImageView mLogoView;
    private Animation mHideAnimation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.animation_content);

        mHideAnimation = AnimationUtils.loadAnimation(this, R.anim.animation_hide);
        mHideAnimation.setAnimationListener(this);
        mLogoView = (ImageView)findViewById(R.id.agent_logo);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLogoView.startAnimation(mHideAnimation);
            }
        }, ANIMATION_DELAY_TIME);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        mLogoView.setVisibility(View.GONE);

        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }
}
