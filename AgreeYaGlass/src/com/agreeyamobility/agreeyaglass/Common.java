package com.agreeyamobility.agreeyaglass;

public class Common {
    public static final String TYPE_WIFI = "wifi";

    public interface ExtraKey {
        public static final String MENU_MODE = "mode";
        
        public static final String QR_CODE_TYPE = "qr_type";
        public static final String QR_ADDRESS= "address";
        public static final String QR_PASSWORD = "password";

        public static final String WIFI_ADDRESS= "address";
        public static final String WIFI_PASSWORD = "password";
        public static final String CITY_NAME = "name";
        public static final String SCHEDULE_DATE = "date";
        public static final String SCHEDULE_DATA = "data";
    }

    public interface WiFiType {
        public static final String WIFI_TYPE = "WIFI";
        public static final String WIFI_SSID = "#SSID@";
        public static final String WIFI_PASS = "@P#";

        public static final String WEP = "WEA";
        public static final String WPA = "WPA";
        public static final String WPA2 = "WPA2";
        public static final String ESS = "ESS";
    }

    public interface CityType {
        public static final String CITY_TYPE = "CITY";
        public static final String CITY_NAME = "#NAME@";
    }

    public interface ScheduleType {
        public static final String SCHEDULE_TYPE = "SCHEDULE";
        public static final String SCHEDULE_TIME = "#TIME@";
        public static final String SCHEDULE_DATA = "@DATA#";
    }
}
