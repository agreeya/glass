package com.agreeyamobility.agreeyaglass;

import com.agreeyamobility.agreeyaglass.Common.ExtraKey;
import com.agreeyamobility.agreeyaglass.fragment.QRCodeMakeFragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuActivity extends Activity implements OnClickListener {
    private Button mWifiBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meun_content);

        mWifiBtn = (Button) findViewById(R.id.menuWiFiConnect);

        mWifiBtn.setText(getString(R.string.btn_title_wifi_connect));

        mWifiBtn.setOnClickListener(this);
        ((Button) findViewById(R.id.menuBTConnect)).setOnClickListener(this);
        ((Button) findViewById(R.id.menuCity)).setOnClickListener(this);
        ((Button) findViewById(R.id.menuSchedule)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent data = null;

        switch (v.getId()) {
        case R.id.menuWiFiConnect:
            data = new Intent(this, QRCodeFragmentActivity.class);
            data.putExtra(ExtraKey.MENU_MODE, QRCodeMakeFragment.MODE_WIFI_QR_CODE);
            startActivity(data);
            break;
        case R.id.menuCity:
            data = new Intent(this, QRCodeFragmentActivity.class);
            data.putExtra(ExtraKey.MENU_MODE, QRCodeMakeFragment.MODE_CITY_QR_CODE);
            startActivity(data);
            break;
        case R.id.menuSchedule:
            data = new Intent(this, QRCodeFragmentActivity.class);
            data.putExtra(ExtraKey.MENU_MODE, QRCodeMakeFragment.MODE_SCHEDULE_QR_CODE);
            startActivity(data);
            break;
        }
    }
}