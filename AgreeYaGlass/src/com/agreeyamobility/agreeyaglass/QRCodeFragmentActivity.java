package com.agreeyamobility.agreeyaglass;

import java.util.ArrayList;

import com.agreeyamobility.agreeyaglass.Common.ExtraKey;
import com.agreeyamobility.agreeyaglass.fragment.WeatherCityFragment;
import com.agreeyamobility.agreeyaglass.fragment.QRCodeMakeFragment;
import com.agreeyamobility.agreeyaglass.fragment.ScheduleFragment;
import com.agreeyamobility.agreeyaglass.fragment.WiFiListFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;


public class QRCodeFragmentActivity extends FragmentActivity {

    private int mMode = QRCodeMakeFragment.MODE_NONE;

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;

    private QRCodeMakeFragment mQrCodeMakeFragment;

    @Override
    protected void onCreate(Bundle stateBundle) {
        super.onCreate(stateBundle);

        setContentView(R.layout.qr_code_fragment_content);
        initialize();
    }

    private void initialize() {
        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        ((Button) findViewById(R.id.finishBtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // mode check
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            mMode = extra.getInt(ExtraKey.MENU_MODE, QRCodeMakeFragment.MODE_NONE);
        }

        switch (mMode) {
        case QRCodeMakeFragment.MODE_WIFI_QR_CODE:
            mPagerAdapter.addFragment(new WiFiListFragment());
            break;
        case QRCodeMakeFragment.MODE_CITY_QR_CODE:
            mPagerAdapter.addFragment(new WeatherCityFragment());
            break;
        case QRCodeMakeFragment.MODE_SCHEDULE_QR_CODE:
            mPagerAdapter.addFragment(new ScheduleFragment());
            break;
        }

        mQrCodeMakeFragment = new QRCodeMakeFragment();
        mPagerAdapter.addFragment(mQrCodeMakeFragment);
        mPagerAdapter.notifyDataSetChanged();
    }

    public void makeQRCode(Bundle data) {
        mQrCodeMakeFragment.createQRCode(mMode, data);
    }

    public void updateUI() {
        mViewPager.setCurrentItem(mPagerAdapter.getCount()-1);
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Fragment> mFragmentsList = new ArrayList<Fragment>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
            mFragmentsList.clear();
        }

        public void addFragment(Fragment fragment) {
            mFragmentsList.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentsList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentsList.size();
        }
    }
}