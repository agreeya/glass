package com.agreeyamobility.agreeyaglass.fragment;


import com.agreeyamobility.agreeyaglass.QRCodeFragmentActivity;
import com.agreeyamobility.agreeyaglass.R;
import com.agreeyamobility.agreeyaglass.Common.CityType;
import com.agreeyamobility.agreeyaglass.Common.ExtraKey;
import com.agreeyamobility.agreeyaglass.Common.ScheduleType;
import com.agreeyamobility.agreeyaglass.Common.WiFiType;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class QRCodeMakeFragment extends Fragment {
    public static final int MODE_NONE     = 0x00;
    public static final int MODE_WIFI_QR_CODE     = 0x01;
    public static final int MODE_CITY_QR_CODE     = 0x02;
    public static final int MODE_SCHEDULE_QR_CODE = 0x03;
    
    private Point mDisplaySize;
    private ImageView mImageView;

    private int mMode = MODE_NONE;
    private Bundle mBundle = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.qr_code_view_content, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDisplaySize = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(mDisplaySize);
        mImageView = (ImageView) getActivity().findViewById(R.id.qrView);
    }

    public void createQRCode(int mode, Bundle data) {
        mMode = mode;
        mBundle = data;
        String makeQr = null;

        if (mBundle != null) {
            StringBuilder strBuilder = new StringBuilder();
            switch (mMode) {
            case MODE_WIFI_QR_CODE:
                strBuilder.append(WiFiType.WIFI_TYPE);
                strBuilder.append(WiFiType.WIFI_SSID);
                strBuilder.append(mBundle.getString(ExtraKey.WIFI_ADDRESS));
                strBuilder.append(WiFiType.WIFI_PASS);
                strBuilder.append(mBundle.getString(ExtraKey.WIFI_PASSWORD));
                makeQr = strBuilder.toString();
                break;
            case MODE_CITY_QR_CODE:
                strBuilder.append(CityType.CITY_TYPE);
                strBuilder.append(CityType.CITY_NAME);
                strBuilder.append(mBundle.getString(ExtraKey.CITY_NAME));
                makeQr = strBuilder.toString();
                break;
            case MODE_SCHEDULE_QR_CODE:
                strBuilder.append(ScheduleType.SCHEDULE_TYPE);
                strBuilder.append(ScheduleType.SCHEDULE_TIME);
                strBuilder.append(mBundle.getString(ExtraKey.SCHEDULE_DATE));
                strBuilder.append(ScheduleType.SCHEDULE_DATA);
                strBuilder.append(mBundle.getString(ExtraKey.SCHEDULE_DATA));
                makeQr = strBuilder.toString();
                break;
            }
    
            if (makeQr != null) {
                new CreateQRCodeAsyncTask().execute(makeQr);
            }
        }
    }

    private class CreateQRCodeAsyncTask extends AsyncTask<String, Void, Bitmap> {
        private static final int WHITE = 0xFFFFFFFF;
        private static final int BLACK = 0xFF000000;

        @Override
        protected Bitmap doInBackground(String... params) {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();

            try {
                BitMatrix result = qrCodeWriter.encode(params[0],
                        BarcodeFormat.QR_CODE, mDisplaySize.x, mDisplaySize.x);
                int width = result.getWidth();
                int height = result.getHeight();
                int[] pixels = new int[width * height];
                // All are 0, or black, by default
                for (int y = 0; y < height; y++) {
                    int offset = y * width;
                    for (int x = 0; x < width; x++) {
                        pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
                    }
                }

                Bitmap bitmap = Bitmap.createBitmap(width, height,
                        Bitmap.Config.ARGB_8888);
                bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

                return bitmap;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                mImageView.setImageBitmap(result);
                mImageView.invalidate();
                ((QRCodeFragmentActivity) getActivity()).updateUI();
            }
        }
    }
}
