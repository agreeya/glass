package com.agreeyamobility.agreeyaglass.fragment;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.agreeyamobility.agreeyaglass.Common.ExtraKey;
import com.agreeyamobility.agreeyaglass.QRCodeFragmentActivity;
import com.agreeyamobility.agreeyaglass.R;

public class ScheduleFragment extends Fragment {
    private Button mDate;
    private Button mTime;
    private EditText mData;

    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_make_content, container,
                false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDate = (Button) getActivity().findViewById(R.id.date);
        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), mDateSetListener, mYear, mMonth-1, mDay).show();
            }
        });

        mTime = (Button) getActivity().findViewById(R.id.time);
        mTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), mTimeSetListener, mHour, mMinute, true).show();
            }
        }); 

        mData = (EditText) getActivity().findViewById(R.id.scheduleData);

        ((Button) getActivity().findViewById(R.id.createQR))
            .setOnClickListener(new View.OnClickListener() {
                @Override
                    public void onClick(View v) {
                        Editable datas = mData.getText();
                        if (datas != null && !TextUtils.isEmpty(datas.toString().trim())) {
                            Bundle data = new Bundle();
                            data.putString(ExtraKey.SCHEDULE_DATE, mDate.getText().toString()
                                    + " " + mTime.getText().toString());
                            data.putString(ExtraKey.SCHEDULE_DATA, datas.toString());
                            ((QRCodeFragmentActivity) getActivity()).makeQRCode(data);
                        }
                    }
            });
        initializeData();
    }

    private void initializeData() {
        GregorianCalendar gCalendar = new GregorianCalendar();
        mYear = gCalendar.get(Calendar.YEAR);
        mMonth = gCalendar.get(Calendar.MONTH) + 1;
        mDay= gCalendar.get(Calendar.DAY_OF_MONTH);
        mHour = gCalendar.get(Calendar.HOUR_OF_DAY);
        mMinute = gCalendar.get(Calendar.MINUTE);

        mDate.setText(String.format("%d-%d-%d", mYear, mMonth, mDay));
        mTime.setText(String.format("%d:%d", mHour, mMinute));
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear+1;
            mDay = dayOfMonth;
            mDate.setText(String.format("%d-%d-%d", year, monthOfYear+1, dayOfMonth));
        }
    };

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mHour = hourOfDay;
            mMinute = minute;
            mTime.setText(String.format("%d:%d", hourOfDay, minute));
        }
    };
}