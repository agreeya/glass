package com.agreeyamobility.agreeyaglass.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.agreeyamobility.agreeyaglass.Common.ExtraKey;
import com.agreeyamobility.agreeyaglass.QRCodeFragmentActivity;
import com.agreeyamobility.agreeyaglass.R;

public class WeatherCityFragment extends Fragment implements
        OnItemClickListener {
    private static final String SHAREDPREFERENCES = "glass";
    private static final String DATA_KEY = "citys";
    private static final String DIVIDER_STR = "@#";
    private WeatherCityAdapter mAdapter;
    private SharedPreferences mPref;

    private AlertDialog mDialog;
    private EditText mCityName;
    private String mFullData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.weather_city_list_content, container,
                false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPref = getActivity().getSharedPreferences(SHAREDPREFERENCES,
                Activity.MODE_PRIVATE);
        String datas = mPref.getString(DATA_KEY, "");

        if (TextUtils.isEmpty(datas.trim())) {
            initalizeData();
        }

        initialize();
    }

    private void initalizeData() {
        StringBuilder str = new StringBuilder();
        str.append("Beijing").append(DIVIDER_STR);
        str.append("Seoul");
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(DATA_KEY, str.toString());
        editor.commit();
    }

    private void initialize() {

        ((Button) getActivity().findViewById(R.id.weatherCityAdd))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFullData = mPref.getString(DATA_KEY, "");
                        mDialog.show();
                    }
                });

        mCityName = new EditText(getActivity());
        mCityName.setInputType(InputType.TYPE_CLASS_TEXT);
        mCityName.setHint(R.string.hint_add_city);

        mAdapter = new WeatherCityAdapter(getActivity());
        ListView listView = (ListView) getActivity()
                .findViewById(R.id.wifiList);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
        createDialog();

        String datas = mPref.getString(DATA_KEY, "");
        if (!TextUtils.isEmpty(datas.trim())) {
            String[] list = datas.split(DIVIDER_STR);
            for (String city : list) {
                mAdapter.addCity(city);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    private void createDialog() {
        mDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_title_add_city)
                .setView(mCityName)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                Editable input = mCityName.getText();
                                if (input != null
                                        && !TextUtils.isEmpty(input.toString()
                                                .trim())) {
                                    mAdapter.addCity(input.toString());
                                    SharedPreferences.Editor editor = mPref
                                            .edit();
                                    editor.putString(DATA_KEY, mFullData
                                            + DIVIDER_STR + input.toString());
                                    editor.commit();
                                }
                            }
                        }).setNegativeButton(android.R.string.cancel, null)
                .create();
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mCityName.setText("");
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        Bundle data = new Bundle();
        data.putString(ExtraKey.CITY_NAME, (String) mAdapter.getItem(position));
        ((QRCodeFragmentActivity) getActivity()).makeQRCode(data);
    }

    private class WeatherCityAdapter extends BaseAdapter {
        private ArrayList<String> mCityList;
        private LayoutInflater mInflater;

        public WeatherCityAdapter(Context context) {
            mCityList = new ArrayList<String>();
            mInflater = LayoutInflater.from(context);
        }

        public void addCity(String city) {
            mCityList.add(city);
        }

        @Override
        public int getCount() {
            return mCityList.size();
        }

        @Override
        public Object getItem(int position) {
            return mCityList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = newView(parent);
            }
            ((TextView) convertView.findViewById(android.R.id.text1))
                    .setText(mCityList.get(position));
            return convertView;
        }

        private View newView(ViewGroup parent) {
            View view = mInflater.inflate(
                    android.R.layout.simple_spinner_dropdown_item, parent,
                    false);
            return view;
        }
    }
}