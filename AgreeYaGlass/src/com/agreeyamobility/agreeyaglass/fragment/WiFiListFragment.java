package com.agreeyamobility.agreeyaglass.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.agreeyamobility.agreeyaglass.Common.ExtraKey;
import com.agreeyamobility.agreeyaglass.QRCodeFragmentActivity;
import com.agreeyamobility.agreeyaglass.R;
import com.agreeyamobility.agreeyaglass.ui.Info;
import com.agreeyamobility.agreeyaglass.ui.WiFiListAdapter;

public class WiFiListFragment extends Fragment implements OnItemClickListener {
    private ListView mListView;
    private WiFiListAdapter mAdapter;
    private WifiScanReceiver mReceiver;

    private WifiManager mWifiManager;

    private AlertDialog mDialog; 
    private AlertDialog mAddWiFIDialog;
    private EditText mAddWiFiView;
    private EditText mPassWordView;

    private Info mSelectInfo;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wifi_list_content, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView emptyView = (TextView) getActivity().findViewById(R.id.wifiEmptyText);
        mListView = (ListView) getActivity().findViewById(R.id.wifiList);
        mListView.setEmptyView(emptyView);
        mAdapter = new WiFiListAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

        mAddWiFiView = new EditText(getActivity());
        mAddWiFiView.setInputType(InputType.TYPE_CLASS_TEXT);
        mAddWiFiView.setHint(R.string.hint_add_wifi_ssid);
        
        mPassWordView = new EditText(getActivity());
        mPassWordView.setInputType(InputType.TYPE_CLASS_TEXT);
        mPassWordView.setHint(R.string.hint_password);
        mPassWordView.setTransformationMethod(new PasswordTransformationMethod());

        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);

        mReceiver = new WifiScanReceiver();
        getActivity().registerReceiver(mReceiver, getIntentFilter());

        ((Button) getActivity().findViewById(R.id.wifiAdd))
        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	mAddWiFIDialog.show();
            }
        });
        
        createDialog();
    }

    private IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        return filter;
    }

    private void createDialog() {
        mDialog = new AlertDialog.Builder(getActivity())
        .setTitle(R.string.dialog_title_password)
        .setView(mPassWordView)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Editable input = mPassWordView.getText();
                if (input != null && !TextUtils.isEmpty(input.toString().trim())) {
                    Bundle data = new Bundle();
                    data.putString(ExtraKey.WIFI_ADDRESS, mSelectInfo.getSSID());
                    data.putString(ExtraKey.WIFI_PASSWORD, input.toString());
                    ((QRCodeFragmentActivity)getActivity()).makeQRCode(data);
                }
            }
        })
        .setNegativeButton(android.R.string.cancel, null)
        .create();
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mPassWordView.setText("");
            }
        });
    
        mAddWiFIDialog = new AlertDialog.Builder(getActivity())
        .setTitle(R.string.dialog_title_add_wifi)
        .setView(mAddWiFiView)
        .setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                            int which) {
                        Editable input = mAddWiFiView.getText();
                        if (input != null && !TextUtils.isEmpty(input.toString().trim())) {
                        	mAdapter.addItem(new Info(input.toString().trim(), -60));
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, null)
        .create();
        
        mAddWiFIDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            	mAddWiFiView.setText("");
            }
        });
    }

    @Override
    public void onDestroy() {
        if (mReceiver != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        mSelectInfo = (Info) mAdapter.getItem(position);
        mDialog.show();
    }

    private class WifiScanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                
            } else if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiManager.WIFI_STATE_ENABLED ) { 
                    mWifiManager.startScan();
                }
            } else {
                List<ScanResult> list = mWifiManager.getScanResults();
                ArrayList<Info> infoList = new ArrayList<Info>();
                for (ScanResult result : list) {
                    if (!TextUtils.isEmpty(result.SSID.trim())) {
//                        String capa = result.capabilities;
//                        android.util.Log.i("TEST", "Capa :: " + capa);
//                        // check type
//                        int type = 0;
//                        if (capa.indexOf(WiFiType.WEP) > 0) {
//                            type = type + 1000;
//                        }
//                        if (capa.indexOf(WiFiType.WPA) > 0) {
//                            type = type + 100;
//                        }
//                        if (capa.indexOf(WiFiType.WPA2) > 0) {
//                            type = type + 10;
//                        }
//                        if (capa.indexOf(WiFiType.ESS) > 0 ) {
//                            type = type + 1;
//                        }

                        //android.util.Log.i("TEST", "capa :: " + type);
                        infoList.add(new Info(result.SSID, result.level));
                    }
                }
                mAdapter.setItemList(infoList);
                getActivity().unregisterReceiver(mReceiver);
                mReceiver = null;
            }
        }
    }
}
