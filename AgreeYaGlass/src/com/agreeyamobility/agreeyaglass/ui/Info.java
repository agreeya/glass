package com.agreeyamobility.agreeyaglass.ui;

public class Info {
    private String mSSID;
    private int mLevel;
   
    public Info(String ssid, int level) {
        mSSID = ssid;
        mLevel = level;
        
    }

    public String getSSID() {
        return mSSID;
    }

    public int getLevel() {
        return mLevel;
    }
}
