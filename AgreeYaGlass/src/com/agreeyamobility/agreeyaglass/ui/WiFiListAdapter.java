package com.agreeyamobility.agreeyaglass.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.agreeyamobility.agreeyaglass.R;

public class WiFiListAdapter extends BaseAdapter {
    private int[] RES_ID = new int[] {
            R.drawable.ic_qs_wifi_0, R.drawable.ic_qs_wifi_1,
            R.drawable.ic_qs_wifi_2, R.drawable.ic_qs_wifi_3,
            R.drawable.ic_qs_wifi_4
    };

    private ArrayList<Info> mWifiList;

    private LayoutInflater mInflater;

    public WiFiListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mWifiList = new ArrayList<Info>();
    }

    public void addItem(Info info) {
        mWifiList.add(0, info);
        this.notifyDataSetChanged();
    }

    public void setItemList(List<Info> list) {
        mWifiList.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mWifiList != null) {
            return mWifiList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mWifiList != null) {
            return mWifiList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView(parent);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();

        Info info = mWifiList.get(position);
        // display data
        holder.mNameView.setText(info.getSSID());

        int level = info.getLevel();
        int index = 0;
        if (level <= -60) {
            index = 4;
        } else if (level <= -50) {
            index = 3;
        } else if (level <= -40) {
            index = 2;
        } else if (level <= -30) {
            index = 1;
        } else {
            index = 0;
        }

        holder.mIconView.setImageResource(RES_ID[index]);
        return convertView;
    }

    private View newView(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.wifi_list_item, parent, false);
        ViewHolder holder = new ViewHolder(
                (TextView) view.findViewById(R.id.wifiName),
                (ImageView) view.findViewById(R.id.wifiIntensity));
        view.setTag(holder);
        return view;
    }

    private class ViewHolder {
        public ImageView mIconView;
        public TextView mNameView;

        public ViewHolder(TextView nameView, ImageView iconView) {
            mNameView = nameView;
            mIconView = iconView;

        }
    }
}
